;;; ---------------------------------------------------------------------------
;;;   License: LGPL-2.1+ (See file 'Copyright' for details).
;;; ---------------------------------------------------------------------------
;;;
;;;  (c) Copyright 2002 by Alexey Dejneka <adejneka@comail.ru>
;;;  (c) Copyright 2004 by Tim Moore <moore@bricoworks.com>
;;;  (c) Copyright 2019 by Daniel Kochmański <daniel@turtleware.eu>
;;;
;;; ---------------------------------------------------------------------------
;;;

(in-package #:clim-internals)

(defun default-tracking-handler (&key event &allow-other-keys)
  (handle-event (event-sheet event) event))

(deftype tracking-pointer-clause ()
  `(member :pointer-motion :pointer-button-press :pointer-button-release
           :presentation :presentation-button-press :presentation-button-release
           :keyboard))

(defclass tracking-pointer-state ()
  ((motion-handler
    :reader motion-handler
    :initarg :pointer-motion)
   (button-press-handler
    :reader button-press-handler
    :initarg :pointer-button-press)
   (buttton-release-handler
    :reader button-release-handler
    :initarg :pointer-button-release)
   (presentation-handler
    :reader presentation-handler
    :initarg :presentation)
   (presentation-button-release-handler
    :reader presentation-button-release-handler
    :initarg :presentation-button-release)
   (presentation-button-press-handler
    :reader presentation-button-press-handler
    :initarg :presentation-button-press)
   (keyboard-handler
    :reader keyboard-handler
    :initarg :keyboard)
   (tracked-sheet
    :reader tracked-sheet
    :initarg :sheet)
   (tracked-pointer
    :reader tracked-pointer
    :initarg :pointer)
   (multiple-window
    :reader multiple-window
    :initarg :multiple-window)
   (transformp
    :reader transformp
    :initarg :transformp)
   (context-type
    :reader context-type
    :initarg :context-type)
   (highlight
    :reader highlight
    :initarg :highlight)
   (%highlighted-presentation
    :accessor %highlighted-presentation
    :initform nil))
  (:default-initargs :pointer-motion         #'default-tracking-handler
                     :pointer-button-press   #'default-tracking-handler
                     :pointer-button-release #'default-tracking-handler
                     :keyboard               #'default-tracking-handler
                     ;; Presentation handlers default to NIL so we can
                     ;; resort to their "raw" counterparts when due.
                     :presentation nil
                     :presentation-button-press nil
                     :presentation-button-release nil
                     :multiple-window nil
                     :transformp nil
                     :context-type t
                     :highlight nil))

(defmethod initialize-instance :after
    ((instance tracking-pointer-state)
     &key pointer
          (presentation nil p-p)
          (presentation-button-press nil pbp-p)
          (presentation-button-release nil pbr-p)
          (highlight nil h-p))
  (declare (ignore presentation
                   presentation-button-press
                   presentation-button-release
                   highlight))
  (unless pointer
    (setf (slot-value instance 'tracked-pointer)
          (port-pointer (port (tracked-sheet instance)))))
  (unless h-p
    (setf (slot-value instance 'highlight)
          (or p-p pbp-p pbr-p))))

(defgeneric sheet-find-presentation (sheet context-type x y event)
  (:method (sheet context-type x y event)
    (declare (ignore sheet context-type x y event))
    nil)
  (:method ((stream output-recording-stream) context-type x y event)
    (labels ((matches-context-p (presentation)
               (presentation-subtypep (presentation-type presentation) context-type))
             (innermost-first (record)
               (map-over-output-records-containing-position #'innermost-first record x y)
               (when (and (presentationp record)
                          (matches-context-p record))
                 (return-from sheet-find-presentation record))))
      (or (innermost-first (stream-output-history stream))
          (let ((presentation (make-blank-area-presentation stream x y event)))
            (when (matches-context-p presentation)
              presentation))))))

;;; This function is responsible for handling events in
;;; tracking-pointer macro.
(defgeneric track-event (state event x y)
  (:method ((state tracking-pointer-state) event x y)
    (declare (ignore x y))
    (default-tracking-handler :event event))
  (:method ((state tracking-pointer-state) (event keyboard-event) x y)
    (funcall (keyboard-handler state) :gesture event :event event :x x :y y)))

(macrolet ((frob (event-type presentation-handler normal-handler)
             `(defmethod track-event ((state tracking-pointer-state)
                                      (event ,event-type)
                                      x y)
                (let ((window (event-sheet event)))
                  (when-let ((highlighted (%highlighted-presentation state)))
                    (highlight-output-record highlighted window :unhighlight))
                  (when-let* ((context-type (context-type state))
                              (handler (,presentation-handler state))
                              (presentation (sheet-find-presentation
                                             window context-type x y event)))
                    (when (highlight state)
                      (setf (%highlighted-presentation state) presentation)
                      (highlight-output-record presentation window :highlight))
                    (return-from track-event
                      (funcall handler
                               :presentation presentation
                               :event event :window window :x x :y y)))
                  (funcall (,normal-handler state) :event event :window window :x x :y y)))))
  (frob pointer-motion-event         presentation-handler                motion-handler)
  (frob pointer-button-press-event   presentation-button-press-handler   button-press-handler)
  (frob pointer-button-release-event presentation-button-release-handler button-release-handler))

(defun invoke-tracking-pointer (state)
  (let* ((tracked-sheet (tracked-sheet state))
         (pointer (tracked-pointer state))
         (multiple-window (multiple-window state))
         (transformp (transformp state))
         (modifier-state)
         (port (port tracked-sheet)))
    (labels ((track-pointer-event (event)
               (multiple-value-call #'track-event state event
                 (let ((x (pointer-event-x event))
                       (y (pointer-event-y event)))
                   (if (not transformp)
                       (values x y)
                       (with-sheet-medium (medium (event-sheet event))
                         (transform-position (medium-transformation medium) x y))))))
             (do-it ()
               (with-pointer-grabbed (port tracked-sheet
                                      :pointer pointer :multiple-window multiple-window)
                 ;; Synthesize a pointer motion event for the current pointer
                 ;; position so that appropriate handlers are called even if no
                 ;; event immediately follows the INVOKE-TRACKING-POINTER call.
                 ;; This ensures, for example, that feedback and/or pointer
                 ;; documentation are initialized right away in the context of
                 ;; presentation drag and drop.
                 ;;
                 ;; However, to prevent things like drag and drop feedback being
                 ;; drawn to the wrong sheet, discard the synthesized event if
                 ;; its sheet is not a tracked sheet. This can happen if
                 ;; MULTIPLE-WINDOW is false, INVOKE-TRACKING-POINTER is invoked
                 ;; via, say, a keyboard gesture or programmatically and the
                 ;; pointer is not over TRACKED-SHEET.
                 (let ((event (synthesize-pointer-motion-event port pointer)))
                   (setf modifier-state (event-modifier-state event))
                   (when (or multiple-window
                             (eql tracked-sheet (event-sheet event)))
                     (track-pointer-event event)))
                 (loop for event = (event-read tracked-sheet)
                       for sheet = (event-sheet event)
                       ;; We let HANDLE-EVENT take care of events that are not
                       ;; for TRACKED-SHEET (unless MULTIPLE-WINDOW is true). On
                       ;; the other hand, we pass events for TRACKED-SHEET (or
                       ;; all events if MULTIPLE-WINDOW is true) to TRACK-EVENT.
                       do (with-output-buffered (tracked-sheet)
                            (with-output-buffered (sheet)
                              (cond ((not (or multiple-window
                                              (eql tracked-sheet sheet)))
                                     ;; Event is not intercepted.
                                     (handle-event sheet event))
                                    ((typep event 'pointer-event)
                                     (track-pointer-event event))
                                    (t
                                     (track-event state event nil nil)))
                              ;; As a special exception, whenever a device event changes
                              ;; the modifier state, we synthesize an event, so that
                              ;; mouse-only and non-MULTIPLE-WINDOW handling can still
                              ;; react to changed keyboard modifiers.
                              (when (typep event 'device-event)
                                (let ((new-state (event-modifier-state event)))
                                  (when (not (eql modifier-state new-state))
                                    (track-pointer-event
                                     (synthesize-pointer-motion-event port pointer)))
                                  (setf modifier-state new-state)))))))))
      (if (keyboard-handler state)
          (with-input-focus (tracked-sheet)
            (do-it))
          (do-it)))))

(defmacro tracking-pointer
    ((sheet &rest args &key pointer multiple-window transformp context-type highlight)
     &body body)
  (declare (ignore pointer multiple-window transformp context-type highlight))
  (with-stream-designator (sheet '*standard-output*)
    ;; The Spec specifies the tracking-pointer clause arguments as,
    ;; e.g., (&key presentation event x y), implying that the user must
    ;; write the &key keyword, but real code doesn't do that. Check if
    ;; &key is in the arg list and add it if it is not.
    (flet ((fix-args (name args)
             (let ((aok nil)
                   (args (if (eq (car args) '&key)
                             args
                             (cons '&key args))))
               (dolist (arg (cdr args))
                 (cond ((find arg '(window event gesture presentation x y) :test #'string=))
                       ((eq arg '&allow-other-keys)
                        (setf aok t))
                       (t
                        (error "TRACKING-POINTER: ~s is not a valid argument for a clause ~s."
                               arg name))))
               (unless aok
                 (setq args (append args '(&allow-other-keys))))
               args)))
      (loop
        for (name arglist . body) in body
        for handler-name = (gensym (symbol-name name))
        do (unless (typep name 'tracking-pointer-clause)
             (error "TRACKING-POINTER: ~s is not a valid clause name." name))
        collect `(,handler-name ,(fix-args name arglist) ,@body) into bindings
        collect `#',handler-name into fn-names
        append  `(,name #',handler-name) into initargs
        finally (return `(flet ,bindings
                           (declare (dynamic-extent ,@fn-names))
                           (invoke-tracking-pointer
                            (make-instance 'tracking-pointer-state
                                           :sheet ,sheet ,@args ,@initargs))))))))


;;; DRAG-OUTPUT-RECORD and DRAGGING-OUTPUT.

(defun make-default-feedback-function (erase repaint dx dy)
  ;; ERASE is the function that removes the record from the output history.
  ;; REPAINT decides whether we should dispatch repaint on the old position.
  (let ((erase-region +nowhere+)
        (last-sheet nil))
    (lambda (record sheet x0 y0 x y action)
      (declare (ignore x0 y0))
      (ecase action
        (:erase
         (when repaint
           (setf erase-region (copy-bounding-rectangle record)))
         (setf last-sheet sheet)
         (maybe-funcall erase record sheet))
        (:draw
         (setf (output-record-position record) (values (+ x dx) (+ y dy)))
         (add-output-record record (stream-output-history sheet))
         (if (or (eq sheet last-sheet) (not repaint))
             (dispatch-repaint sheet (region-union erase-region record))
             (progn
               (dispatch-repaint last-sheet erase-region)
               (dispatch-repaint sheet record))))))))

(defmethod drag-output-record
    ((stream output-recording-stream) (record output-record)
     &key feedback finish-on-release multiple-window
       (erase #'erase-output-record) (repaint t))
  (nest
   (multiple-value-bind (abs-x0 abs-y0) (pointer-position (port-pointer (port stream))))
   (multiple-value-bind (str-x0 str-y0) (stream-pointer-position stream))
   (multiple-value-bind (rx ry)         (output-record-position record))
   (let* (;; last-sheet, last-x and last-y show the last pointer
          ;; position as observed from pointer-motion handler. This is
          ;; necessary to provide correct arguments for erasure.
          last-sheet last-x last-y
          ;; Mouse position relative to the record start is
          ;; necessary to drag it without repositioning it to
          ;; start where the cursor does.
          (dx (- rx str-x0))
          (dy (- ry str-y0)))
     (orf feedback (make-default-feedback-function erase repaint dx dy))
     (setf (stream-current-output-record stream)
           (stream-output-history stream))
     ;; feedback function may draw something not resembling
     ;; output record so we erase and draw it right away.
     (funcall feedback record stream str-x0 str-y0 str-x0 str-y0 :erase)
     (funcall feedback record stream str-x0 str-y0 str-x0 str-y0 :draw)
     (setf last-sheet stream
           last-x str-x0
           last-y str-y0)
     (tracking-pointer (stream :multiple-window multiple-window)
       (:pointer-motion
        (&key window x y)
        (unless (and (eql last-sheet window)
                     (= last-x x)
                     (= last-y y))
          (when (output-recording-stream-p window)
            (multiple-value-bind (x0 y0)
                (if (eql window stream)
                    (values str-x0 str-y0)
                    (let* ((graft (graft window))
                           (tr (sheet-delta-transformation window graft)))
                      (untransform-position tr abs-x0 abs-y0)))
              (funcall feedback record last-sheet x0 y0 last-x last-y :erase)
              (funcall feedback record window x0 y0 x y :draw))
            (setf last-sheet window
                  last-x x
                  last-y y))))
       (:pointer-button-press
        (&key x y)
        (unless finish-on-release
          (return-from drag-output-record (values x y))))
       (:pointer-button-release
        (&key x y)
        (when finish-on-release
          (return-from drag-output-record (values x y))))))))

(defmacro dragging-output ((&optional (stream '*standard-output*) &rest args
                            &key (repaint t) finish-on-release multiple-window)
                           &body body)
  (declare (ignore repaint finish-on-release multiple-window))
  (with-stream-designator (stream '*standard-output*)
    (with-gensyms (erase record)
      `(let ((,record (with-new-output-record (,stream) ,@body)))
         (multiple-value-prog1
             (drag-output-record ,stream ,record ,@args)
           (erase-output-record ,record ,stream nil))))))
