;;; ---------------------------------------------------------------------------
;;;   License: LGPL-2.1+ (See file 'Copyright' for details).
;;; ---------------------------------------------------------------------------
;;;
;;;  (c) Copyright 2000 by Michael McDonald <mikemac@mikemac.com>
;;;  (c) Copyright 2002 by Gilbert Baumann <unk6@rz.uni-karlsruhe.de>
;;;  (c) Copyright 2014 by Robert Strandh <robert.strandh@gmail.com>
;;;  (c) Copyright 2023 by Daniel Kochmański <daniel@turtleware.eu>
;;;
;;; ---------------------------------------------------------------------------
;;;
;;; The implementation of FIFO queues. There are special provisions for
;;; compressing events, but the queue may handle arbitrary items.
;;;
;;; SIMPLE-QUEUE is not thread-safe and works with immediate i/o mixins.
;;; CONCURRENT-QUEUE is thread-safe and works with standard  i/o mixins.
;;;
;;; The interface:
;;;
;;; - (queue-schedule queue item seconds) - the item is appended with delay
;;; - (queue-append   queue item) - the item is appended  immedietely (back)
;;; - (queue-prepend  queue item) - the item is prepended immedietely (front)
;;;
;;; - (queue-peek queue)
;;; - (queue-peek-if fun queue)
;;;
;;; - (queue-listen queue)
;;; - (queue-listen-or-wait queue timeout wait-function)
;;;
;;; - (queue-read queue)
;;; - (queue-read-no-hang queue)
;;; - (queue-read-or-wait queue timeout wait-function)

(in-package "CLIM-INTERNALS")

;; Event queues

(defstruct (schedule-entry
            (:constructor make-schedule-entry (time event))
            (:predicate nil)
            (:copier nil))
  (time  0   :type (real 0) :read-only t)
  (event nil                :read-only t))

(defclass schedule-mixin ()
  ((schedule-time :initform nil
                  :accessor event-schedule-time
                  :documentation "The next time an event should be scheduled.")
   (schedule :initform nil
             :accessor schedule-queue
             :documentation "Time ordered queue of events to schedule."))
  (:documentation "Experimental timer event extension."))

(declaim (inline now compute-decay))
(defun now ()
  (/ (get-internal-real-time)
     #.(float internal-time-units-per-second)))

;; Given two alarm times compute remaining time (in seconds). If both times are
;; NIL returns NIL.
(defun compute-decay (time-1 time-2)
  (cond ((and time-1 time-2) (max 0 (- (min time-1 time-2) (now))))
        (time-1              (max 0 (- time-1 (now))))
        (time-2              (max 0 (- time-2 (now))))
        (t                   nil)))

;; See if it's time to inject a scheduled event into the queue.
(defgeneric check-schedule (queue)
  (:method ((queue schedule-mixin))
    (when-let* ((schedule-time (event-schedule-time queue))
                (execute-p (>= (now) schedule-time))
                (entry (pop (schedule-queue queue))))
      (setf (event-schedule-time queue)
            (if-let ((next-entry (first (schedule-queue queue))))
              (schedule-entry-time next-entry)
              nil))
      (queue-append queue (schedule-entry-event entry)))))

(defmethod queue-schedule ((queue schedule-mixin) event delay)
  (with-slots (schedule) queue
    (let* ((alarm (+ (now) delay))
           (entry (make-schedule-entry alarm event)))
      (cond ((null schedule) ; no other events scheduled
             (push entry schedule)
             (setf (event-schedule-time queue) alarm))
            ((< alarm (event-schedule-time queue)) ; EVENT is new earliest
             (push entry schedule)
             (setf (event-schedule-time queue) alarm))
            (t ; EVENT goes somewhere after the earliest event
             (do* ((previous schedule (rest previous))
                   (rest  (rest schedule) (rest rest))
                   (current #1=(first rest) #1#))
                  ((or (null rest)
                       (< alarm (schedule-entry-time current)))
                   (setf (cdr previous) (list* entry (cdr previous))))))))))


(defclass proto-queue-mixin ()
  ((head :initform nil
         :accessor queue-head
         :documentation "Head pointer of queue.")
   (tail :initform nil
         :accessor queue-tail
         :documentation "Tail pointer of queue.")))

(defun %queue-push-head (queue item)
  (if (null (queue-head queue))
      (setf (queue-tail queue)
            (setf (queue-head queue) (cons item nil)))
      (push item (queue-head queue))))

(defun %queue-push-tail (queue item)
  (if (null (queue-head queue))
      (setf (queue-tail queue)
            (setf (queue-head queue) (cons item nil)))
      (setf (queue-tail queue)
            (setf (cdr (queue-tail queue)) (cons item nil)))))

(defun %queue-read (queue)
  (let ((res (pop (queue-head queue))))
    (when (null (queue-head queue))
      (setf (queue-tail queue) nil))
    res))

(defun %queue-drain (queue)
  (let ((res (queue-head queue)))
    (unless (null res)
      (setf (queue-head queue) nil)
      (setf (queue-tail queue) nil))
    res))


(defclass simple-queue (queue proto-queue-mixin schedule-mixin)
  ((port :initform nil
         :initarg  :port
         :accessor queue-port
         :documentation "The port which will be generating events for the queue."))
  (:documentation "Event queue which works under assumption that there are no
concurrent threads. Most notably it calls process-next-event by itself. Doesn't
use condition-variables nor locks."))

;;; We want to force port output just in case we wait for an event yet to be
;;; performed by a user (which may be prompted on a screen). -- jd 2018-12-26
(defun do-port-force-output (port-queue)
  (when-let ((port (queue-port port-queue)))
    (port-force-output port)))

(defmethod queue-read ((queue simple-queue))
  (do-port-force-output queue)
  (check-schedule queue)
  (process-next-event (queue-port queue) :timeout 0)
  (loop
     with port = (queue-port queue)
     as result = (%queue-read queue)
     if result do (return result)
     else do
       (if-let ((decay (compute-decay nil (event-schedule-time queue))))
         (process-next-event port :timeout decay)
         (process-next-event port))
       (check-schedule queue)))

(defmethod queue-drain ((queue simple-queue))
  (do-port-force-output queue)
  (check-schedule queue)
  (%queue-drain queue))

(defmethod queue-read-no-hang ((queue simple-queue))
  (do-port-force-output queue)
  (check-schedule queue)
  (process-next-event (queue-port queue) :timeout 0)
  (%queue-read queue))

(defmethod queue-read-or-wait ((queue simple-queue) timeout wait-function)
  (do-port-force-output queue)
  ;; Preemptive check (timeout may be past due or wait-function may
  ;; already return true). -- jd 2020-01-22
  (check-schedule queue)
  (let ((port (queue-port queue)))
    (multiple-value-bind (available reason)
        (process-next-event port :timeout 0 :wait-function wait-function)
      (declare (ignore available))
      (cond ((eq reason :wait-function)
             (return-from queue-read-or-wait
               (values nil :wait-function)))
            ((queue-head queue)
             (return-from queue-read-or-wait
               (%queue-read queue)))))
    (loop
      with timeout-time = (and timeout (+ timeout (now)))
      do (cond ((maybe-funcall wait-function)
                (return (values nil :wait-function)))
               ((queue-head queue)
                (return (%queue-read queue)))
               ((and timeout-time (> (now) timeout-time))
                (return (values nil :timeout)))
               ((multiple-value-bind (available reason)
                    (let* ((schedule-time (event-schedule-time queue))
                           (decay (compute-decay timeout-time schedule-time)))
                      (process-next-event port
                                          :timeout decay
                                          :wait-function wait-function))
                  (when (null available)
                    (return (values nil reason)))))
               (t (check-schedule queue))))))

(defmethod queue-append ((queue simple-queue) item)
  (flet ((event-delete-if (predicate)
           (when (not (null (queue-head queue)))
             (setf (queue-head queue) (delete-if predicate (queue-head queue))
                   (queue-tail queue) (last (queue-head queue))))))
    (typecase item
      ;;
      ;; Motion Event Compression
      ;;
      ;; . find the (at most one) motion event
      ;; . delete it
      ;; . append item to queue
      ;;
      ;; But leave enter/exit events.
      ;;
      ((and pointer-motion-event (not pointer-boundary-event))
       (let ((sheet (event-sheet item)))
         (event-delete-if
          #'(lambda (x)
              (and (typep x 'pointer-motion-event)
                   (not (typep x 'pointer-boundary-event))
                   (eq (event-sheet x) sheet))))
         (%queue-push-tail queue item)))
      ;;
      ;; Resize event compression
      ;;
      ;; Configuration events must be prepended to the queue to ensure that
      ;; they are always before repaint events. Otherwise compressed repaint
      ;; could go before its window resize event and be clipped by the old
      ;; region (leaving part of the dirty region intact). When a separate
      ;; repaint queue is introduced, then do append again. -- jd 2022-06-01
      ;;
      (window-configuration-event
       (when (typep (event-sheet item) 'top-level-sheet-mixin)
         (let ((sheet (event-sheet item)))
           (event-delete-if
            #'(lambda (ev)
                (and (typep ev 'window-configuration-event)
                     (eq (event-sheet ev) sheet)))))
         (%queue-push-head queue item)))
      ;;
      ;; Repaint event compression
      ;;
      (window-repaint-event
       (let ((region (window-event-native-region item))
             (sheet  (event-sheet item))
             (did-something-p nil))
         (labels ((fun (xs)
                    (cond ((null xs)
                           ;; We reached the queue's tail: Append the new event,
                           ;; construct a new one if necessary.
                           (when did-something-p
                             (setf item
                                   (make-instance 'window-repaint-event
                                                  :timestamp (event-timestamp item)
                                                  :sheet     (event-sheet item)
                                                  :region    region)))
                           (setf (queue-tail queue) (cons item nil)) )
                          ((and (typep (car xs) 'window-repaint-event)
                                (eq (event-sheet (car xs)) sheet))
                           ;; This is a repaint event for the same
                           ;; sheet, delete it and combine its region
                           ;; into the new event.
                           (setf region
                                 (region-union region
                                               (window-event-native-region (car xs))))
                           ;; Here is an alternative, which just takes
                           ;; the bounding rectangle.
                           ;; NOTE: When doing this also take care that
                           ;; the new region really is cleared.
                           ;; (setf region
                           ;;   (let ((old-region (window-event-native-region (car xs))))
                           ;;     (make-rectangle*
                           ;;      (min (bounding-rectangle-min-x region)
                           ;;           (bounding-rectangle-min-x old-region))
                           ;;      (min (bounding-rectangle-min-y region)
                           ;;           (bounding-rectangle-min-y old-region))
                           ;;      (max (bounding-rectangle-max-x region)
                           ;;           (bounding-rectangle-max-x old-region))
                           ;;      (max (bounding-rectangle-max-y region)
                           ;;           (bounding-rectangle-max-y old-region)))))
                           (setf did-something-p t)
                           (fun (cdr xs)))
                          ;;
                          (t
                           (setf (cdr xs) (fun (cdr xs)))
                           xs))))
           (setf (queue-head queue) (fun (queue-head queue))))))
      ;; Regular events are just appended:
      (otherwise
       (%queue-push-tail queue item)))))

(defmethod queue-prepend ((queue simple-queue) item)
  (%queue-push-head queue item))

(defmethod queue-peek ((queue simple-queue))
  (do-port-force-output queue)
  (check-schedule queue)
  (process-next-event (queue-port queue) :timeout 0)
  (first (queue-head queue)))

(defmethod queue-peek-if (predicate (queue simple-queue))
  (do-port-force-output queue)
  (check-schedule queue)
  ;; Slurp as many elements as available.
  (loop with port = (queue-port queue)
        while (process-next-event port :timeout 0))
  (find-if predicate (queue-head queue)))

(defmethod queue-listen-or-wait
    ((queue simple-queue) timeout wait-function)
  (do-port-force-output queue)
  (check-schedule queue)
  (let ((port (queue-port queue)))
   (multiple-value-bind (available reason)
       (process-next-event port :timeout 0
                                :wait-function wait-function)
     (declare (ignore available))
     (cond ((eq reason :wait-function)
            (return-from queue-listen-or-wait
              (values nil :wait-function)))
           ((queue-head queue)
            (return-from queue-listen-or-wait
              t))))
    (loop
      with timeout-time = (and timeout (+ timeout (now)))
      do (cond ((maybe-funcall wait-function)
                (return (values nil :wait-function)))
               ((queue-head queue)
                (return t))
               ((and timeout-time (> (now) timeout-time))
                (return (values nil :timeout)))
               ((multiple-value-bind (available reason)
                    (let* ((schedule-time (event-schedule-time queue))
                           (decay (compute-decay timeout-time schedule-time)))
                      (process-next-event port
                                          :timeout decay
                                          :wait-function wait-function))
                  (when (null available)
                    (return (values nil reason)))))
               (t (check-schedule queue))))))

(defmethod queue-listen ((queue simple-queue))
  (queue-listen-or-wait queue nil 0))


;;; XXX SBCL doesn't reacquire lock when condition-variable prematurely returns.
;;; This means that we can't use recursive locks. --jd 2019-06-26

(defclass concurrent-queue (simple-queue)
  ((lock :initform (make-lock "Event queue")
         :reader queue-lock)
   (processes :initform (make-condition-variable)
              :accessor queue-processes
              :documentation "Condition variable for waiting processes")
   (schedule-lock :initform (make-lock "Event queue schedule")
                  :reader schedule-queue-lock
                  :documentation "Protects SCHEDULE-TIME and SCHEDULE slots.")))

(defmethod queue-read ((queue concurrent-queue))
  (do-port-force-output queue)
  (let ((lock (queue-lock queue))
        (cv (queue-processes queue)))
    (loop
       (check-schedule queue)
       (with-lock-held (lock)
         (if-let ((result (%queue-read queue)))
           (return result)
           (let* ((schedule-time (event-schedule-time queue))
                  (decay (compute-decay nil schedule-time)))
             (condition-wait cv lock decay)))))))

(defmethod queue-drain ((queue simple-queue))
  (do-port-force-output queue)
  (check-schedule queue)
  (with-lock-held ((queue-lock queue))
    (%queue-drain queue)))

(defmethod queue-read-no-hang ((queue concurrent-queue))
  (do-port-force-output queue)
  (check-schedule queue)
  (with-lock-held ((queue-lock queue))
    (%queue-read queue)))

(defmethod queue-read-or-wait ((queue concurrent-queue)
                                     timeout wait-function)
  (do-port-force-output queue)
  ;; We need to LOOP because of possible spurious wakeup (sbcl/bt quirk).
  (loop
     with lock = (queue-lock queue)
     with cv = (queue-processes queue)
     with timeout-time = (and timeout (+ timeout (now)))
     with event = nil
     do (check-schedule queue)
        (when (maybe-funcall wait-function)
          (return (values nil :wait-function)))
        (with-lock-held (lock)
          (cond ((setf event (%queue-read queue))
                 (return event))
                ((and timeout-time (> (now) timeout-time))
                 (return (values nil :timeout))))
          (let* ((schedule-time (event-schedule-time queue))
                 (decay (compute-decay timeout-time schedule-time)))
            (condition-wait cv lock decay)))))

(defmethod queue-append ((queue concurrent-queue) item)
  (declare (ignore item))
  (with-lock-held ((queue-lock queue))
    (call-next-method)
    (condition-notify (queue-processes queue))))

(defmethod queue-prepend ((queue concurrent-queue) item)
  (declare (ignore item))
  (with-lock-held ((queue-lock queue))
    (call-next-method)
    (condition-notify (queue-processes queue))))

(defmethod queue-peek ((queue concurrent-queue))
  (do-port-force-output queue)
  (check-schedule queue)
  (with-lock-held ((queue-lock queue))
    (first (queue-head queue))))

(defmethod queue-peek-if (predicate (queue concurrent-queue))
  (do-port-force-output queue)
  (check-schedule queue)
  (with-lock-held ((queue-lock queue))
    (find-if predicate (queue-head queue))))

(defmethod queue-listen-or-wait
    ((queue concurrent-queue) timeout wait-function)
  (do-port-force-output queue)
  ;; We need to LOOP because of possible spurious wakeup (sbcl/bt quirk).
  (loop
     with lock = (queue-lock queue)
     with cv = (queue-processes queue)
     with timeout-time = (and timeout (+ timeout (now)))
     do (check-schedule queue)
        (when (maybe-funcall wait-function)
          (return (values nil :wait-function)))
        (with-lock-held (lock)
          (cond ((queue-head queue)
                 (return t))
                ((and timeout-time (> (now) timeout-time))
                 (return (values nil :timeout)))
                (wait-function
                 ;; We CLAMP decay when wait-function is present to
                 ;; ensure that we don't get stuck until next event
                 ;; arrives (or the timeout happens). It is busy wait
                 ;; with a lousy grain. -- jd 2019-06-06
                 (if-let ((decay (compute-decay timeout-time
                                                (event-schedule-time queue))))
                   (condition-wait cv lock (min 0.01 decay))
                   (condition-wait cv lock 0.01)))
                (t
                 (let ((decay (compute-decay timeout-time
                                             (event-schedule-time queue))))
                   (condition-wait cv lock decay)))))))

(defmethod check-schedule :around ((queue concurrent-queue))
  (with-lock-held ((schedule-queue-lock queue))
    (call-next-method)))

(defmethod queue-schedule :around ((queue concurrent-queue) event delay)
  (declare (ignore event delay))
  (with-lock-held ((schedule-queue-lock queue))
    (call-next-method)))
