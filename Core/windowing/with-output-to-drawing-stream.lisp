(in-package #:climi)

(defmacro with-output-to-drawing-stream ((stream backend destination &rest args) &body body)
  (with-gensyms (cont)
    `(flet ((,cont (,stream) ,@body))
       (declare (dynamic-extent (function ,cont)))
       (invoke-with-output-to-drawing-stream (function ,cont) ,backend ,destination ,@args))))

(defmethod invoke-with-output-to-drawing-stream (continuation backend destination &rest args)
  (with-port (port backend)
    (apply #'invoke-with-output-to-drawing-stream continuation port destination args)))

;;; KLUDGE CHANGING-SPACE-REQUIREMENTS implementation is not correct -- the
;;; macro should work on the current application frame and not on all frames in
;;; the dynamic context. I'm already sidetracked by this, so I'm adding this
;;; kludge and leave that as is. -- jd 2025-01-29
(defmethod invoke-with-output-to-drawing-stream :around (continuation port destination &rest args)
  (declare (ignore args))
  (let ((*changing-space-requirements* nil))
    (declare (special *changing-space-requirements*))
    (call-next-method)))
