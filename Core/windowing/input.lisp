;;; ---------------------------------------------------------------------------
;;;   License: LGPL-2.1+ (See file 'Copyright' for details).
;;; ---------------------------------------------------------------------------
;;;
;;;  (c) Copyright 1998,1999,2000 by Michael McDonald <mikemac@mikemac.com>
;;;  (c) Copyright 2002 by Gilbert Baumann <unk6@rz.uni-karlsruhe.de>
;;;  (c) Copyright 2014 by Robert Strandh <robert.strandh@gmail.com>
;;;  (c) Copyright 2018 by Daniel Kochmański <daniel@turtleware.eu>
;;;
;;; ---------------------------------------------------------------------------
;;;

(in-package #:clim-internals)

;;; STANDARD-SHEET-INPUT-MIXIN

(defclass standard-sheet-input-mixin ()
  ((event-queue :initform (if *multiprocessing-p*
                              (make-instance 'concurrent-queue)
                              (make-instance 'simple-queue))
                :reader sheet-event-queue
                :initarg :event-queue)))

(defmethod dispatch-event ((sheet standard-sheet-input-mixin) event)
  (queue-event sheet event))

(defmethod queue-event ((sheet standard-sheet-input-mixin) event)
  (queue-append (sheet-event-queue sheet) event))

(defmethod schedule-event ((sheet standard-sheet-input-mixin) event delay)
  (queue-schedule (sheet-event-queue sheet) event delay))

(defmethod handle-event ((sheet standard-sheet-input-mixin) event)
  ;; Standard practice is to ignore events.
  (declare (ignore sheet event))
  (when (next-method-p)
    (call-next-method)))

(defmethod event-read ((sheet standard-sheet-input-mixin))
  (queue-read (sheet-event-queue sheet)))

(defmethod event-read-no-hang ((sheet standard-sheet-input-mixin))
  (queue-read-no-hang (sheet-event-queue sheet)))

(defmethod event-peek ((sheet standard-sheet-input-mixin) &optional event-type)
  (if event-type
      (queue-peek-if (lambda (x) (typep x event-type)) (sheet-event-queue sheet))
      (queue-peek (sheet-event-queue sheet))))

(defmethod event-unread ((sheet standard-sheet-input-mixin) event)
  (queue-prepend (sheet-event-queue sheet) event))

(defmethod event-listen ((sheet standard-sheet-input-mixin))
  (queue-listen (sheet-event-queue sheet)))

(defclass no-event-queue-mixin ()
  ())

(defclass immediate-sheet-input-mixin (no-event-queue-mixin)
  ())

(defmethod dispatch-event ((sheet immediate-sheet-input-mixin) event)
  (handle-event sheet event))

(defmethod handle-event ((sheet immediate-sheet-input-mixin) event)
  (declare (ignore sheet event))
  (when (next-method-p)
    (call-next-method)))

(define-condition sheet-is-mute-for-input (error)
    ())

(defclass sheet-mute-input-mixin (no-event-queue-mixin)
  ())

(defmethod dispatch-event ((sheet sheet-mute-input-mixin) event)
  (declare (ignore event))
  (error 'sheet-is-mute-for-input))

(defmethod queue-event ((sheet sheet-mute-input-mixin) event)
  (declare (ignore event))
  (error 'sheet-is-mute-for-input))

(defmethod schedule-event ((sheet sheet-mute-input-mixin) event delay)
  (declare (ignore event delay))
  (error 'sheet-is-mute-for-input))

(defmethod handle-event ((sheet sheet-mute-input-mixin) event)
  (declare (ignore sheet event))
  (error 'sheet-is-mute-for-input))

(defmethod event-read ((sheet sheet-mute-input-mixin))
  (error 'sheet-is-mute-for-input))

(defmethod event-read-no-hang ((sheet sheet-mute-input-mixin))
  (error 'sheet-is-mute-for-input))

(defmethod event-peek ((sheet sheet-mute-input-mixin) &optional event-type)
  (declare (ignore event-type))
  (error 'sheet-is-mute-for-input))

(defmethod event-unread ((sheet sheet-mute-input-mixin) event)
  (declare (ignore event))
  (error 'sheet-is-mute-for-input))

(defmethod event-listen ((sheet sheet-mute-input-mixin))
  (error 'sheet-is-mute-for-input))

;;;;

(defclass delegate-sheet-input-mixin ()
  ((delegate :initform nil
             :initarg :delegate
             :accessor delegate-sheet-delegate)))

(defmethod dispatch-event ((sheet delegate-sheet-input-mixin) event)
  (dispatch-event (delegate-sheet-delegate sheet) event))

(defmethod queue-event ((sheet delegate-sheet-input-mixin) event)
  (queue-event (delegate-sheet-delegate sheet) event))

(defmethod schedule-event ((sheet delegate-sheet-input-mixin) event delay)
  (schedule-event (delegate-sheet-delegate sheet) event delay))

(defmethod handle-event ((sheet delegate-sheet-input-mixin) event)
  (handle-event (delegate-sheet-delegate sheet) event))

(defmethod event-read ((sheet delegate-sheet-input-mixin))
  (event-read (delegate-sheet-delegate sheet)))

(defmethod event-read-no-hang ((sheet delegate-sheet-input-mixin))
  (event-read-no-hang (delegate-sheet-delegate sheet)))

(defmethod event-peek ((sheet delegate-sheet-input-mixin) &optional event-type)
  (event-peek (delegate-sheet-delegate sheet) event-type))

(defmethod event-unread ((sheet delegate-sheet-input-mixin) event)
  (event-unread (delegate-sheet-delegate sheet) event))

(defmethod event-listen ((sheet delegate-sheet-input-mixin))
  (event-listen (delegate-sheet-delegate sheet)))

;;; Extensions involving wait-function
;;;
;;; wait-function in principle behaves like for process-next-event
;;; (and for single-threaded run it is exactly what happens - we pass
;;; it to the port method). It is not called in a busy loop but rather
;;; after some input wakes up blocking backend-specific wait
;;; function. Then we call wait-function. -- jd 2019-03-26

(defmethod event-read-with-timeout ((sheet standard-sheet-input-mixin)
                                    &key (timeout nil) (wait-function nil))
  (queue-read-or-wait (sheet-event-queue sheet) timeout wait-function))

(defmethod event-read-with-timeout ((sheet sheet-mute-input-mixin)
                                    &key (timeout nil) (wait-function nil))
  (declare (ignore timeout wait-function))
  (error 'sheet-is-mute-for-input))

(defmethod event-read-with-timeout ((sheet delegate-sheet-input-mixin)
                                    &key (timeout nil) (wait-function nil))
  (event-read-with-timeout (delegate-sheet-delegate sheet)
                           :timeout timeout :wait-function wait-function))

(defmethod event-listen-or-wait ((sheet standard-sheet-input-mixin)
                                 &key (timeout nil) (wait-function nil))
  (queue-listen-or-wait (sheet-event-queue sheet) timeout wait-function))

(defmethod event-listen-or-wait ((sheet sheet-mute-input-mixin)
                                 &key (timeout nil) (wait-function nil))
  (declare (ignore timeout wait-function))
  (error 'sheet-is-mute-for-input))

(defmethod event-listen-or-wait ((sheet delegate-sheet-input-mixin)
                                 &key (timeout nil) (wait-function nil))
  (event-listen-or-wait (delegate-sheet-delegate sheet)
                        :timeout timeout :wait-function wait-function))
