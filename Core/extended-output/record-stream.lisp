;;; ---------------------------------------------------------------------------
;;;   License: LGPL-2.1+ (See file 'Copyright' for details).
;;; ---------------------------------------------------------------------------
;;;
;;;  (c) copyright 1998,1999,2000,2001,2003 Michael McDonald <mikemac@mikemac.com>
;;;  (c) copyright 2000-2003,2009-2016 Robert Strandh <robert.strandh@gmail.com>
;;;  (c) copyright 2001 Arnaud Rouanet <rouanet@emi.u-bordeaux.fr>
;;;  (c) copyright 2001 Lionel Salabartan <salabart@emi.u-bordeaux.fr>
;;;  (c) copyright 2001,2002 Alexey Dejneka <adejneka@comail.ru>
;;;  (c) copyright 2002,2003,2004 Timothy Moore <tmoore@common-lisp.net>
;;;  (c) copyright 2002,2003,2004,2005 Gilbert Baumann <unk6@rz.uni-karlsruhe.de>
;;;  (c) copyright 2003-2008 Andy Hefner <ahefner@common-lisp.net>
;;;  (c) copyright 2005,2006 Christophe Rhodes <crhodes@common-lisp.net>
;;;  (c) copyright 2006 Andreas Fuchs <afuchs@common-lisp.net>
;;;  (c) copyright 2007 David Lichteblau <dlichteblau@common-lisp.net>
;;;  (c) copyright 2007 Robert Goldman <rgoldman@common-lisp.net>
;;;  (c) copyright 2017 Cyrus Harmon <cyrus@bobobeach.com>
;;;  (c) copyright 2018 Elias Martenson <lokedhs@gmail.com>
;;;  (c) copyright 2018-2021 Jan Moringen <jmoringe@techfak.uni-bielefeld.de>
;;;  (c) copyright 2016-2024 Daniel Kochmański <daniel@turtleware.eu>
;;;
;;; ---------------------------------------------------------------------------
;;;
;;; Output recording streams.
;;;

(in-package #:clim-internals)

;;; Macro masturbation...

(defmacro define-invoke-with (macro-name func-name record-type doc-string)
  `(defmacro ,macro-name ((stream
                           &optional
                           (record-type '',record-type)
                           (record (gensym))
                           &rest initargs)
                          &body body)
     ,doc-string
     (orf record (gensym))
     (with-stream-designator (stream '*standard-output*)
       (with-gensyms (continuation)
         (multiple-value-bind (bindings m-i-args)
             (rebind-arguments initargs)
           `(let ,bindings
              (flet ((,continuation (,stream ,record)
                       ,(declare-ignorable-form* stream record)
                       ,@body))
                (declare (dynamic-extent #',continuation))
                (,',func-name ,stream #',continuation ,record-type ,@m-i-args))))))))

(define-invoke-with with-new-output-record invoke-with-new-output-record
  standard-sequence-output-record
  "Creates a new output record of type RECORD-TYPE and then captures
the output of BODY into the new output record, and inserts the new
record into the current \"open\" output record assotiated with STREAM.
    If RECORD is supplied, it is the name of a variable that will be
lexically bound to the new output record inside the body. INITARGS are
CLOS initargs that are passed to MAKE-INSTANCE when the new output
record is created.
    It returns the created output record.
    The STREAM argument is a symbol that is bound to an output
recording stream. If it is T, *STANDARD-OUTPUT* is used.")

(define-invoke-with with-output-to-output-record
    invoke-with-output-to-output-record
  standard-sequence-output-record
  "Creates a new output record of type RECORD-TYPE and then captures
the output of BODY into the new output record. The cursor position of
STREAM is initially bound to (0,0)
    If RECORD is supplied, it is the name of a variable that will be
lexically bound to the new output record inside the body. INITARGS are
CLOS initargs that are passed to MAKE-INSTANCE when the new output
record is created.
    It returns the created output record.
    The STREAM argument is a symbol that is bound to an output
recording stream. If it is T, *STANDARD-OUTPUT* is used.")

(defmacro with-output-recording-options ((stream
                                          &key (record nil record-supplied-p)
                                               (draw nil draw-supplied-p))
                                         &body body)
  (with-stream-designator (stream '*standard-output*)
    (with-gensyms (continuation)
      `(flet ((,continuation  (,stream)
                ,(declare-ignorable-form* stream)
                ,@body))
         (declare (dynamic-extent #',continuation))
         (invoke-with-output-recording-options
          ,stream #',continuation
          ,(if record-supplied-p record `(stream-recording-p ,stream))
          ,(if draw-supplied-p draw `(stream-drawing-p ,stream)))))))

(defun replay (record stream &optional region)
  (when (typep stream 'encapsulating-stream)
    (return-from replay (replay record (encapsulating-stream-stream stream) region)))
  (unless region
    (setf region (sheet-visible-region stream)))
  (stream-close-text-output-record stream)
  (with-cursor-off ((stream-text-cursor stream))
    (reset-stream-cursor stream (stream-text-cursor stream))
    (with-identity-transformation (stream)
      (replay-output-record record stream region))))

(defmethod replay-output-record
    ((record compound-output-record) (stream encapsulating-stream)
     &optional region (x-offset 0) (y-offset 0))
  (replay-output-record record (encapsulating-stream-stream stream)
                        region x-offset y-offset))

(defmethod highlight-output-record ((record output-record) stream state)
  (with-identity-transformation (stream)
    (flet ((compute-region ()
             ;; We can't "just" draw-rectangle :filled nil because the path lines
             ;; rounding may get outside the bounding rectangle. -- jd 2019-02-01
             (multiple-value-bind (x1 y1 x2 y2) (bounding-rectangle* record)
               (if (or (> (1+ x1) (1- x2))
                       (> (1+ y1) (1- y2)))
                   (bounding-rectangle record)
                   (region-difference
                    (bounding-rectangle record)
                    (make-rectangle* (1+ x1) (1+ y1) (1- x2) (1- y2)))))))
     (ecase state
       (:highlight
        (draw-design stream (compute-region) :ink +foreground-ink+))
       (:unhighlight
        (dispatch-repaint stream (compute-region)))))))


(defclass updating-output-stream-mixin (updating-output-map-mixin)
  ((redisplaying-p
    :initform nil
    :reader stream-redisplaying-p)
   (incremental-redisplay
    :initform nil
    :initarg :incremental-redisplay
    :accessor pane-incremental-redisplay)
   ;; For incremental output, holds the top level updating-output-record.
   (updating-record
    :initform nil
    :initarg :updating-record
    :accessor updating-record)))

(defmacro with-stream-redisplaying ((stream) &body body)
  `(letf (((slot-value ,stream 'redisplaying-p) t)) ,@body))

(defmethod redisplayable-stream-p ((stream updating-output-stream-mixin))
  (declare (ignore stream))
  t)

(defmethod pane-needs-redisplay :around ((pane updating-output-stream-mixin))
  (let ((redisplayp (call-next-method)))
    (values redisplayp (and (not (eq redisplayp :no-clear))
                            (not (pane-incremental-redisplay pane))))))


;;; The TLS-RECORD-CONTEXT is a thread-safe data structure that allows for
;;; concurrent recording of drawing and text.

(defclass tls-record-context ()
  ((stream
    :initarg :stream
    :accessor context-stream)
   (medium
    :dynamic :tls
    :accessor context-medium)
   (save-p
    :dynamic :tls
    :initform t
    :accessor context-save-p)
   (draw-p
    :dynamic :tls
    :initform t
    :accessor context-draw-p)
   (draw-output-record
    :dynamic :tls
    :initarg :draw-record
    :initform nil
    :accessor context-draw-output-record)
   (text-output-record
    :dynamic :tls
    :initarg :text-record
    :initform nil
    :accessor context-text-output-record)
   (text-cursor
    :dynamic t                          ; global value is shared
    :initarg :text-cursor
    :accessor context-text-cursor)
   #+ (or)                              ; unused
   (text-buffer
    :dynamic t                          ; global value is shared
    :initarg :text-buffer
    :accessor context-text-buffer))
  (:metaclass class-with-dynamic-slots))

;;; We don't use ALLOCATE-MEDIUM, because we are not interested in the medium
;;; specialized by the backend. This faux medium is used only for recording.
;;; Some queries are an exceptions handled by the stream. -- jd 2024-11-13
(defun make-context-medium (self)
  (let ((stream (context-stream self)))
    (make-instance 'basic-medium :port (port stream) :stream stream)))

(defmethod slot-unbound ((class standard-class)
                         (self tls-record-context)
                         (slot-name (eql 'medium)))
  (let ((stream (context-stream self))
        (medium (make-context-medium self)))
    (setf (graphics-state medium) stream)
    (setf (context-medium self) medium)))

;;; 16.4. Output Recording Streams
(defclass standard-output-recording-stream (updating-output-stream-mixin
                                            complete-medium-state
                                            output-recording-stream)
  ((record-context :reader stream-record-context)
   (output-history :reader stream-output-history)
   (needs-layout-p :accessor needs-layout-p :initform nil)
   (needs-scroll-p :accessor needs-scroll-p :initform nil)
   ;; Different abstractions like to rhyme with each other. -- jd 2024-11-13
   (#:dirty-region :accessor stream-dirty-region :initform +nowhere+))
  (:documentation "This class is mixed into some other stream class to
add output recording facilities. It is not instantiable."))

(defmethod sheet-medium ((stream standard-output-recording-stream))
  (if (stream-recording-p stream)
      (let ((context (stream-record-context stream)))
        (context-medium context))
      (call-next-method)))

(macrolet ((def-accessor (stream-op context-op)
             `(progn
                (defmethod ,stream-op
                    ((stream standard-output-recording-stream))
                  (,context-op (stream-record-context stream)))
                (defmethod (setf ,stream-op)
                    (value (stream standard-output-recording-stream))
                  (setf (,context-op (stream-record-context stream)) value)))))
  (def-accessor stream-recording-p context-save-p)
  (def-accessor stream-drawing-p context-draw-p)
  (def-accessor stream-current-output-record context-draw-output-record)
  (def-accessor stream-current-text-output-record context-text-output-record)
  (def-accessor stream-text-cursor context-text-cursor))

(defmethod initialize-instance :after
    ((stream standard-output-recording-stream) &rest args)
  (declare (ignore args))
  (let* ((history (make-instance 'standard-tree-output-history :stream stream))
         (tcursor (make-instance 'standard-text-cursor :sheet stream))
         (context (make-instance 'tls-record-context :stream stream
                                                     :draw-record history
                                                     :text-cursor tcursor)))
    (setf (slot-value stream 'record-context) context
          (slot-value stream 'output-history) history)))

(defmacro with-stream-context (stream-and-options bindings &body body)
  (let* ((context (gensym))
         (binding (loop for (slot-name slot-value) in bindings
                        collect `((,context ',slot-name) ,slot-value))))
    (destructuring-bind (stream) (ensure-list stream-and-options)
      `(let ((,context (stream-record-context ,stream)))
         (slot-dlet ,binding ,@body)))))

(defmacro with-stream-draw-record ((stream record) &body body)
  `(with-stream-context ,stream ((draw-output-record ,record))
     ,@body))

(defmacro with-stream-text-record ((stream record) &body body)
  `(with-stream-context ,stream ((text-output-record ,record))
     ,@body))

(defmacro with-stream-page-layout ((stream &key (cursor nil cursor-supplied-p))
                                   &body body)
  (unless cursor-supplied-p
    (setf cursor `(make-instance 'standard-text-cursor :sheet ,stream)))
  `(with-stream-context ,stream ((text-cursor ,cursor))
     ,@body))

;;; FIXME make this macro a base for CHANGING-SPACE-REQUIREMENTS (w/o frames)
;;; and make it also collect SCROLL-EXTENT requests.
(defvar *changing-stream* nil)
(defvar *deferred-stream-changes* nil)
(defmacro with-deferred-stream-changes ((stream) &body body)
  `(if *changing-stream*
       (progn ,@body)
       (let ((*changing-stream* ,stream)
             (*deferred-stream-changes* '()))
         (unwind-protect (progn ,@body)
           (loop for stream in (remove-duplicates *deferred-stream-changes*) do
             (stream-finish-output stream))))))

(defmethod stream-force-output :around ((stream standard-output-recording-stream))
  (if *changing-stream*
      (push stream *deferred-stream-changes*)
      (call-next-method)))

(defmethod stream-finish-output :around ((stream standard-output-recording-stream))
  (if *changing-stream*
      (push stream *deferred-stream-changes*)
      (call-next-method)))

(defmacro with-stream-history-locked ((stream state) &body body)
  (check-type state keyword)
  (let ((history (gensym)))
    `(let ((,history (stream-output-history ,stream)))
       (with-deferred-stream-changes (,stream)
         (with-recursive-lock-held
             ((output-history-lock ,history) ,state)
           (letf (((output-history-lock-owner ,history) (clim-sys:current-process))
                  ((output-history-lock-state ,history) ,state))
             ,@body))))))

(defun stream-add-dirty-region (self dirty-region)
  (with-stream-history-locked (self :dirty-region)
    (region-union-f (stream-dirty-region self)
                    (if (output-record-p dirty-region)
                        (copy-bounding-rectangle dirty-region)
                        dirty-region))))

(defun stream-pop-dirty-region (self)
  (with-stream-history-locked (self :dirty-region)
    (prog1 (stream-dirty-region self)
      (setf (stream-dirty-region self) +nowhere+))))

;;; 16.4.1 The Output Recording Stream Protocol
(defmethod (setf stream-recording-p) :around
    (recording-p (stream standard-output-recording-stream))
  (let ((old-val (stream-recording-p stream)))
    (unless (eq old-val recording-p)
      (stream-close-text-output-record stream)
      (call-next-method))
    recording-p))

(defmethod invoke-with-output-recording-options
    ((stream standard-output-recording-stream) continuation save draw)
  (if save
      (let* ((context (stream-record-context stream))
             (medium (make-context-medium context)))
        (setf (graphics-state medium) (sheet-medium stream))
        (with-stream-context stream
            ((draw-p draw)
             (save-p save)
             (medium medium))
          (funcall continuation stream)))
      (with-stream-context stream
          ((draw-p draw)
           (save-p save))
        (funcall continuation stream))))


(defmethod stream-replay ((stream standard-output-recording-stream)
                          &optional (region (sheet-visible-region stream)))
  (replay (stream-output-history stream) stream region))

(defun output-record-ancestor-p (ancestor child)
  (or (eq ancestor child)
      (loop for record = child then parent
            for parent = (output-record-parent record)
            when (eq parent nil) do (return nil)
            when (eq parent ancestor) do (return t))))

(defmethod stream-clear-output-history
    ((stream standard-output-recording-stream))
  (with-stream-history-locked (stream :clear)
    (stream-close-text-output-record stream)
    (clear-output-record (stream-output-history stream))
    (clear-map stream)
    (when-let ((cursor (stream-text-cursor stream)))
      (reset-stream-cursor stream cursor))
    (stream-add-dirty-region stream +everywhere+)))

(defmethod stream-add-output-record
    ((stream standard-output-recording-stream) record)
  (let ((parent (stream-current-output-record stream)))
    (if (output-record-ancestor-p (stream-output-history stream) parent)
        (with-stream-history-locked (stream :write)
          (add-output-record record parent)
          (stream-add-dirty-region stream record))
        (add-output-record record parent))))

(defmethod erase-output-record (record (stream standard-output-recording-stream)
                                &optional (errorp t))
  (with-stream-history-locked (stream :erase)
    (let ((parent (output-record-parent record)))
      (cond
        ((output-record-ancestor-p (stream-output-history stream) record)
         (delete-output-record record parent))
        (errorp
         (error "~S is not contained in ~S." record stream))))
    (stream-add-dirty-region stream record)))

;;; 16.4.3. Text Output Recording
(defmethod stream-text-output-record
    ((stream standard-output-recording-stream) text-style)
  (declare (ignore text-style))
  (or (stream-current-text-output-record stream)
      (setf (stream-current-text-output-record stream)
            (stream-open-text-output-record stream))))

(defun stream-advance-one-line (stream cursor soft-newline-p)
  (nest
   (let ((hspace (stream-horizontal-spacing stream))
         (vspace (stream-vertical-spacing stream))))
   (multiple-value-bind (x0 y0) (stream-cursor-initial-position stream))
   ;; (multiple-value-bind (xn yn) (stream-cursor-final-position stream))
   (multiple-value-bind (cx cy) (cursor-position cursor))
   (multiple-value-bind (hsize vsize) (cursor-size cursor))
   (multiple-value-bind (updated-cx updated-cy)
       (ecase (stream-page-direction stream)
         (:top-to-bottom (values x0 (+ cy vsize vspace)))
         (:bottom-to-top (values x0 (- cy vsize vspace)))
         (:left-to-right (values (+ cx hsize hspace) y0))
         (:right-to-left (values (- cx hsize hspace) y0)))
     (setf (cursor-position cursor) (values updated-cx updated-cy))))
  (unless soft-newline-p
    (empty-stream-cursor stream cursor))
  (stream-advance-one-line-hook stream cursor soft-newline-p))

(defun stream-advance-one-line-hook (stream cursor soft-newline-p)
  (when-let ((after-line-break-fn (after-line-break stream)))
    (letf (((stream-text-cursor stream) cursor))
      (funcall after-line-break-fn stream soft-newline-p)
      (stream-close-text-output-record stream))))

;; FIXME wrapping the page to the beginning is a nonsense - the text overlaps
;; with past output and is not readable at all. Figure out the right thing to
;; do, for example (+ (* PAGE-NUM PAGE-OFF) INITIAL-POS). -- jd 2024-11-18
(defun stream-advance-one-page (stream cursor)
  (setf (cursor-position cursor) (stream-cursor-initial-position stream)))

;;; FIXME merge both methods(?)
(defgeneric %rewrap-text-output-record-chunk (stream cursor record chunk)
  (:method (stream cursor record (chunk output-record))
    (let* ((break-line-p (member (stream-end-of-line-action stream) '(:wrap :wrap*)))
           (break-page-p (member (stream-end-of-page-action stream) '(:wrap :wrap*)))
           (fresh-line-p (zerop (stream-text-offset stream cursor)))
           (fresh-page-p (zerop (stream-page-offset stream cursor))))
      (tagbody
         (go :start-line)
       :break-page
         (stream-advance-one-page stream cursor)
         (setf fresh-page-p t)
         (setf fresh-line-p t)
         (go :start-line)
       :break-line
         (stream-advance-one-line stream cursor t)
         (setf fresh-line-p t)
         (go :start-line)
       :start-line
         (multiple-value-bind (dx dy fx fy bx by ex ey eol-p eop-p)
             (stream-cursor-motion stream cursor chunk)
           (when (and eop-p break-page-p (not fresh-page-p))
             (go :break-page))
           (when (and eol-p break-line-p (not fresh-line-p))
             (go :break-line))
           (set-output-record-origin* chunk dx dy)
           (add-output-record chunk record)
           (setf (cursor-position cursor) (values fx fy)
                 (cursor-offset cursor) (values bx by)
                 (cursor-extent cursor) (values ex ey))))))
  (:method (stream cursor record (chunk draw-text-output-record))
    (let* ((break-line-p (member (stream-end-of-line-action stream) '(:wrap :wrap*)))
           (break-page-p (member (stream-end-of-page-action stream) '(:wrap :wrap*)))
           (fresh-page-p (zerop (stream-page-offset stream cursor)))
           (string (slot-value chunk 'string))
           (text-style (graphics-state-text-style chunk))
           (ink (graphics-state-ink chunk))
           (start 0)
           (end (length string))
           (split end))
      (tagbody
         (go :start-line)
       :break-page
         (stream-advance-one-page stream cursor)
         (setf fresh-page-p t)
         (go :start-line)
       :break-line
         (stream-advance-one-line stream cursor t)
         (setf start split split end)
         (go :start-line)
       :start-line
         (multiple-value-bind (dx dy fx fy bx by ex ey eol-p eop-p)
             (stream-cursor-motion stream cursor string :start start :end end
                                                        :text-style text-style)
           (when (and eop-p break-page-p (not fresh-page-p))
             (go :break-page))
           (when (and eol-p break-line-p)
             (setf split (stream-text-break stream cursor text-style
                                            string start end)))
           (let* ((sub-string (create-string string start split))
                  (sub-record (make-draw-text-output-record stream sub-string dx dy
                                                            text-style ink)))
             (add-output-record sub-record record))
           (setf (cursor-position cursor) (values fx fy)
                 (cursor-offset cursor) (values bx by)
                 (cursor-extent cursor) (values ex ey))
           (when (and split (/= split end))
             (setf fresh-page-p nil)
             (go :break-line)))))))

;;; This function takes TEXT-OUTPUT-RECORD-CHUNKS and depending on the record's
;;; start position, stream margins and overflow actions splits them into parts
;;; that are replayed onto the stream. These parts are added to the text output
;;; record as children. The end cursor position is updated. -- jd 2024-11-15
(defun rewrap-text-output-record (stream record)
  (let* ((cursor (record-text-cursor record))
         (chunks (text-output-record-chunks record)))
    (domap (chunk chunks)
      (multiple-value-bind (dx dy fx fy bx by ex ey)
          (if (typep chunk 'gs-text-style-mixin)
              (stream-cursor-motion stream cursor " " :text-style
                                    (graphics-state-text-style chunk))
              (stream-cursor-motion stream cursor chunk))
        (declare (ignore dx dy fx fy))
        (setf (cursor-offset cursor) (values bx by)
              (cursor-extent cursor) (values ex ey))))
    (domap (chunk chunks)
      (%rewrap-text-output-record-chunk stream cursor record chunk))))

;;; This function either creates a new one, or reopens the last output record.
;;; Currently we simply create a new one.
(defgeneric stream-open-text-output-record (stream)
  (:method ((stream standard-output-recording-stream))
    (make-instance 'standard-text-displayed-output-record :stream stream)))

(defmethod stream-close-text-output-record ((stream standard-output-recording-stream))
  ;; INV STREAM-CURRENT-TEXT-OUTPUT-RECORD is thread-local. -- jd 2024-11-15
  (when-let ((record (stream-current-text-output-record stream)))
    (setf (stream-current-text-output-record stream) nil)
    (with-stream-history-locked (stream :write-text)
      (let ((stream-cursor (stream-text-cursor stream))
            (record-cursor (record-text-cursor record)))
        (update-cursor record-cursor stream-cursor)
        (tracking-cursor (stream record-cursor record)
          (rewrap-text-output-record stream record))
        (update-cursor stream-cursor record-cursor)
        (tree-recompute-extent record)
        (stream-add-output-record stream record)
        (when (text-output-record-closed record)
          (stream-advance-one-line stream stream-cursor nil)))
      (setf (needs-scroll-p stream) t)
      t)))

(defmacro with-stream-text-output-record ((record stream) &body body)
  (check-type stream symbol)
  `(let ((,record (stream-text-output-record ,stream nil)))
     ,@body))

(defun stream-add-record-output (stream record)
  (with-stream-text-output-record (text-record stream)
    (add-object-to-text-record text-record record)))

(defmethod stream-add-character-output ((stream standard-output-recording-stream)
                                        character text-style width height baseline)
  (assert (char/= character #\newline))
  (with-stream-text-output-record (text-record stream)
    (add-character-output-to-text-record
     text-record character text-style width height baseline)))

(defmethod stream-add-string-output ((stream standard-output-recording-stream)
                                     string start end text-style
                                     width height baseline)
  (with-stream-text-output-record (text-record stream)
    (add-string-output-to-text-record
     text-record string start end text-style width height baseline)))

(defun stream-add-newline-output (stream soft-newline-p)
  (declare (ignore soft-newline-p))
  (with-stream-text-output-record (text-record stream)
    (add-newline-output-to-text-record text-record)
    (stream-close-text-output-record stream)))

(defun stream-add-text-output (stream string start end)
  (let ((force-p nil))
    (with-stream-text-output-record (text-record stream)
      (declare (ignore text-record))
      (let* ((text-style (medium-text-style stream))
             (text-style-height (text-style-height text-style stream))
             (text-style-base-y (text-style-ascent text-style stream))
             (seg-start start)
             (end (or end (length string))))
        (flet ((stream-add-string-output (string start end)
                 ;; XXX ADD-STRING-OUTPUT-TO-TEXT-RECORD does not care about
                 ;; width and cursor is updated on the result of calling
                 ;; STREAM-CURSOR-MOTION , so we could skip this computation.
                 ;; -- jd 2024-11-15
                 (let ((width (stream-string-width stream string
                                                   :text-style text-style
                                                   :start start :end end)))
                   (stream-add-string-output
                    stream string start end text-style
                    width text-style-height text-style-base-y))))
          (loop for i from start below end do
            (case (char string i)
              (#\newline
               (stream-add-string-output string seg-start i)
               (stream-add-newline-output stream nil)
               (setq seg-start (1+ i))
               (setf force-p t))
              (#\tab
               (stream-add-string-output string seg-start i)
               (stream-add-string-output "        " 0 8)
               (setq seg-start (1+ i)))))
          (stream-add-string-output string seg-start end))))
    (when force-p
      (force-output stream))))


;;; 16.4.4. Output Recording Utilities

(defmethod invoke-with-new-output-record
    ((stream output-recording-stream) continuation record-type
     &rest initargs &key parent)
  (orf record-type 'standard-sequence-output-record)
  (with-keywords-removed (initargs (:parent))
    (let ((new-record (apply #'make-instance record-type initargs)))
      (with-output-recording-options (stream :record t)
        (stream-close-text-output-record stream)
        (with-stream-context stream
            ((draw-output-record new-record))
          (funcall continuation stream new-record)
          (stream-close-text-output-record stream))
        (if parent
            (add-output-record new-record parent)
            (stream-add-output-record stream new-record)))
      new-record)))

(defmethod invoke-with-output-to-output-record
    ((stream output-recording-stream) continuation record-type
     &rest initargs &key parent)
  (orf record-type 'standard-sequence-output-record)
  (with-keywords-removed (initargs (:parent))
    (let ((new-record (apply #'make-instance record-type initargs)))
      (with-output-recording-options (stream :record t :draw nil)
        (with-stream-context stream
          ((draw-output-record new-record)
           (text-output-record nil))
          (with-pristine-viewport (stream)
            (funcall continuation stream new-record)))
        (when parent
          (add-output-record new-record parent)))
      new-record)))

(defmethod invoke-with-output-to-pixmap
    ((sheet output-recording-stream) cont &key width height)
  (unless (and width height)
    ;; What to do when only width or height are given?  And what's the meaning
    ;; of medium-var? -- rudi 2005-09-05
    ;;
    ;; We default WIDTH or HEIGHT to provided values. The output is clipped to a
    ;; rectactangle [0 0 (or width max-x) (height max-y)]. We record the output
    ;; only to learn about dimensions - it is not replayed because the medium
    ;; can't be expected to work with this protocol. To produce the output we
    ;; invoke the continuation again. -- jd 2022-03-16
    (if (output-recording-stream-p sheet)
        (with-bounding-rectangle* (:x2 max-x :y2 max-y)
            (invoke-with-output-to-output-record sheet
                                                 (lambda (sheet record)
                                                   (declare (ignore record))
                                                   (funcall cont sheet))
                                                 'standard-sequence-output-record)
          (setf width (or width max-x)
                height (or height max-y)))
        (error "WITH-OUTPUT-TO-PIXMAP: please provide :WIDTH and :HEIGHT.")))
  (let* ((port (port sheet))
         (pixmap (allocate-pixmap sheet width height))
         (pixmap-medium (make-medium port sheet))
         (drawing-plane (make-rectangle* 0 0 width height)))
    (degraft-medium pixmap-medium port sheet)
    (letf (((medium-drawable pixmap-medium) pixmap)
           ((medium-clipping-region pixmap-medium) drawing-plane)
           ((medium-background pixmap-medium) +transparent-ink+))
      (medium-clear-area pixmap-medium 0 0 width height)
      (funcall cont pixmap-medium)
      pixmap)))

(macrolet
    ((frob (name)
       `(progn
          (defmethod ,name ((self standard-output-recording-stream))
            (cond
              ((stream-recording-p self)
               (,name (sheet-medium self)))
              ((stream-drawing-p self)
               (with-sheet-medium (medium self)
                 (,name medium)))
              (t
               (call-next-method))))
          (defmethod (setf ,name) (value (self standard-output-recording-stream))
            (cond
              ((stream-recording-p self)
               (setf (,name (sheet-medium self)) value))
              ((stream-drawing-p self)
               (with-sheet-medium (medium self)
                 (setf (,name medium) value)))
              (t
               (call-next-method)))))))
  (frob medium-transformation)
  (frob medium-region)
  (frob medium-ink)
  (frob medium-line-style)
  (frob medium-text-style)
  (frob medium-line-direction)
  (frob medium-page-direction)
  (frob medium-foreground)
  (frob medium-background)
  (frob medium-default-text-style))

;;; Pixmaps are port-dependent and specialization is on medium.
(defmethod allocate-pixmap
    ((self standard-output-recording-stream) width height)
  (with-output-recording-options (self :record nil :draw t)
    (call-next-method)))

(defmethod copy-from-pixmap (pixmap pixmap-x pixmap-y width height
                             (self standard-output-recording-stream)
                             sheet-x sheet-y)
  (with-output-recording-options (self :record nil :draw t)
    (call-next-method)))

(defmethod copy-to-pixmap ((self standard-output-recording-stream)
                           sheet-x sheet-y width height
                           &optional pixmap (pixmap-x 0) (pixmap-y 0))
  (with-output-recording-options (self :record nil :draw t)
    (call-next-method)))

;;; Self-copy
(defmethod copy-area ((self standard-output-recording-stream) src-x src-y
                      copy-ws copy-hs dst-x dst-y)
  (with-output-recording-options (self :record nil :draw t)
    (call-next-method)))

(defmethod medium-copy-area ((self standard-output-recording-stream) src-x src-y
                             copy-ws copy-hs destination dst-x dst-y)
  (with-output-recording-options (self :record nil :draw t)
    (call-next-method)))

(defmethod medium-copy-area (source src-x src-y copy-ws copy-hs
                             (self standard-output-recording-stream) dst-x dst-y)
  (with-output-recording-options (self :record nil :draw t)
    (call-next-method)))

(defmethod medium-copy-area ((self1 standard-output-recording-stream)
                             src-x src-y copy-ws copy-hs
                             (self2 standard-output-recording-stream)
                             dst-x dst-y)
  (with-output-recording-options (self1 :record nil :draw t)
    (with-output-recording-options (self2 :record nil :draw t)
      (call-next-method))))

;;; Text metrics are heavily dependent on the port, so we must use the
;;; specialized medium allocated by it. -- jd 2024-11-13

(defmethod text-bounding-rectangle*
    ((self standard-output-recording-stream) string &rest args)
  (let ((new-args (append args `(:text-style ,(medium-text-style self)))))
    (with-output-recording-options (self :record nil :draw t)
      (apply #'call-next-method self string new-args))))

(defmethod text-size
    ((self standard-output-recording-stream) string &rest more)
  (declare (ignore string more))
  (with-output-recording-options (self :record nil :draw t)
    (call-next-method)))

(defmethod text-style-ascent
    (ts (self standard-output-recording-stream))
  (with-output-recording-options (self :record nil :draw t)
    (call-next-method)))

(defmethod text-style-descent
    (ts (self standard-output-recording-stream))
  (with-output-recording-options (self :record nil :draw t)
    (call-next-method)))

(defmethod text-style-width
    (ts (self standard-output-recording-stream))
  (with-output-recording-options (self :record nil :draw t)
    (call-next-method)))

(defmethod text-style-height
    (ts (self standard-output-recording-stream))
  (with-output-recording-options (self :record nil :draw t)
    (call-next-method)))

(defmethod text-style-character-width
    (ts (self standard-output-recording-stream) char)
  (with-output-recording-options (self :record nil :draw t)
    (call-next-method)))

(defmethod invoke-with-clipping-region
    ((sheet output-recording-stream) continuation region)
  (declare (ignore continuation))
  (if (stream-recording-p sheet)
      (with-sheet-medium (medium sheet)
        (let* ((tr (medium-transformation medium))
               (clip (transform-region tr region)))
          (with-new-output-record (sheet 'clipping-output-record record
                                         :clipping-region clip)
            (call-next-method)
            (setf (rectangle-edges* record)
                  (bounding-rectangle*
                   (region-intersection (sheet-region sheet) clip))))))
      (call-next-method)))

;;; This may look like a good idea but it is not. With this method it is
;;; impossible to have feedback when recording. -- jd 2025-02-07
#+ (or)
(defmethod invoke-with-output-buffered
    ((self output-recording-stream) continuation &optional buffered-p)
  (flet ((cont ()
           (if (stream-recording-p self)
               (with-new-output-record (self)
                 (funcall continuation))
               (funcall continuation))))
    (call-next-method self #'cont buffered-p)))

(defmethod invoke-with-output-buffered
    ((self output-recording-stream) continuation &optional buffered-p)
  (let ((medium (with-output-recording-options (self :record nil :draw t)
                  (sheet-medium self))))
    (invoke-with-output-buffered medium continuation buffered-p)))

(defmethod medium-buffering-output-p ((self standard-output-recording-stream))
  (with-output-recording-options (self :record nil :draw t)
    (call-next-method)))

;;; FIXME: Change things so the rectangle below is only drawn in response
;;;        to explicit repaint requests from the user, not exposes from X.
;;; FIXME: Use DRAW-DESIGN, that is fix DRAW-DESIGN.
(defmethod handle-repaint ((stream output-recording-stream) region)
  (let ((region-1 (region-intersection region (sheet-region stream))))
    (unless (region-equal region-1 +nowhere+)
      (stream-replay stream region-1))))

(defmethod repaint-sheet :around ((stream output-recording-stream) region)
  (with-stream-history-locked (stream :repaint-sheet)
    (with-output-recording-options (stream :record nil :draw t)
      (call-next-method))))

;;; Check an invariant to avoid deadlocks with the mirror.
(defmethod dispatch-repaint :before ((stream output-recording-stream) region)
  (assert (not (eq (output-history-lock-owner
                    (stream-output-history stream))
                   (clim-sys:current-process)))))

(defmethod scroll-extent :around ((stream output-recording-stream) x y)
  (declare (ignore x y))
  (when (stream-drawing-p stream)
    (call-next-method)))
