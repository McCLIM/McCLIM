;;; ---------------------------------------------------------------------------
;;;   License: LGPL-2.1+ (See file 'Copyright' for details).
;;; ---------------------------------------------------------------------------
;;;
;;;  (c) Copyright 1998-2001 by Michael McDonald <mikemac@mikemac.com>
;;;  (c) Copyright 2000,2014 by Robert Strandh <robert.strandh@gmail.com>
;;;  (c) Copyright 2016-2024 by Daniel Kochmański <daniel@turtleware.eu>
;;;
;;; ---------------------------------------------------------------------------
;;;

(in-package #:clim-internals)

;;; Standard-Output-Stream class
(defclass standard-output-stream (output-stream) ())


;;; Standard-Extended-Output-Stream class

(defclass standard-extended-output-stream (extended-output-stream
                                           standard-output-stream
                                           standard-page-layout
                                           filling-output-mixin
                                           standard-output-recording-stream)
  ((view :initarg :default-view :accessor stream-default-view)
   (vspace :initarg :vertical-spacing :accessor stream-vertical-spacing)
   (hspace :initarg :horizontal-spacing :accessor stream-horizontal-spacing)
   (end-of-line-action :accessor stream-end-of-line-action)
   (end-of-page-action :accessor stream-end-of-page-action))
  (:default-initargs
   :foreground +black+ :background +white+ :text-style *default-text-style*
   :vertical-spacing 2 :horizontal-spacing 2
   :default-view +textual-view+))

(defmethod stream-cursor-position ((stream standard-extended-output-stream))
  (cursor-position (stream-text-cursor stream)))

(defmethod* (setf stream-cursor-position) (x y (stream standard-extended-output-stream))
  (stream-close-text-output-record stream)
  (setf (cursor-position (stream-text-cursor stream)) (values x y)))

(defmethod stream-baseline ((sheet standard-extended-output-stream))
  (let ((cursor (stream-text-cursor sheet)))
    (ecase (stream-line-direction sheet)
      ((:left-to-right :right-to-left) (cursor-offset-y cursor))
      ((:top-to-bottom :bottom-to-top) (cursor-offset-x cursor)))))

(defmethod (setf stream-baseline) (baseline (sheet standard-extended-output-stream))
  (let ((cursor (stream-text-cursor sheet)))
   (ecase (stream-line-direction sheet)
     ((:left-to-right :right-to-left) (setf (cursor-offset-y cursor) baseline))
     ((:top-to-bottom :bottom-to-top) (setf (cursor-offset-x cursor) baseline)))))

(defmethod stream-set-cursor-position ((stream standard-extended-output-stream) x y)
  (setf (stream-cursor-position stream) (values x y)))

(defmethod stream-increment-cursor-position
    ((stream standard-extended-output-stream) dx dy)
  (let ((cursor (stream-text-cursor stream))
        (dx (or dx 0))
        (dy (or dy 0)))
   (multiple-value-bind (x y) (cursor-position cursor)
     (setf (cursor-position cursor)
           (values (+ x dx) (+ y dy))))))

(defun reset-stream-cursor (stream cursor)
  (setf (cursor-position cursor) (stream-cursor-initial-position stream)
        (cursor-offset cursor) (values 0 0)
        (cursor-extent cursor) (values 0 0)))

(defun empty-stream-cursor (stream cursor)
  (declare (ignore stream))
  (setf (cursor-offset cursor) (values 0 0)
        (cursor-extent cursor) (values 0 0)))

(defmethod note-sheet-grafted :after ((stream standard-extended-output-stream))
  (reset-stream-cursor stream (stream-text-cursor stream)))


(defun seos-update-scroll (stream)
  (let ((lscroll (eq (stream-end-of-line-action stream) :scroll))
        (pscroll (eq (stream-end-of-page-action stream) :scroll))
        (cursor (stream-text-cursor stream)))
    (cond
      ((and lscroll pscroll) (scroll-extent* stream cursor))
      (lscroll               (scroll-extent/line stream cursor))
      (pscroll               (scroll-extent/page stream cursor)))))

(defgeneric seos-finish-output (stream)
  (:method :around ((stream immediate-repainting-mixin))
    (clim-sys:with-lock-held ((immediate-repainiting-mixin-lock stream) :finish)
      (call-next-method)))
  (:method ((stream standard-extended-output-stream))
    (let (layout-p scroll-p region)
      (with-stream-history-locked (stream :read)
        (setf layout-p (needs-layout-p stream)
              scroll-p (needs-scroll-p stream)
              (needs-layout-p stream) nil
              (needs-scroll-p stream) nil
              region (stream-pop-dirty-region stream)))
      (when layout-p
        (change-space-requirements stream))
      (when scroll-p
        (seos-update-scroll stream))
      (unless (region-equal region +nowhere+)
        (dispatch-repaint stream region)))
    (values)))

(defmethod stream-force-output :after ((stream standard-extended-output-stream))
  (stream-close-text-output-record stream)
  (seos-finish-output stream)
  (medium-force-output stream))

(defmethod stream-finish-output :after ((stream standard-extended-output-stream))
  (stream-close-text-output-record stream)
  (seos-finish-output stream)
  (medium-finish-output stream))

(defgeneric stream-write-object (stream object)
  (:method ((stream standard-extended-output-stream) object)
    (stream-add-record-output stream object)
    object))

(defgeneric stream-write-vector (stream vector start end)
  (:method ((stream standard-extended-output-stream) vector start end)
    (loop for index from start below end do
      (stream-add-record-output stream (aref vector index)))
    vector))

(defmethod stream-write-char ((stream standard-extended-output-stream) char)
  (stream-add-text-output stream (string char) 0 1)
  char)

(defmethod stream-write-string ((stream standard-extended-output-stream) string
                                &optional (start 0) end)
  (stream-add-text-output stream string start end)
  string)


(defmethod stream-character-width ((stream standard-extended-output-stream) char
                                   &key (text-style (medium-text-style stream)))
  (text-style-character-width text-style stream char))

(defmethod stream-string-width ((stream standard-extended-output-stream) string
                                &key (start 0) (end nil)
                                     (text-style (medium-text-style stream)))
  (multiple-value-bind (total-width total-height final-x)
      (text-size stream string :text-style text-style
                               :start start :end end)
    (declare (ignore total-height))
    (values final-x total-width)))

(defmethod stream-text-margin ((stream standard-extended-output-stream))
  (bounding-rectangle-max-x (stream-page-region stream)))

(defmethod (setf stream-text-margin) (margin (stream standard-extended-output-stream))
  (setf (stream-text-margins stream)
        (if margin
            `(:right (:absolute ,margin))
            `(:right (:relative 0)))))

;;; FIXME this is incorrect -- CLIM II specifies this function to measure the
;;; line "from the baseline of the text-style to its ascent". On the other hand
;;; such definition doesn't seem to have much utility. -- jd 2024-02-06
(defmethod stream-line-height ((stream standard-extended-output-stream)
                               &key (text-style (medium-text-style stream)))
  (+ (text-style-height text-style stream)
     (stream-vertical-spacing stream)))

(defmethod stream-line-width ((stream standard-extended-output-stream))
  (bounding-rectangle-width (stream-page-region stream)))

(defmethod stream-line-column ((stream standard-extended-output-stream))
  (let ((line-width (- (stream-cursor-position stream)
                       (stream-cursor-initial-position stream))))
    (if (minusp line-width)
        nil
        ;; Some PPRINT implemenations require STREAM-LINE-COLUMN to return an
        ;; integer here. May be worth revising in the future. -- jd 2021-12-18
        (floor (/ line-width (stream-character-width stream #\M))))))

(defmethod stream-start-line-p ((stream standard-extended-output-stream))
  (and (zerop (stream-text-offset stream (stream-text-cursor stream)))
       (let ((record (stream-current-text-output-record stream)))
         (or (null record)
             (emptyp (slot-value record 'records))))))

(defmethod beep (&optional medium)
  (case medium
    ((nil t)
     (when (sheetp *standard-output*)
       (medium-beep *standard-output*)))
    (otherwise
     (medium-beep medium))))


;;; Backend part of the output destination mechanism
;;;
;;; See clim-core/commands.lisp for the "user interface" part.

(defgeneric invoke-with-standard-output (continuation destination)
  (:documentation
   "Call CONTINUATION (with no arguments) with *STANDARD-OUTPUT*
rebound according to DESTINATION."))

(defmethod invoke-with-standard-output (continuation (destination null))
  ;; Call CONTINUATION without rebinding *STANDARD-OUTPUT* at all.
  (funcall continuation))

(defclass output-destination ()
  ())

(defclass stream-destination (output-destination)
  ((destination-stream :accessor destination-stream
                       :initarg :destination-stream)))

(defmethod invoke-with-standard-output
    (continuation (destination stream-destination))
  (let ((*standard-output* (destination-stream destination)))
    (funcall continuation)))

(defclass file-destination (output-destination)
  ((file :reader destination-file :initarg :file)))

(defmethod destination-element-type ((destination file-destination))
  :default)

(defmethod invoke-with-standard-output
    (continuation (destination file-destination))
  (with-open-file (*standard-output* (destination-file destination)
                                     :element-type (destination-element-type
                                                    destination)
                                     :direction :output
                                     :if-exists :supersede)
    (funcall continuation)))

(defparameter *output-destination-types*
  '(("Stream" stream-destination)))

(defun register-output-destination-type (name class-name)
  (let ((class (find-class class-name nil)))
    (cond ((null class)
           (error "~@<~S is not the name of a class.~@:>" class-name))
          ((not (subtypep class #1='output-destination))
           (error "~@<~A is not a subclass of ~S.~@:>" class #1#))))
  (pushnew (list name class-name) *output-destination-types* :test #'equal))
