;;; ---------------------------------------------------------------------------
;;;   License: LGPL-2.1+ (See file 'Copyright' for details).
;;; ---------------------------------------------------------------------------
;;;
;;;  (c) Copyright 2002 by Michael McDonald <mikemac@mikemac.com>
;;;  (c) Copyright 2002-2004 by Tim Moore <moore@bricoworks.com>
;;;  (c) Copyright 2014 by Robert Strandh <robert.strandh@gmail.com>
;;;  (c) Copyright 2021 by Daniel Kochmański <daniel@turtleware.eu>
;;;
;;; ---------------------------------------------------------------------------
;;;
;;; This file contains the implementation of the redisplay including computation
;;; of the difference set and the macro UPDATING-OUTPUT.
;;;
(in-package #:clim-internals)

;;; Incremental Redisplay Theory of Operation
;;;
;;; Incremental redisplay compares the tree of output records before and after
;;; calling REDISPLAY and updates those parts of the screen that are different.
;;; UPDATING-OUTPUT forms in the code create UPDATING-OUTPUT-RECORDs in the record
;;; tree. These records hold the before and after snapshots of the tree.  When the
;;; display code is first run, the bodies of all the UPDATING-OUTPUT forms are
;;; captured as closures.
;;;
;;; Redisplay proceeds thus:
;;;
;;; All the updating output records are visited. Their state is changed to
;;; :UPDATING and the OLD-CHILDREN slot is set to the current children.
;;;
;;; The closure of the root updating output record is called. None of the closures
;;; the child updating output records are called because any free variables
;;; captured in the UPDATING-OUTPUT forms need to see the fresh bindings from this
;;; run of the code. As UPDATING-OUTPUT forms are encountered, several things can
;;; happen:
;;;
;;; * The cache value of the form compares to the value stored in the record. The
;;; record, and all the updating output records below it, are marked :clean. The
;;; body of UPDATING-OUTPUT isn't run.
;;;
;;; * The cache value doesn't compare. The record is marked :UPDATED, and the body
;;; is run.
;;;
;;; * There isn't an existing UPDATING-OUTPUT-RECORD for this UPDATING-OUTPUT
;;; form. One is created in state :UPDATED. The body is run.
;;;
;;; Before the top level UPDATING-OUTPUT closure returns, various output records
;;; in the history tree might be mutated e.g., be moved. The most common case of
;;; this is in table layout, where the records for each cell are first created
;;; and then assigned a final location based on the dimensions of the table. But
;;; these nodes may be children of an updating output record that has been
;;; marked :CLEAN. Therefore, they have to be treated specially so that the rest
;;; of incremental redisplay will consider them and not leave the screen in a
;;; trashed state. An around method on (SETF OUTPUT-RECORD-POSITION) for display
;;; records checks if incremental redisplay is in progress; if so, it stores the
;;; mutated record in its closest parent UPDATING-OUTPUT record (if any). If
;;; that parent is :CLEAN then it and any other clean parent updating output
;;; records are marked as :UPDATED and the record itself is marked as :MOVED.
;;;
;;; Next, COMPUTE-DIFFERENCE-SET compares the old and new trees. Output records
;;; may be removed, added or moved. Their region union must be erased and then
;;; replayed from the history to ensure the correctness. COMPUTE-DIFFERENCE-SET
;;; compares all display output records that are the record descendants.
;;;
;;; Finally, the old tree is walked. All updating output records in the state
;;; :UPDATING that were not visited at all and thus are deleted from their
;;; parent caches.
;;;
;;;
;;; Problems / Future work
;;;
;;; COMPUTE-DIFFERENCE-SET is very expensive because we compare the old history
;;; and the new history to compute an exact difference. Unless drawing heavily
;;; dominates computations (i.e a plotter), it makes more sense to just record
;;; the bounding rectangle before and after redisplay and dispatch a repaint.
;;;
;;; It is not possible to concurrently redisplay and replay the stream history,
;;; because the updating output record has a position that is relative to the
;;; stream and not to the output record parent. If we store relative coordinates
;;; and compute stream coordinates lazily, then we can move compound output
;;; records for free, and present old children for replay and new for redisplay.
;;;
;;; vv Below are past "Future" entries I don't understand.. -- jd 2024-11-26
;;;
;;; The complete traversals of the output history tree could be avoided by
;;; keeping a generation number in the updating output record and updating
;;; that everytime the node is visited.
;;;
;;; The search for equal display nodes is expensive in part because we have no
;;; spatially organized data structure.

(defvar *current-updating-output* nil)

(defmethod compute-new-output-records ((record standard-updating-output-record) stream)
  (with-output-recording-options (stream :record t :draw nil)
    (with-stream-history-locked (stream :compute)
      (compute-new-output-records/mark-updating record stream)
      (compute-new-output-records/invoke-displayer record stream))
    (compute-new-output-records/drop-stalled record stream)))

(defun compute-new-output-records/mark-updating (record stream)
  (declare (ignore stream))
  (map-over-updating-output
   #'(lambda (r)
       (setf (output-record-dirty r) :updating)
       (when-let ((sub-record (sub-record r)))
         (setf (old-children r) sub-record)
         (setf (rectangle-edges* (old-bounds r))
               (rectangle-edges* sub-record))))
   record
   nil))

(defun compute-new-output-records/drop-stalled (record stream)
  (declare (ignore stream))
  (map-over-updating-output
   #'(lambda (r)
       (when (eq (output-record-dirty r) :updating)
         (delete-from-map (parent-cache r)
                          (output-record-unique-id r)
                          (output-record-id-test r))))
   record
   t))

(defun compute-new-output-records/update-parent (record stream)
  ;; Don't add this record repeatedly to a parent updating-output-record.
  (let ((old-parent (output-record-parent record))
        (new-parent (stream-current-output-record stream)))
    (unless (eq old-parent new-parent)
      (delete-output-record record old-parent)
      (add-output-record record new-parent))))

(defun compute-new-output-records/invoke-displayer (record stream)
  "Like compute-new-output-records with an explicit displayer function."
  (check-type record standard-updating-output-record)
  (stream-add-dirty-region stream record)
  (when-let ((sub-record (sub-record record)))
    (delete-output-record sub-record record))
  ;; We need to ensure that there is no open text line so that
  ;; WITH-NEW-OUTPUT-RECORD won't add the line as a SUB-RECORD when the line.
  (stream-close-text-output-record stream)
  (let ((cursor (stream-text-cursor stream)))
    (tracking-cursor (stream cursor record)
      (with-stream-draw-record (stream record)
        (with-new-output-record (stream)
          (funcall (output-record-displayer record) stream)))))
  (setf (output-record-dirty record) :updated))

(defconstant +fixnum-bits+ (integer-length most-positive-fixnum))

(declaim (inline hash-coords))
(defun hash-coords (x1 y1 x2 y2)
  (declare (type coordinate x1 y1 x2 y2))
  (let ((hash-val 0))
      (declare (type fixnum hash-val))
      (labels ((rot4 (val)
                 (dpb (ldb (byte 4 0) val)
                      (byte 4 (- +fixnum-bits+ 4 1))
                      (ash val -4)))
               (mix-it-in (val)
               (let ((xval (sxhash val)))
                 (declare (type fixnum xval))
                 (when (minusp val)
                   (setq xval (rot4 xval)))
                 (setq hash-val (logxor (rot4 hash-val) xval)))))
        (declare (inline rot4 mix-it-in))
        (mix-it-in x1)
        (mix-it-in y1)
        (mix-it-in x2)
        (mix-it-in y2)
        hash-val)))

(defgeneric output-record-hash (record)
  (:documentation "Produce a value that can be used to hash the output record
in an equalp hash table")
  (:method  ((record standard-bounding-rectangle))
    (slot-value record 'coordinates))
  (:method ((record output-record))
    (with-bounding-rectangle* (x1 y1 x2 y2) record
      (hash-coords x1 y1 x2 y2))))

(defmethod compute-difference-set ((record standard-updating-output-record)
                                   &optional (check-overlapping t))
  (let ((old-table (make-hash-table :test #'equalp))
        (new-table (make-hash-table :test #'equalp))
        (all-table (make-hash-table)))
    (collect (old-records new-records)
      (flet ((collect-1 (record set)
               (setf (gethash record all-table) set)
               (ecase set
                 (:old
                  (old-records record)
                  (push record (gethash (output-record-hash record) old-table)))
                 (:new
                  (new-records record)
                  (push record (gethash (output-record-hash record) new-table))))))
        (labels ((gather-records (record set)
                   (typecase record
                     (displayed-output-record
                      (collect-1 record set))
                     (updating-output-record
                      (ecase (output-record-dirty record)
                        ((:clean :moved)
                         (collect-1 record set))
                        ((:updating :updated)
                         (let ((sub (ecase set
                                      (:old (old-children record))
                                      (:new (sub-record record)))))
                           (map-over-output-records #'gather-records sub
                                                    nil nil set)))))
                     (otherwise
                      (map-over-output-records #'gather-records record
                                               nil nil set)))))
          (gather-records record :old)
          (gather-records record :new)))
      (collect (erases moves draws)
        (flet ((add-record (rec set)
                 (if (updating-output-record-p rec)
                     (ecase (output-record-dirty rec)
                       (:moved
                        ;; If we ever use the new position for something
                        ;; then the specification says it stick it here.
                        (moves (list rec (old-bounds rec) #|new-position|#)))
                       (:clean
                        ;; no need to redraw clean records.
                        nil)
                       #+ (or)
                       ((:updating :updated)
                        ;; UPDATING-OUTPUT-RECORDs with the state :UPDATED
                        ;; are not collected (their children are collected).
                        (error "Updated recoreds are not collected!")))
                     (flet ((match-record (r) (output-record-equal rec r)))
                       (let* ((hash (output-record-hash rec))
                              ;; The bounding rectangle is always the same.
                              (entry (list rec (bounding-rectangle rec)))
                              (old-p (some #'match-record (gethash hash old-table)))
                              (new-p (some #'match-record (gethash hash new-table))))
                         (cond ((null new-p) (erases entry))
                               ((null old-p) (draws entry))
                               ;; Siblings might have been reordered so we
                               ;; need to "move it" in place.
                               ;; Don't add the same output record twice  v
                               (t (when (and check-overlapping (eq set :new))
                                    (moves entry)))))))))
          (maphash #'add-record all-table))
        (if (null check-overlapping)
            (list (erases) (moves) (draws)      nil    nil)
            (list      nil     nil (draws) (erases) (moves)))))))

(defvar *no-unique-id* (cons nil nil))

;;; FIXME these are implemented without puting much thought into it. Sorry!
(progn
  (defmethod find-child-output-record
      ((cache updating-output-map-mixin) use-old-elements record-type
       &key unique-id unique-id-test)
    (declare (ignore use-old-elements record-type))
    (get-from-map cache unique-id unique-id-test))

  (defmethod find-cached-output-record
      ((cache updating-output-map-mixin) use-old-elements record-type
       &key unique-id unique-id-test &allow-other-keys)
    (declare (ignore use-old-elements record-type))
    (get-from-map cache unique-id unique-id-test))

  (defmethod output-record-contents-ok ((record updating-output-record))
    ;; This is the test we actually use in INVOKE-UPDATING-OUTPUT. Note that we
    ;; refer CACHE-VALUE that is not passed to this function.
    #+ (or)
    (funcall (output-record-cache-test record)
             cache-value
             (output-record-cache-value record))
    ;; Technically the cache test may do something useful even when the cache
    ;; value does not change... I guess?
    #+ (or)
    (funcall (output-record-cache-test record)
             (output-record-cache-value record)
             (output-record-cache-value record))
    ;; Alternatively this may be a slot that is assigned when the cache value
    ;; changes but we skip the redisplay.. Why though? Let's stub it for now:
    t)

  ;; ditto
  (defmethod recompute-contents-ok ((record updating-output-record))
    (output-record-contents-ok record))

  ;; KLUDGE ADD-TO-MAP lazily initializes the test.
  (defmethod cache-output-record ((cache updating-output-map-mixin)
                                  (child updating-output-record)
                                  unique-id)
    (add-to-map cache child unique-id (tester-function cache)))

  ;; USE-OLD-ELEMENTS?
  (defmethod decache-output-record ((cache updating-output-map-mixin)
                                    (child updating-output-record)
                                    use-old-elements)
    (declare (ignore use-old-elements))
    (delete-from-map cache
                     (output-record-unique-id child)
                     (tester-function cache)))

  (defmethod decache-output-record ((cache updating-output-map-mixin)
                                    unique-id ;dwim
                                    use-old-elements)
    (declare (ignore use-old-elements))
    (delete-from-map cache unique-id (tester-function cache))))

(defmethod invoke-updating-output :around
    ((stream updating-output-stream-mixin) continuation record-type
     unique-id id-test cache-value cache-test &key &allow-other-keys)
  (multiple-value-prog1
      (with-stream-history-locked (stream :updating)
        (call-next-method))
    (force-output stream)))

(defmethod invoke-updating-output ((stream updating-output-stream-mixin)
                                   continuation
                                   record-type
                                   unique-id id-test cache-value cache-test
                                   &key (fixed-position nil) (all-new nil)
                                        (parent-cache nil))
  (setf parent-cache (or parent-cache *current-updating-output* stream))
  (when (or (eq unique-id *no-unique-id*)
            (null unique-id))
    (setq unique-id (gensym "UNIQUE-ID")))
  (let ((record (get-from-map parent-cache unique-id id-test)))
    (if (null record)
        (with-new-output-record
            (stream record-type *current-updating-output*
                    :unique-id unique-id
                    :id-test id-test
                    :cache-value cache-value
                    :cache-test cache-test
                    :fixed-position fixed-position
                    :displayer continuation
                    :parent-cache parent-cache
                    :stream stream
                    :parent-updating-output *current-updating-output*)
          (setq record *current-updating-output*)
          (compute-new-output-records/invoke-displayer record stream)
          (add-to-map parent-cache record  unique-id id-test))
        (progn
          ;; FIXME update ID-TEST and PARENT-UPDATING-OUTPUT? -- jd 2025-02-13
          (setf (output-record-displayer record)  continuation
                (parent-cache record)             parent-cache
                (output-record-cache-test record) cache-test
                (output-record-fixed-position record) fixed-position)
          (update-output-record record stream cache-value all-new)))
    record))

(defun update-output-record (record stream cache-value all-new)
  (let ((fixed-position (output-record-fixed-position record)))
    ;; INV since there is a sub-record the start position is bound.
    (when (and fixed-position (sub-record record))
      (setf (stream-cursor-position stream)
            (output-record-start-cursor-position record)))
    (cond
      ((or all-new
           (not (funcall (output-record-cache-test record)
                         cache-value
                         (output-record-cache-value record))))
       (let ((*current-updating-output* record))
         (setf (output-record-cache-value record) cache-value)
         (compute-new-output-records/update-parent record stream)
         (compute-new-output-records/invoke-displayer record stream)))
      ;; It doesn't need to be updated, but it does go into the parent's
      ;; sequence of records.
      (fixed-position
       (setf (output-record-dirty record) :clean)
       (compute-new-output-records/update-parent record stream))
      ;; It doesn't need to be updated, but it does go into the parent's
      ;; sequence of records. The record also needs to be moved.
      (t
       (nest
        (multiple-value-bind (cx cy) (stream-cursor-position stream))
        (multiple-value-bind (sx sy) (output-record-start-cursor-position record))
        (let* ((dx (- cx sx))
               (dy (- cy sy))
               (cleanp (= dx dy 0)))
          (setf (output-record-dirty record) (if cleanp :clean :moved))
          (compute-new-output-records/update-parent record stream)
          (when (not cleanp)
            (set-output-record-origin* record cx cy))
          (setf (stream-cursor-position stream)
                (output-record-end-cursor-position record))))))))

;;; The Franz user guide says that updating-output does &allow-other-keys, and
;;; some code I've encountered does mention other magical arguments, so we'll
;;; do the same. -- moore
(defun force-update-cache-test (a b)
  (declare (ignore a b))
  nil)

(defmacro updating-output
    ((stream &key (unique-id '*no-unique-id*) (id-test '#'eql)
                  (cache-value ''no-cache-value cache-value-supplied-p)
                  (cache-test '#'eql cache-test-supplied-p)
                  (fixed-position nil fixed-position-p)
                  (all-new nil all-new-p)
                  (parent-cache nil parent-cache-p)
                  (record-type ''standard-updating-output-record)
             &allow-other-keys)
     &body body)
  (when (eq stream t)
    (setq stream '*standard-output*))
  (unless (or cache-value-supplied-p cache-test-supplied-p)
    (setq cache-test '(function force-update-cache-test)))
  (let ((func (gensym "UPDATING-OUTPUT-CONTINUATION")))
    `(flet ((,func (,stream)
              (declare (ignorable ,stream))
              ,@body))
       (invoke-updating-output ,stream (function ,func) ,record-type ,unique-id
                               ,id-test ,cache-value ,cache-test
                               ,@(and fixed-position-p
                                      `(:fixed-position ,fixed-position))
                               ,@(and all-new-p `(:all-new ,all-new))
                               ,@(and parent-cache-p
                                      `(:parent-cache ,parent-cache))))))

(defun redisplay (record stream &key (check-overlapping t))
  (redisplay-output-record record stream check-overlapping))

(defmethod redisplay-output-record ((record updating-output-record)
                                    (stream updating-output-stream-mixin)
                                    &optional (check-overlapping t))
  (with-stream-page-layout (stream :cursor (record-text-cursor record))
    (let ((*current-updating-output* record)
          (stream-cursor (stream-text-cursor stream)))
      (setf (cursor-position stream-cursor)
            (output-record-start-cursor-position record))
      (with-stream-redisplaying (stream)
        (compute-new-output-records record stream))
      (let ((difference-set (compute-difference-set record check-overlapping)))
        (note-output-record-child-changed
         (output-record-parent record) record :change
         nil (old-bounds record) stream
         :difference-set difference-set
         :check-overlapping check-overlapping))))
  (force-output stream))

(defun redisplay-output-record* (unique-id stream)
  (when-let ((record (get-from-map stream unique-id #'eql)))
    (redisplay-output-record record stream)))

;;; Suppress the got-sheet/lost-sheet notices during redisplay.

(defmethod note-output-record-lost-sheet :around
    (record (sheet updating-output-stream-mixin))
  (declare (ignore record))
  (unless (stream-redisplaying-p sheet)
    (call-next-method)))

(defmethod note-output-record-got-sheet :around
    (record (sheet updating-output-stream-mixin))
  (declare (ignore record))
  (unless (stream-redisplaying-p sheet)
    (call-next-method)))

;;; Support for explicitly changing output records.
;;; Example where the child of a :CLEAN output record may be moved:
#+ (or)
(formatting-item-list (stream)
  (formatting-cell (stream)
    (draw-rectangle* stream 0 0 *size* *size*))
  (updating-output (stream :cache-value t)
    (formatting-cell (stream)
      (draw-rectangle* stream 0 0 30 30 :ink +red+))))

(defun mark-updating-output-changed (record)
  (when (and (not (eq record *current-updating-output*))
             (eq (output-record-dirty record) :clean))
    (setf (output-record-dirty record) :updated)
    (let ((parent (parent-updating-output record)))
      (assert (not (null parent)) () "parent of ~S null." record)
      (mark-updating-output-changed parent))))

(defgeneric propagate-to-updating-output
    (record child mode old-bounding-rectangle)
  (:method ((record standard-updating-output-record) child mode old-bbox)
    (declare (ignore child old-bbox))
    (when (and (eq mode :move)
               (eq (output-record-dirty record) :clean))
      (mark-updating-output-changed record)))
  (:method ((record output-record) child mode old-bbox)
    (when-let ((parent (output-record-parent record)))
      (propagate-to-updating-output parent child mode old-bbox))))

(defmethod* (setf output-record-position) :around
  (nx ny (record displayed-output-record))
  (with-bounding-rectangle* (x1 y1 x2 y2) record
    (multiple-value-prog1 (call-next-method)
      (unless (and (coordinate= x1 nx) (coordinate= y1 ny))
        (when-let* ((stream (and (slot-exists-p record 'stream)
                                 (slot-value  record 'stream)))
                    (parent (output-record-parent record)))
          (when (stream-redisplaying-p stream)
            (propagate-to-updating-output
             parent record :move (make-bounding-rectangle x1 y1 x2 y2))))))))

;;; INCREMENTAL-REDISPLAY takes as input the difference set computed by
;;; COMPUTE-DIFFERENCE-SET and updates the screen. The 5 kinds of updates are
;;; not very well defined in the spec. I understand their semantics thus:
;;;
;;; ERASES, MOVES, and DRAWS refer to records that don't overlap *with other
;;; records that survive in the current rendering*. In other words, they don't
;;; overlap with records that were not considered by COMPUTE-DIFFRENCE-SET,
;;; either because they are children of a clean updating output node or they
;;; are in another part of the output history that is not being redisplayed.
;;;
;;; Another way to think about erases, moves and draws is in terms of a
;;; possible implementation:
;;;
;;; - ERASES regions would be erased
;;; - MOVES regions would be blitted
;;; - DRAWS records would be replayed
;;;
;;; Records in ERASE-OVERLAPPING and MOVE-OVERLAPPING might overlap with any
;;; other record. They need to be implemented by erasing their region on the
;;; screen and then replaying the output history for that region. Thus, any
;;; ordering issues implied by overlapping records is handled correctly. Note
;;; that DRAWS records may be drawn without concern for the history because
;;; they additive. -- jd 2021-12-01
(defmethod incremental-redisplay ((stream updating-output-stream-mixin) position
                                  erases moves draws erase-overlapping move-overlapping)
  (declare (ignore position))
  (let ((regions +nowhere+))
    (flet ((add-region (region)
             (setf regions (region-union regions region))))
      (loop for (record bbox) in erases
            do (note-output-record-lost-sheet record stream)
               (add-region bbox))
      (loop for (record old-bbox) in moves
            do (add-region old-bbox)
               (add-region record))
      (loop for (record bbox) in draws
            do (note-output-record-got-sheet record stream)
               (add-region bbox))
      (loop for (record bbox) in erase-overlapping
            do (note-output-record-lost-sheet record stream)
               (add-region bbox))
      (loop for (nil bbox) in move-overlapping
            do (add-region bbox)))
    (dispatch-repaint stream regions)))


;;; Notification protocol for the record change propagation. The function
;;; NOTE-OUTPUT-RECORD-CHILD-CHANGED propagates changes upwards.

(defmethod propagate-output-record-changes-p
    (record child mode old-position old-bounding-rectangle)
  (declare (ignore mode old-position))
  (and record
       (or (null old-bounding-rectangle)
           (not (region-equal child old-bounding-rectangle)))))

(defmethod propagate-output-record-changes
    (record child mode &optional old-position old-bounding-rectangle
                                 difference-set check-overlapping)
  (declare (ignore old-position))
  (ecase mode
    (:none
     nil)
    (:add
     (recompute-extent-for-new-child record child))
    (:delete
     (with-bounding-rectangle* (x1 y1 x2 y2) child
       (recompute-extent-for-changed-child record child x1 y1 x2 y2)))
    ((:change :move :clear)
     (with-bounding-rectangle* (x1 y1 x2 y2) old-bounding-rectangle
       (recompute-extent-for-changed-child record child x1 y1 x2 y2))))
  (values difference-set check-overlapping))

(defmethod note-output-record-child-changed
    (record child mode old-position old-bounding-rectangle stream
     &key difference-set check-overlapping)
  (if (propagate-output-record-changes-p
       record child mode old-position old-bounding-rectangle)
      (let ((old-bbox (copy-bounding-rectangle record)))
        (multiple-value-bind (difference-set check-overlapping)
            (with-stream-history-locked (stream :propagate)
              (propagate-output-record-changes
               record child mode old-position old-bounding-rectangle
               difference-set check-overlapping))
          (note-output-record-child-changed
           (output-record-parent record) record :change nil old-bbox stream
           :difference-set difference-set
           :check-overlapping check-overlapping)))
      (destructuring-bind (erases moves draws erases* moves*) difference-set
        (incremental-redisplay stream nil erases moves draws erases* moves*))))
