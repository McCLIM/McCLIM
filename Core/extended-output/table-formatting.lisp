;;; ---------------------------------------------------------------------------
;;;   License: LGPL-2.1+ (See file 'Copyright' for details).
;;; ---------------------------------------------------------------------------
;;;
;;;  (c) copyright 2001 Alexey Dejneka <adejneka@comail.ru>
;;;  (c) copyright 2003 Gilbert Baumann <unk6@rz.uni-karlsruhe.de>
;;;
;;; ---------------------------------------------------------------------------
;;;
;;; Macros and functions for formatting tables and item lists.
;;;

;;; TODO:
;;;
;;; - Check types: RATIONAL, COORDINATE, REAL?
;;; - Check default values of unsupplied arguments.
;;; - Better error detection.
;;; -
;;; - Multiple columns:
;;; - - all columns are assumed to have the same width;
;;; - - all columns have the same number of rows; they should have the
;;;     same height.
;;; - :MOVE-CURSOR T support.
;;; - All types of widths, heights.
;;;   width, height too?
;;; - Item list formatting: what is :EQUALIZE-COLUMN-WIDTHS?!

;;; - I would prefer if the INITIALIZE-INSTANCE would grok spacing and
;;;   all. Hmm, is that correct?

;;; The question araise if we need to support something like:
;;; (formatting-row ()
;;;   (with-output-as-presentation ()
;;;     (formatting-cell ())
;;;     (formatting-cell ())))

;;; Further: Should this table be somehow dynamic? That is when cell
;;; contents change also rerun the layout protocol? Or is that somehow
;;; covered by the incremental redisplay?

;;; Guess, we just do, it couldn't hurt.

(in-package #:clim-internals)

;;; Macro argument checking

(eval-when (:compile-toplevel :load-toplevel :execute)
  ;; FORMAT-ITEMS uses FORMATTING-CELL, so we need this function at
  ;; compile-time.
  (defun %maybe-check-constant-keyword-arguments
      (record-type standard-record-type arguments valid-keywords)
    (when (and (constantp record-type)
               (eq (eval record-type) standard-record-type))
      (loop for (key nil) on arguments by #'cddr
            when (constantp key)
              do (let ((key (eval key)))
                   (unless (or (eq key :record-type)
                               (member key valid-keywords))
                     (with-current-source-form (arguments)
                       (error "~@<~S is not a valid initarg for ~S.~@:>"
                              key standard-record-type))))))))

;;; Helper macro for defining FORMATTING-* macros in the corresponding
;;; INVOKE-FORMATTING-* functions.
;;;
;;; ARGS is a list of argument specifications of the form
;;;
;;;   (name &key bindp default transform passp)
;;;
;;; where NAME specifies the name of the keyword argument in the
;;; generated function and the generated macro.
;;;
;;; BINDP controls whether NAME should be bound when BODY is evaluated
;;; even if the argument is just "passed through".
;;;
;;; DEFAULT specifies a default value for the argument. Defaulting is
;;; done in the generated function, not the macro.
;;;
;;; TRANSFORM is a form that computes a new value for the argument
;;; based on the current value (that is, the form can refer to NAME),
;;; for example normalizing the value.
;;;
;;; PASSP, which defaults to true, controls whether the argument is
;;; passed to the output record as an initarg.
(defmacro define-formatting-macro ((name standard-output-record-class)
                                   args
                                   ((stream-var record-var) &body body))
  (check-type stream-var symbol)
  (check-type record-var symbol)
  (let* ((invoke-name (alexandria:symbolicate '#:invoke- name))
         (keywords '())
         (keyword-arguments '())
         (keyword-arguments/defaults '())
         (transformed-keyword-arguments '())
         (remove-keyword-arguments '()))
    (mapc (lambda (arg)
            (destructuring-bind (name &key bindp
                                           (default nil defaultp)
                                           (transform nil transformp)
                                           (passp t))
                arg
              (let ((keyword (alexandria:make-keyword name)))
                (alexandria:appendf keywords (list keyword))
                (alexandria:appendf keyword-arguments (list name))
                (when bindp
                  (alexandria:appendf keyword-arguments/defaults
                                      (list name)))
                (when (or defaultp transformp)
                  (alexandria:appendf keyword-arguments/defaults
                                      (list `(,name ,default))))
                (when (and passp (or defaultp transformp))
                  (alexandria:appendf transformed-keyword-arguments
                                      `(,keyword ,(if transformp
                                                      transform
                                                      name))))
                (when (or defaultp transformp (not passp))
                  (alexandria:appendf remove-keyword-arguments (list keyword))))))
          args)
    `(progn
       (defun ,invoke-name (,stream-var continuation
                            &rest args
                            &key (record-type ',standard-output-record-class)
                                 ,@keyword-arguments/defaults
                            &allow-other-keys)
         ((lambda (,record-var) ,@body)
          (with-keywords-removed (args (:record-type ,@remove-keyword-arguments))
            (apply #'invoke-with-output-to-output-record ,stream-var
                   (lambda (,stream-var ,record-var)
                     (declare (ignore ,record-var))
                     (funcall continuation ,stream-var))
                   record-type ,@transformed-keyword-arguments
                   args))))

       (defmacro ,name ((&optional (stream t)
                         &rest args
                         &key (record-type '',standard-output-record-class)
                              ,@keyword-arguments
                         &allow-other-keys)
                        &body body)
         (declare (ignore ,@keyword-arguments))
         ;; If RECORD-TYPE is the default, make sure that all keyword
         ;; arguments with constant keys correspond to known initargs.
         (%maybe-check-constant-keyword-arguments
          record-type ',standard-output-record-class args ',keywords)
         (with-stream-designator (stream '*standard-output*)
           (gen-invoke-trampoline
            ',invoke-name (list stream) args body))))))

(defun format-insert-record (record stream move-cursor)
  (if (null move-cursor)
      (cond
        ((stream-recording-p stream)
         (stream-add-output-record stream record))
        ((stream-drawing-p stream)
         ;; See #1423. --jd 2025-02-07
         (with-identity-transformation (stream)
           (replay-output-record record stream))))
      (multiple-value-bind (cx cy) (output-record-offset record)
        (set-output-record-origin record cx cy)
        (stream-write-object stream record))))

;;; Cell formatting

;;; STANDARD-CELL-OUTPUT-RECORD class
(defclass standard-cell-output-record (cell-output-record
                                       standard-sequence-output-record)
  ((align-x    :initarg :align-x    :reader cell-align-x)
   (align-y    :initarg :align-y    :reader cell-align-y)
   (min-width  :initarg :min-width  :reader cell-min-width)
   (min-height :initarg :min-height :reader cell-min-height))
  (:default-initargs
   :align-x :left :align-y :baseline :min-width 0 :min-height 0))

(define-formatting-macro (formatting-cell standard-cell-output-record)
    ((align-x :default :left)
     (align-y :default :baseline)
     (min-width :default 0
                :transform (parse-space stream min-width :horizontal))
     (min-height :default 0
                 :transform (parse-space stream min-height :vertical)))
  ((stream record)
   (with-output-to-output-record (stream nil nil :parent record)
     (ensure-gutter stream 0 0
                    (cell-min-width record)
                    (cell-min-height record)))
   (stream-add-output-record stream record)))


;;; Generic block formatting
(defclass block-output-record-mixin ()
  ()
  (:documentation "The class representing one-dimensional blocks of cells."))

(defmethod replay-output-record ((bl block-output-record-mixin) stream
                                 &optional (region (sheet-visible-region stream))
                                           (x-offset 0) (y-offset 0))
  (with-drawing-options (stream :clipping-region region)
    (let (other-records)
      (map-over-output-records-overlapping-region
       #'(lambda (record)
           (if (cell-output-record-p record)
               (replay-output-record record stream region x-offset y-offset)
               (push record other-records)))
       bl region x-offset y-offset)
      (mapc #'(lambda (record)
                (replay-output-record record stream region x-offset y-offset))
            (nreverse other-records)))))

(defmethod map-over-block-cells (function (block block-output-record-mixin))
  ;; ### we need to do better -- yeah! how?
  (labels ((foo (row-record)
             (map-over-output-records
              (lambda (record)
                (if (cell-output-record-p record)
                    (funcall function record)
                    (foo record)))
              row-record)))
    (declare (dynamic-extent #'foo))
    (foo block)))


;;; Row formatting

;;; STANDARD-ROW-OUTPUT-RECORD class
(defclass standard-row-output-record (row-output-record
                                      block-output-record-mixin
                                      standard-sequence-output-record)
  ())

(defmethod map-over-row-cells (function
                               (row-record standard-row-output-record))
  (map-over-block-cells function row-record))

(define-formatting-macro (formatting-row standard-row-output-record)
    ()
  ((stream record)
   #+ (or)
   ;; FIXME cols/rows should format cells and table should format blocks.
   (with-output-to-output-record (stream nil nil :parent record)
     (ensure-gutter stream 0 0))
   (stream-add-output-record stream record)))


;;; Column formatting

;;; STANDARD-COLUMN-OUTPUT-RECORD class
(defclass standard-column-output-record (column-output-record
                                         block-output-record-mixin
                                         standard-sequence-output-record)
  ())

(defmethod map-over-column-cells (function (column-record standard-column-output-record))
  (map-over-block-cells function column-record))

(define-formatting-macro (formatting-column standard-column-output-record)
    ()
  ((stream record)
   #+ (or)
   ;; FIXME cols/rows should format cells and table should format blocks.
   (with-output-to-output-record (stream nil nil :parent record)
     (ensure-gutter stream 0 0))
   (stream-add-output-record stream record)))


;;; Table formatting

;;; STANDARD-TABLE-OUTPUT-RECORD class
(defclass standard-table-output-record (table-output-record
                                        standard-sequence-output-record)
  (;; standard slots
   (x-spacing :initarg :x-spacing)
   (y-spacing :initarg :y-spacing)
   (multiple-columns :initarg :multiple-columns)
   (multiple-columns-x-spacing :initarg :multiple-columns-x-spacing)
   (equalize-column-widths :initarg :equalize-column-widths)
   ;; book keeping -- communication from adjust-table-cells to
   ;; adjust-multiple-columns
   (widths)
   (heights)                            ;needed?
   (rows))
  (:default-initargs
   :multiple-columns nil
   :multiple-columns-x-spacing nil
   :equalize-column-widths nil))

(defmethod initialize-instance :after ((table standard-table-output-record)
                                       &rest initargs)
  (declare (ignore initargs))
  (when (null (slot-value table 'multiple-columns-x-spacing))
    (setf (slot-value table 'multiple-columns-x-spacing)
          (slot-value table 'x-spacing))))

(defmethod replay-output-record ((table standard-table-output-record) stream
                                 &optional (region (sheet-visible-region stream))
                                           (x-offset 0) (y-offset 0))
  (with-drawing-options (stream :clipping-region region)
    (let (other-records)
      (map-over-output-records-overlapping-region
       #'(lambda (record)
           (if (or (column-output-record-p record)
                   (row-output-record-p record))
               (replay-output-record record stream region x-offset y-offset)
               (push record other-records)))
       table region x-offset y-offset)
      (mapc #'(lambda (record)
                (replay-output-record record stream region x-offset y-offset))
            (nreverse other-records)))))

(defmethod format-output-record ((table standard-table-output-record) stream)
  (adjust-table-cells table stream)
  (when (slot-value table 'multiple-columns)
    (adjust-multiple-columns table stream))
  (tree-recompute-extent table))

(define-formatting-macro (formatting-table standard-table-output-record)
    ((move-cursor :default t :passp nil)
     (x-spacing :transform (parse-space stream (or x-spacing #\Space) :horizontal))
     (y-spacing :transform (parse-space stream (or y-spacing
                                                   (stream-vertical-spacing stream))
                                        :vertical))
     (multiple-columns)
     (multiple-columns-x-spacing :transform (if multiple-columns-x-spacing
                                                (parse-space stream multiple-columns-x-spacing :horizontal)
                                                x-spacing))
     (equalize-column-widths))
  ((stream record)
   (with-output-to-output-record (stream nil nil :parent record)
     (ensure-gutter stream 0 0))
   (format-output-record record stream)
   (format-insert-record record stream move-cursor)
   record))

;;; Think about rewriting this using a common superclass for row and
;;; column records.

(defmethod map-over-table-elements
    (function (table-record standard-table-output-record) (type (eql :row)))
  (labels ((row-mapper (table-record)
             (map-over-output-records
              (lambda (record)
                (if (row-output-record-p record)
                    (funcall function record)
                    (row-mapper record)))
              table-record)))
    (declare (dynamic-extent #'row-mapper))
    (row-mapper table-record)))

(defmethod map-over-table-elements
    (function (table-record standard-table-output-record) (type (eql :column)))
  (labels ((col-mapper (table-record)
             (map-over-output-records
              (lambda (record)
                (if (column-output-record-p record)
                    (funcall function record)
                    (col-mapper record)))
              table-record)))
    (declare (dynamic-extent #'col-mapper))
    (col-mapper table-record)))

(defmethod map-over-table-elements (function
                                    (table-record standard-table-output-record)
                                    (type (eql :row-or-column)))
  (labels ((row-and-col-mapper (table-record)
             (map-over-output-records
              (lambda (record)
                (if (or (row-output-record-p record)
                        (column-output-record-p record))
                    (funcall function record)
                    (row-and-col-mapper record)))
              table-record)))
    (declare (dynamic-extent #'row-and-col-mapper))
    (row-and-col-mapper table-record)))


;;; Item list formatting

(defclass standard-item-list-output-record (item-list-output-record
                                            block-output-record-mixin
                                            standard-sequence-output-record)
  ((x-spacing :initarg :x-spacing)
   (y-spacing :initarg :y-spacing)
   (n-columns :initarg :n-columns)
   (n-rows :initarg :n-rows)
   (max-width :initarg :max-width)
   (max-height :initarg :max-height)
   (initial-spacing :initarg :initial-spacing)
   (row-wise :initarg :row-wise))
  (:default-initargs
   :n-columns nil :n-rows nil :max-width nil :max-height nil
   :initial-spacing nil :row-wise t))

(defmethod map-over-item-list-cells
    (function (item-list-record standard-item-list-output-record))
  (map-over-block-cells function item-list-record))

(define-formatting-macro (formatting-item-list standard-item-list-output-record)
    ((move-cursor :default t :passp nil)
     (x-spacing :transform (parse-space stream (or x-spacing #\Space) :horizontal))
     (y-spacing :transform (parse-space stream (or y-spacing
                                                   (stream-vertical-spacing stream))
                                        :vertical))
     (n-columns)
     (n-rows)
     (max-width)
     (max-height)
     (initial-spacing)
     (row-wise :default t))
    ((stream record)
      (with-output-to-output-record (stream nil nil :parent record)
        (ensure-gutter stream 0 0))
      (format-output-record record stream)
      (format-insert-record record stream move-cursor)
      record))

(defmethod format-output-record ((item-list standard-item-list-output-record) stream)
  (ensure-gutter stream 0 0)
  (adjust-item-list-cells item-list stream)
  (tree-recompute-extent item-list))

;;; KLUDGE this function is later redefined to handle the argument
;;; PRESENTATION-TYPE.
(defun format-items (items &rest args
                     &key (stream *standard-output*)
                          (printer #'prin1) presentation-type
                          cell-align-x cell-align-y
                     &allow-other-keys)
  (declare (ignore presentation-type))
  (with-keywords-removed
      (args (:stream :printer :presentation-type :cell-align-x :cell-align-y))
    (apply #'invoke-formatting-item-list stream
           (lambda (stream)
             (dolist (item items)
               (formatting-cell (stream :align-x cell-align-x
                                        :align-y cell-align-y)
                 (funcall printer item stream))))
           args)))

;;; Helper function

(defun make-table-array (table-record)
  "Given a table record, creates an array of arrays of cells in row major
  order. Returns (array-of-cells number-of-rows number-of-columns)"
  (let* ((row-based (block
                        find-table-type
                      (map-over-table-elements
                       (lambda (thing)
                         (cond ((row-output-record-p thing)
                                (return-from find-table-type t))
                               ((column-output-record-p thing)
                                (return-from find-table-type nil))
                               (t
                                (error "Something is wrong."))))
                       table-record
                       :row-or-column)
                      ;; It's empty
                      (return-from make-table-array (values nil 0 0))))
         (rows (make-array (if row-based 1 0)
                           :adjustable t
                           :fill-pointer (if row-based
                                             0
                                             nil)
                           :initial-element nil))
         (number-of-columns 0))
    (if row-based
        (map-over-table-elements
         (lambda (row)
           (let ((row-array (make-array 4 :adjustable t :fill-pointer 0)))
             (map-over-row-cells (lambda (cell)
                                   (vector-push-extend cell row-array))
                                 row)
             (vector-push-extend row-array rows)
             (maxf number-of-columns (length row-array))))
         table-record
         :row)
        (let ((col-index 0))
          (map-over-table-elements
           (lambda (col)
             (let ((row-index 0))
               (map-over-column-cells
                (lambda (cell)
                  (when (>= row-index (length rows))
                    (adjust-array rows (1+ row-index) :initial-element nil))
                  (let ((row-array (aref rows row-index)))
                    (cond ((null row-array)
                           (setf row-array
                                 (make-array (1+ col-index)
                                             :adjustable t
                                             :initial-element nil))
                           (setf (aref rows row-index) row-array))
                          ((>= col-index (length row-array))
                           (adjust-array row-array (1+ col-index)
                                         :initial-element nil))
                          (t nil))
                    (setf (aref row-array col-index) cell))
                  (incf row-index))
                col))
             (incf col-index))
           table-record
           :column)
          (setq number-of-columns col-index)))
    (values rows (length rows) number-of-columns)))

(defmethod adjust-table-cells ((table-record standard-table-output-record)
                               stream)
  (with-slots (x-spacing y-spacing equalize-column-widths) table-record
    ;; Note: for the purpose of layout it is pretty much irrelevant if
    ;;       this is a table by rows or a table by columns
    ;;
    ;; Since we have :baseline vertical alignment (and no :char horizontal
    ;; alignment like in HTML), we always work from rows.
    (multiple-value-bind (rows nrows ncols)
        (make-table-array table-record)
      (unless rows
        (return-from adjust-table-cells nil))

      (let ((widthen  (make-array ncols :initial-element 0))
            (heights  (make-array nrows :initial-element 0))
            (ascents  (make-array nrows :initial-element 0))
            (descents (make-array nrows :initial-element 0)))
        ;; collect widthen, heights
        (loop for row across rows
              for i from 0 do
                (loop for cell across row
                      for j from 0 do
                        ;; we have cell at row i col j at hand.
                        ;; width:
                        (when cell
                          (with-bounding-rectangle* (:width width :height height) cell
                            (maxf (aref widthen j) width)
                            (maxf (aref heights i) height)
                            (when (eq (cell-align-y cell) :baseline)
                              (multiple-value-bind (bx by) (output-record-offset cell)
                                (declare (ignore bx))
                                (maxf (aref ascents i) by)
                                (maxf (aref descents i) (- height by))))))))

        ;; baseline aligned cells can force the row to be taller.
        (loop for i from 0 below nrows do
          (maxf (aref heights i) (+ (aref ascents i) (aref descents i))))

        (when (slot-value table-record 'equalize-column-widths)
          (setf widthen (make-array ncols :initial-element (reduce #'max widthen :initial-value 0))))

        (setf (slot-value table-record 'widths) widthen
              (slot-value table-record 'heights) heights
              (slot-value table-record 'rows) rows)

        ;; Finally just put the cells where they belong.

        (loop for row across rows
              for y = 0 then (+ y h y-spacing)
              for h across heights
              for ascent across ascents
              do
                 (loop for cell across row
                       for x = 0 then (+ x w x-spacing)
                       for w across widthen do
                         (when cell
                           (adjust-cell* cell x y w h ascent))))))))

(defmethod adjust-multiple-columns ((table standard-table-output-record) stream)
  (with-slots (widths heights rows
               multiple-columns multiple-columns-x-spacing x-spacing y-spacing)
      table
    (let* ((mcolumn-width
             ;; total width of a column of the "meta" table.
             (+ (reduce #'+ widths)
                (* (1- (length widths)) x-spacing)
                multiple-columns-x-spacing))
           (n-columns
             (max 1
                  (if (eq multiple-columns t)
                      (floor (- (stream-text-margin stream)
                                (stream-cursor-position stream))
                             mcolumn-width)
                      multiple-columns)))
           (column-size (ceiling (length rows) n-columns)) )
      (loop with y = 0
            with dy = 0
            for row across rows
            for h across heights
            for i from 0
            do (multiple-value-bind (ci ri) (floor i column-size)
                 (when (zerop ri)
                   (setf dy (- y)))
                 (let ((dx (* ci mcolumn-width)))
                   (loop for cell across row do
                     (multiple-value-bind (x y) (output-record-position cell)
                       (setf (output-record-position cell)
                             (values (+ x dx) (+ y dy))))))
                 (incf y h)
                 (incf y y-spacing))) )))

;;; The adjust of the item list is different from the table:
;;;
;;; - row-wise -- cols in different rows are not aligned horizontally
;;; - col-wise -- rows in different cols are not aligned vertically
;;;
;;; In other words they are flowing from left to right or from top to bottom and
;;; wrap over the appropriate edge. That also means that unless n-columns/n-rows
;;; is specified, individual columns/rows may have variable number of cells.

(defun adjust-item-list-1 (record n-elements max-size row-wise-p)
  (with-slots (initial-spacing x-spacing y-spacing) record
    (let* ((x0 (if initial-spacing (floor x-spacing 2) 0))
           (y0 (if initial-spacing (floor y-spacing 2) 0))
           (current-n 0)
           (current-x x0)
           (current-y y0)
           (current-max-h 0)
           (current-max-w 0)
           (current-max-b 0)
           (limit-s (if n-elements nil (and max-size (- max-size x0))))
           (limit-n (or n-elements nil))
           (this-slice (make-array 0 :adjustable t :fill-pointer t)))
      (labels ((finish-slice ()
                 (if row-wise-p
                     (loop for (item cell-x cell-w) across this-slice
                           do (adjust-cell* item
                                            cell-x current-y
                                            cell-w
                                            current-max-h
                                            current-max-b))
                     (loop for (item cell-y cell-h) across this-slice
                           do (adjust-cell* item
                                            current-x cell-y
                                            current-max-w
                                            cell-h
                                            (output-record-offset-y item))))
                 (setf current-n 0)
                 (if row-wise-p
                     (setf current-x x0
                           current-y (+ current-y (+ current-max-h y-spacing)))
                     (setf current-x (+ current-x (+ current-max-w x-spacing))
                           current-y y0))
                 (setf current-max-w 0)
                 (setf current-max-h 0)
                 (setf current-max-b 0)
                 (setf (fill-pointer this-slice) 0))
               (end-slice-p (cell-w cell-h)
                 (or (and limit-n (>= current-n limit-n))
                     (and limit-s (if row-wise-p
                                      (> (+ current-x cell-w) limit-s)
                                      (> (+ current-y cell-h) limit-s)))))
               (parse-cell (item)
                 (with-bounding-rectangle* (:width cell-w :height cell-h) item
                   (when (end-slice-p cell-w cell-h)
                     (finish-slice))
                   (maxf current-max-w cell-w)
                   (maxf current-max-h cell-h)
                   (maxf current-max-b (output-record-offset-y item))
                   (incf current-n)
                   (if row-wise-p
                       (progn
                         (vector-push-extend (list item current-x cell-w) this-slice)
                         (incf current-x (+ cell-w x-spacing)))
                       (progn
                         (vector-push-extend (list item current-y cell-h) this-slice)
                         (incf current-y (+ cell-h y-spacing)))))))
        (map-over-item-list-cells #'parse-cell record)
        (finish-slice)))))

(defmethod adjust-item-list-cells ((record standard-item-list-output-record) stream)
  (with-slots (x-spacing y-spacing initial-spacing row-wise
               n-columns n-rows max-width max-height)
      record
    ;; The limit applies to each slice (a row or a column). If we can determine
    ;; the number of elements in each slice, then the maximal size is ignored.
    ;; When there are no limits, then:
    ;; - row-wise: determine the maximal width based on the stream width
    ;; - col-wise: all elements are put in a single column without wrapping
    (flet ((compute-limits-for-rows ()
             (cond (n-columns)
                   (n-rows     (ceiling (output-record-count record) n-rows))
                   (max-width  (values nil max-width))
                   (t          (values nil (- (stream-text-margin stream)
                                              (stream-cursor-position stream)
                                              (if initial-spacing x-spacing 0))))))
           (compute-limits-for-cols ()
             (cond (n-rows)
                   (n-columns  (ceiling (output-record-count record) n-columns))
                   (max-height (values nil max-height))
                   (t          (values nil nil)))))
      (multiple-value-bind (n-elements max-size)
          (if row-wise
              (compute-limits-for-rows)
              (compute-limits-for-cols))
        (adjust-item-list-1 record n-elements max-size row-wise)))))

(defun adjust-cell* (cell x y w h ascent)
  (setf (output-record-position cell)
        (values
         (case (cell-align-x cell)
           ((nil :left)   x)            ;###
           (:center (+ x (/ (- w (bounding-rectangle-width cell)) 2)))
           (:right  (- (+ x w) (bounding-rectangle-width cell))))
         (case (cell-align-y cell)
           (:top    y)
           (:bottom (- (+ y h) (bounding-rectangle-height cell)))
           (:center (+ y (/ (- h (bounding-rectangle-height cell)) 2)))
           ((nil :baseline)
            ;; make (+ y ascents) line up with (+ y1 b)
            ;; that is y+a = y1+b -> y1= y+a-b
            (+ y (- ascent (output-record-offset-y cell))))))))
