;;; ---------------------------------------------------------------------------
;;;   License: LGPL-2.1+ (See file 'Copyright' for details).
;;; ---------------------------------------------------------------------------
;;;
;;;  (c) copyright 1998-2003 Michael McDonald <mikemac@mikemac.com>
;;;  (c) copyright 2001 Arnaud Rouanet <rouanet@emi.u-bordeaux.fr>
;;;  (c) copyright 2001,2002 Alexey Dejneka
;;;  (c) copyright 2002 Brian Spilsbury
;;;  (c) copyright 2002,2003 Gilbert Baumann <gbaumann@common-lisp.net>
;;;  (c) copyright 2003-2008 Andy Hefner <ahefner@common-lisp.net>
;;;  (c) copyright 2005,2006 Timothy Moore <tmoore@common-lisp.net>
;;;  (c) copyright 2005 Rudi Schlatte <rschlatte@common-lisp.net>
;;;  (c) copyright 2008 Troels Henriksen <thenriksen@common-lisp.net>
;;;  (c) copyright 2014 Robert Strandh <robert.strandh@gmail.com>
;;;  (c) copyright 2016 Alessandro Serra <gas2serra@gmail.com>
;;;  (c) copyright 2018 Elias Mårtenson <lokedhs@gmail.com>
;;;  (c) copyright 2019-2021 Jan Moringen <jmoringe@techfak.uni-bielefeld.de>
;;;  (c) copyright 2016-2024 Daniel Kochmański <daniel@turtleware.eu>
;;;
;;; ---------------------------------------------------------------------------
;;;
;;; Graphics.
;;;

(in-package #:clim-internals)

(defun do-graphics-with-options-internal
    (medium func &rest args
     &key ink clipping-region transformation
       line-style
       line-unit line-thickness
       (line-dashes nil dashes-p)
       line-joint-shape line-cap-shape
       (text-style nil text-style-p)
       (text-family nil text-family-p)
       (text-face nil text-face-p)
       (text-size nil text-size-p)
       (line-direction nil ldir-p)
       (page-direction nil pdir-p)
     &allow-other-keys)
  (declare (ignore args))
  (flet ((compute-ink ()
           (when ink
             (unless (design-equalp ink (medium-ink medium))
               ink)))
         (compute-line ()
           (when (or line-style line-unit line-thickness dashes-p
                     line-joint-shape line-cap-shape)
             (let* ((old-line-style (medium-line-style medium))
                    (line-style (or line-style old-line-style))
                    (line-unit (or line-unit (line-style-unit line-style)))
                    (line-thickness (or line-thickness (line-style-thickness line-style)))
                    (line-dashes (if dashes-p line-dashes (line-style-dashes line-style)))
                    (line-joint-shape (or line-joint-shape
                                          (line-style-joint-shape line-style)))
                    (line-cap-shape (or line-cap-shape (line-style-cap-shape line-style)))
                    (new-line-style (make-line-style :unit line-unit
                                                     :thickness line-thickness
                                                     :joint-shape line-joint-shape
                                                     :cap-shape line-cap-shape
                                                     :dashes line-dashes)))
               (unless (line-style-equalp old-line-style new-line-style)
                 new-line-style))))
         (compute-text ()
           (when (or text-style text-family-p text-face-p text-size-p)
             (let* ((merged-text-style (medium-merged-text-style medium))
                    (text-style (if text-style-p
                                    (merge-text-styles text-style merged-text-style)
                                    merged-text-style))
                    (text-style (if (or text-family-p text-face-p text-size-p)
                                    (merge-text-styles (make-text-style text-family
                                                                        text-face
                                                                        text-size)
                                                       text-style)
                                    text-style)))
               (unless (text-style-equalp text-style merged-text-style)
                 text-style))))
         (compute-ldir ()
           (when ldir-p
             (unless (eql line-direction (medium-line-direction medium))
               line-direction)))
         (compute-pdir ()
           (when pdir-p
             (unless (eql page-direction (medium-page-direction medium))
               page-direction)))
         (compute-transformation ()
           (when (and transformation
                      (not (identity-transformation-p transformation)))
             (compose-transformations (medium-transformation medium)
                                      transformation))))
    (macrolet ((with-options (bindings &body body)
                 (loop for (place form) in bindings
                       for old-value = (gensym "OLD")
                       for new-value = (gensym "NEW")
                       collect old-value into old-vars
                       collect `(,new-value ,form) into new-vars
                       collect `(when ,new-value
                                  (setf ,old-value ,place
                                        ,place ,new-value))
                         into sets
                       collect `(when ,new-value
                                  (setf ,place ,old-value))
                         into undo
                       finally (return `(let (,@old-vars ,@new-vars)
                                          (unwind-protect (progn ,@sets ,@body)
                                            ,@(nreverse undo)))))))
      (let ((*foreground-ink* (medium-foreground medium))
            (*background-ink* (medium-background medium)))
        (with-options (((medium-ink medium) (compute-ink))
                       ((medium-line-style medium) (compute-line))
                       ((medium-text-style medium) (compute-text))
                       ((medium-line-direction medium) (compute-ldir))
                       ((medium-page-direction medium) (compute-pdir))
                       ((medium-transformation medium) (compute-transformation)))
          (with-clipping-region (medium clipping-region)
            (funcall func medium)))))))

(defmacro with-drawing-options ((medium &rest drawing-options) &body body)
  (with-stream-designator (medium '*standard-output*)
    (with-gensyms (gcontinuation cont-arg)
      `(flet ((,gcontinuation (,cont-arg)
                (declare (ignore ,cont-arg))
                ,@body))
         (declare (dynamic-extent (function ,gcontinuation)))
         (invoke-with-drawing-options
          ,medium (function ,gcontinuation) ,@drawing-options)))))

;;; Same as the above, but applies args..
(defmacro with-drawing-options* ((medium drawing-options) &body body)
  (with-stream-designator (medium '*standard-output*)
    (with-gensyms (gcontinuation cont-arg)
      `(flet ((,gcontinuation (,cont-arg)
                (declare (ignore ,cont-arg))
                ,@body))
         (declare (dynamic-extent #',gcontinuation))
         (apply #'invoke-with-drawing-options
                ,medium (function ,gcontinuation) ,drawing-options)))))

;;; Compatibility with real CLIM
(defmethod invoke-with-drawing-options (self cont &rest opts)
  (declare (ignore opts))
  (funcall cont self))

(defmethod invoke-with-drawing-options ((self medium) cont &rest opts)
  (apply #'do-graphics-with-options-internal self cont opts))

(defmethod invoke-with-identity-transformation
    ((medium medium) continuation)
  (letf (((medium-transformation medium) +identity-transformation+))
    (funcall continuation medium)))

(defun invoke-with-local-coordinates (medium cont x y)
  (orf x 0)
  (orf y 0)
  (with-identity-transformation (medium)
    (with-translation (medium x y)
      (funcall cont medium))))

(defun invoke-with-first-quadrant-coordinates (medium cont x y)
  (orf x 0)
  (orf y 0)
  (with-identity-transformation (medium)
    (with-translation (medium x y)
      (with-scaling (medium 1 -1)
        (funcall cont medium)))))

;;; 10.3 Line Styles

;;; 10.3.2 Contrasting Dash Patterns

(defconstant +contrasting-dash-patterns+
  ;; Must be at least eight according to the specification (Section
  ;; 10.3.2 Contrasting Dash Patterns).
  #(#(2 2) #(2 4)     #(2 8)     #(2 16) ; dots with varying empty space
           #(4 2)     #(8 2)     #(16 2) ; varying dashes with minimum empty space
           #(2 2 4 2) #(2 2 8 2)))       ; mixed

(defmethod contrasting-dash-pattern-limit (port)
  (declare (ignore port))
  (length +contrasting-dash-patterns+))

(defun make-contrasting-dash-patterns (n &optional k)
  (let ((contrasting-dash-patterns +contrasting-dash-patterns+))
    (unless (<= 1 n (length contrasting-dash-patterns))
      (error "The argument N = ~D is out of range [1, ~D]"
             n (length contrasting-dash-patterns)))
    (unless (or (null k) (<= 0 k (1- n)))
      (error "The argument K = ~D is out of range [0, ~D]" k (1- n)))
    (if (null k)
        (subseq contrasting-dash-patterns 0 n)
        (aref contrasting-dash-patterns k))))

;;;
;;; DRAW-DESIGN
;;

(defmethod draw-design (medium (design point)
                        &rest options &key &allow-other-keys)
  (with-drawing-options* (medium options)
    (medium-draw-point* medium (point-x design) (point-y design))))

(defmethod draw-design (medium (design polyline)
                        &rest options &key &allow-other-keys)
  (with-drawing-options* (medium options)
    (let ((coords (expand-point-seq (polygon-points design)))
          (closed (polyline-closed design)))
     (medium-draw-polygon* medium coords closed nil))))

(defmethod draw-design (medium (design polygon)
                        &rest options &key (filled t) &allow-other-keys)
  (with-drawing-options* (medium options)
    (let ((coords (expand-point-seq (polygon-points design))))
      (medium-draw-polygon* medium coords t filled))))

(defmethod draw-design (medium (design polybezier)
                        &rest options &key &allow-other-keys)
  (with-drawing-options* (medium options)
    (let ((coords (expand-point-seq (bezigon-points design))))
      (medium-draw-bezigon* medium coords nil nil))))

(defmethod draw-design (medium (design bezigon)
                        &rest options &key (closed t) (filled t) &allow-other-keys)
  (with-drawing-options* (medium options)
    (let ((coords (expand-point-seq (bezigon-points design))))
     (medium-draw-bezigon* medium coords closed filled))))

(defmethod draw-design (medium (design line)
                        &rest options &key &allow-other-keys)
  (with-drawing-options* (medium options)
    (multiple-value-bind (x1 y1) (line-start-point* design)
      (multiple-value-bind (x2 y2) (line-end-point* design)
        (medium-draw-line* medium x1 y1 x2 y2)))))

(defmethod draw-design (medium (design rectangle)
                        &rest options &key (filled t) &allow-other-keys)
  (with-drawing-options* (medium options)
    (multiple-value-bind (x1 y1 x2 y2) (rectangle-edges* design)
      (medium-draw-rectangle* medium x1 y1 x2 y2 filled))))

(defmethod draw-design (medium (design ellipse)
                        &rest options &key (filled t) &allow-other-keys)
  (with-drawing-options* (medium options)
    (multiple-value-bind (cx cy) (ellipse-center-point* design)
      (multiple-value-bind (r1x r1y r2x r2y) (ellipse-radii design)
        (let ((sa (or (ellipse-start-angle design) 0.0))
              (ea (or (ellipse-end-angle design) (* 2.0 pi))))
          (medium-draw-ellipse* medium cx cy r1x r1y r2x r2y sa ea filled))))))

(defmethod draw-design (medium (design elliptical-arc)
                        &rest options &key &allow-other-keys)
  (with-drawing-options* (medium options)
    (multiple-value-bind (cx cy) (ellipse-center-point* design)
      (multiple-value-bind (r1x r1y r2x r2y) (ellipse-radii design)
        (let ((sa (or (ellipse-start-angle design) 0.0))
              (ea (or (ellipse-end-angle design) (* 2.0 pi))))
          (medium-draw-ellipse* medium cx cy r1x r1y r2x r2y sa ea nil))))))

(defmethod draw-design (medium (design standard-region-union)
                        &rest options &key &allow-other-keys)
  (map-over-region-set-regions (lambda (region)
                                 (apply #'draw-design medium region options))
                               design))

(defmethod draw-design (medium (design standard-rectangle-set)
                        &rest options &key &allow-other-keys)
  ;; ### we can do better (faster) than this.
  (map-over-region-set-regions (lambda (region)
                                 (apply #'draw-design medium region options))
                               design))

(defmethod draw-design (medium (design standard-region-intersection)
                        &rest options &key &allow-other-keys)
  (apply #'draw-design medium +everywhere+ :clipping-region design options))

(defmethod draw-design (medium (design standard-region-complement)
                        &rest options &key &allow-other-keys)
  (apply #'draw-design medium +everywhere+ :clipping-region design options))

(defmethod draw-design (medium (design nowhere-region)
                        &rest options &key &allow-other-keys)
  (declare (ignore medium design options))
  nil)

(defmethod draw-design (medium (design everywhere-region)
                        &rest options &key &allow-other-keys)
  (apply #'draw-design medium
         (bounding-rectangle (medium-clipping-region medium))
         options))

;;;

(defmethod draw-design (medium (color color)
                        &rest options &key &allow-other-keys)
  (apply #'draw-design medium +everywhere+ :ink color options))

(defmethod draw-design (medium (color opacity)
                        &rest options &key &allow-other-keys)
  (apply #'draw-design medium +everywhere+ :ink color options))

(defmethod draw-design (medium (color standard-flipping-ink)
                        &rest options &key &allow-other-keys)
  (apply #'draw-design medium +everywhere+ :ink color options))

(defmethod draw-design (medium (color indirect-ink)
                        &rest options &key &allow-other-keys)
  (apply #'draw-design medium +everywhere+ :ink color options))

;;;

(defmethod draw-design (medium (design over-compositum)
                        &rest options &key &allow-other-keys)
  (apply #'draw-design medium (compositum-background design) options)
  (apply #'draw-design medium (compositum-foreground design) options))

(defmethod draw-design (medium (design in-compositum)
                        &rest options &key &allow-other-keys)
  (let ((mask (compositum-mask design)))
    (if (regionp mask)
        (apply #'draw-design medium mask :ink (compositum-ink design) options)
        (apply #'draw-design medium +everywhere+ :ink design options))))

(defmethod draw-design (medium (design out-compositum)
                        &rest options &key &allow-other-keys)
  (let ((mask (compositum-mask design)))
    (if (regionp mask)
        (apply #'draw-design medium (region-complement mask)
               :ink (compositum-ink design)
               options)
        (apply #'draw-design medium +everywhere+
               :ink design
               options))))

;;;
(defmethod draw-design (medium (design transformed-design) &rest args)
  (with-drawing-options (medium :transformation (transformed-design-transformation design))
    (apply #'draw-design medium (transformed-design-design design) args)))

(defmethod draw-design (medium (pattern pattern)
                        &key clipping-region transformation &allow-other-keys)
  ;; It is said, that DRAW-PATTERN* performs only translation from the supplied
  ;; transformation. If we draw pattern with a DRAW-DESIGN we do apply full
  ;; transformation. That way we have open door for easy drawing transformed
  ;; patterns without compromising the specification. -- jd 2018-09-08
  (let ((width (pattern-width pattern))
        (height (pattern-height pattern)))
    (flet ((draw-it ()
             (draw-rectangle* medium 0 0 width height
                              :ink (transform-region (medium-transformation medium) pattern))))
      (if (or clipping-region transformation)
          (with-drawing-options (medium :clipping-region clipping-region
                                        :transformation  transformation)
            (draw-it))
          (draw-it)))))

(defmethod draw-design (medium (pattern transformed-pattern)
                        &key clipping-region transformation &allow-other-keys)
  (flet ((draw-it ()
           (let* ((pattern-tr (transformed-design-transformation pattern))
                  (pattern-ds (transformed-design-design pattern))
                  (ink-tr (compose-transformations (medium-transformation medium) pattern-tr))
                  (width (pattern-width pattern-ds))
                  (height (pattern-height pattern-ds))
                  (region (transform-region pattern-tr (make-rectangle* 0 0 width height))))
             (draw-design medium region :ink (transform-region ink-tr pattern-ds)))))
    (if (or clipping-region transformation)
        (with-drawing-options (medium :clipping-region clipping-region
                                      :transformation  transformation)
          (draw-it))
        (draw-it))))
