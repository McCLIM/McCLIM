(in-package #:asdf-user)

(defsystem "mcclim-clx"
  :depends-on ("alexandria"
               "babel"
               "cl-unicode"
               "zpb-ttf"
               "trivial-garbage"
               "clx"
               "mcclim-fonts/truetype")
  :serial t
  :components
  ((:module "basic" :pathname "" :components
            ((:file "package")
             (:file "utilities")
             (:file "clipboard")
             (:file "basic" :depends-on ("package"))
             (:file "port" :depends-on ("package" "graft" "basic" "mirror"))
             (:file "frame-manager" :depends-on ("port"))
             (:file "keysyms-common" :depends-on ("basic" "package"))
             (:file "keysymdef" :depends-on ("keysyms-common"))
             (:file "graft" :depends-on ("basic"))
             (:file "cursor" :depends-on ("basic"))
             (:file "mirror" :depends-on ("basic"))
             (:file "window" :depends-on ("basic" "mirror"))))
   (:module "output" :pathname "" :components
            ((:file "bidi" :depends-on ())
             (:file "fonts" :depends-on ("bidi" "medium"))
             (:file "drawing")
             (:file "medium" :depends-on ("drawing"))
             (:file "fonts-truetype" :depends-on ("bidi"))
             (:file "medium-xrender" :depends-on ("medium"))
             (:file "pixmap" :depends-on ("medium"))))
   (:file "input")))

(defsystem "mcclim-clx/freetype"
  :depends-on ("mcclim-clx"
               "mcclim-fonts/clx-freetype"))
