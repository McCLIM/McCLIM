(in-package #:asdf-user)

(defsystem "mcclim-clx-fb"
  :depends-on ("mcclim-clx" "mcclim-render")
  :components
  ((:file "package")
   (:file "port" :depends-on ("package" "medium"))
   (:file "medium" :depends-on ("package"))
   (:file "mirror" :depends-on ("port" "package"))))
