(cl:defpackage "CLIM-TESTS"
  (:use "CLIM-LISP" "CLIM" "CLIME" "FIVEAM")
  (:shadowing-import-from "FIVEAM" "TEST")
  (:import-from "CLIMI"
                "COORDINATE="
                "BASIC-OUTPUT-RECORD"
                "NULL-BOUNDING-RECTANGLE-P")
  (:import-from "CLIMI"
                "DSET" "DREF" "DLET"
                "MAKE-DYNAMIC-VARIABLE" "MAKE-THREAD-LOCAL-VARIABLE"
                "DYNAMIC-VARIABLE-BOUND-P" "DYNAMIC-VARIABLE-MAKUNBOUND"
                "CLASS-WITH-DYNAMIC-SLOTS" "SLOT-DLET")
  (:import-from "CLIM-TEST-UTIL" "FAILS" "COMPILATION-SIGNALS")
  (:export "RUN-TESTS"))

(cl:in-package "CLIM-TESTS")

(def-suite :mcclim)

(defun run-tests ()
  (run! :mcclim))
