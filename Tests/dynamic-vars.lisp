(cl:in-package #:clim-tests)

(def-suite :dynamic-vars :in :mcclim
  :description "Dynamic variables and dynamic slots.")

(def-suite :variables :in :dynamic-vars
  :description "Dynamic variable test suite.")

(def-suite :metaclass :in :dynamic-vars
  :description "Dynamic slots test suite.")

(def-suite :stress :in :dynamic-vars
  :description "Stress tests for dynamic bindings.")

(defmacro with-normal-variable-classes (() &body body)
  `(dolist (*default-dynamic-variable-class*
            '(standard-dynamic-variable surrogate-dynamic-variable))
     ,@body))

(defmacro with-thread-local-variable-classes (() &body body)
  `(dolist (*default-thread-local-variable-class*
            '(standard-thread-local-variable surrogate-thread-local-variable))
     ,@body))

(defmacro with-all-dynamic-variable-classes (() &body body)
  `(dolist (*default-dynamic-variable-class*
            '(standard-dynamic-variable surrogate-dynamic-variable
              standard-thread-local-variable surrogate-thread-local-variable))
     ,@body))

(defmacro with-surrogate-dynamic-variable-classes (() &body body)
  `(dolist (*default-dynamic-variable-class*
            '(surrogate-dynamic-variable surrogate-thread-local-variable))
     ,@body))

(defmacro call-threaded (&body body)
  `(let ((result :not-set))
     (bt:join-thread (bt:make-thread
                      (lambda ()
                        (handler-case
                            (setf result (progn ,@body))
                          (serious-condition (c)
                            (setf result `(:error ,c)))))))
     result))


(in-suite :variables)

(test variables.1.smoke
  "Test basic dvar operators."
  (with-all-dynamic-variable-classes ()
    (let ((dvar (make-dynamic-variable)))
      (is (eql 42 (dset dvar 42)))
      (is (eql 42 (dref dvar)))
      (ignore-errors
       (dlet ((dvar :x))
         (is (eql :x (dref dvar)))
         (error "foo")))
      (is (eql 42 (dref dvar))))))

(test variables.2.boundp
  "Test bound-p operator."
  (with-all-dynamic-variable-classes ()
    (let ((dvar (make-dynamic-variable)))
      (is (not (dynamic-variable-bound-p dvar)))
      (dset dvar 15)
      (is (dynamic-variable-bound-p dvar))
      (dynamic-variable-makunbound dvar)
      (is (not (dynamic-variable-bound-p dvar))))))

(test variables.3.makeunbound
  "Test makunbound operator."
  (with-all-dynamic-variable-classes ()
    (let ((dvar (make-dynamic-variable)))
      (dset dvar t)
      (is (dynamic-variable-bound-p dvar))
      (finishes (dynamic-variable-makunbound dvar))
      (is (not (dynamic-variable-bound-p dvar))))))

(test variables.4.local-boundp
  "Test locally bound-p operator."
  (with-all-dynamic-variable-classes ()
    (let ((dvar (make-dynamic-variable)))
      (is (not (dynamic-variable-bound-p dvar)))
      (dlet ((dvar 15))
        (is (dynamic-variable-bound-p dvar)))
      (is (not (dynamic-variable-bound-p dvar))))))

(test variables.5.local-makunbound
  "Test locally unbound-p operator."
  (with-all-dynamic-variable-classes ()
    (let ((dvar (make-dynamic-variable)))
      (dset dvar t)
      (is (dynamic-variable-bound-p dvar))
      (dlet ((dvar nil))
        (is (dynamic-variable-bound-p dvar))
        (finishes (dynamic-variable-makunbound dvar))
        (is (not (dynamic-variable-bound-p dvar))))
      (is (dynamic-variable-bound-p dvar)))))

(test variables.6.differences
  "Test differences between normal and thread-local variables."
  (with-normal-variable-classes ()
    (let ((dvar (make-dynamic-variable)))
      (dset dvar t)
      (is (dynamic-variable-bound-p dvar))
      (is (call-threaded (dynamic-variable-bound-p dvar)))))
  (with-thread-local-variable-classes ()
    (let ((dvar (make-thread-local-variable)))
      (dset dvar t)
      (is (dynamic-variable-bound-p dvar))
      (is (null (call-threaded (dynamic-variable-bound-p dvar)))))))

(test variables.7.thread-local.initfun
  "Test thread local variable initfun."
  (with-thread-local-variable-classes ()
    (let* ((counter 0)
           (dvar (make-thread-local-variable
                  :initfun (lambda () (incf counter)))))
      (is (= 1 (dref dvar)))
      (is (= 1 (dref dvar)))
      (is (= 2 (call-threaded (dref dvar))))
      (is (= 1 (dref dvar)))
      (dlet ((dvar :a))
        (is (= 3 (call-threaded (dref dvar))))
        (is (eq :a (dref dvar)))))))

(test variables.8.thread-local.initval
  "Test thread local variables initval."
  (with-thread-local-variable-classes ()
    (let* ((counter 0)
           (dvar (make-thread-local-variable
                  :initval counter)))
      (is (= 0 (dref dvar)))
      (is (= 0 (dref dvar)))
      (is (= 0 (call-threaded (dref dvar))))
      (incf counter)
      (is (= 0 (dref dvar)))
      (is (= 0 (call-threaded (dref dvar)))))))


(in-suite :metaclass)

(defclass c1 ()
  ((slot1 :initarg :slot1 :dynamic nil :accessor slot1)
   (slot2 :initarg :slot2 :dynamic t   :accessor slot2)
   (slot3 :initarg :slot3 :dynamic :tls :accessor slot3))
  (:metaclass class-with-dynamic-slots))

(defmacro with-dynamic-slot-variants (() &body body)
  `(with-normal-variable-classes ()
     (with-thread-local-variable-classes ()
       ,@body)))

(test metaclass.1.smoke
  (with-dynamic-slot-variants ()
    (let ((o1 (make-instance 'c1 :slot1 :a :slot2 :b :slot3 :c))
          (o2 (make-instance 'c1 :slot1 :x :slot2 :y :slot3 :z)))
      (with-slots (slot1 slot2 slot3) o1
        (is (eq :a slot1))
        (is (eq :b slot2))
        (is (eq :c slot3)))
      (with-slots (slot1 slot2 slot3) o2
        (is (eq :x slot1))
        (is (eq :y slot2))
        (is (eq :z slot3))))))

(test metaclass.2.binding
  (with-dynamic-slot-variants ()
    (let ((o1 (make-instance 'c1 :slot1 :a :slot2 :b :slot3 :c))
          (o2 (make-instance 'c1 :slot1 :x :slot2 :y :slot3 :z)))
      (signals error (slot-dlet (((o1 'slot1) 1)) nil))
      (slot-dlet (((o1 'slot2) :k))
        (is (eq :k (slot-value o1 'slot2)))
        (is (eq :y (slot-value o2 'slot2))))
      (slot-dlet (((o1 'slot3) :l))
        (is (eq :l (slot-value o1 'slot3)))
        (is (eq :z (slot-value o2 'slot3)))))))

(test metaclass.3.concurrent
  (with-dynamic-slot-variants ()
    (let ((o1 (make-instance 'c1 :slot1 :a :slot2 :b :slot3 :c))
          (fail nil)
          (pass nil))
      (is (eq (slot1 o1) :a))
      (is (eq (slot2 o1) :b))
      (is (eq (slot3 o1) :c))
      (flet ((make-runner (values)
               (lambda ()
                 (setf (slot1 o1) :x
                       (slot2 o1) :y
                       (slot3 o1) :z)
                 (setf pass (and (eq (slot1 o1) :x)
                                 (eq (slot2 o1) :y)
                                 (eq (slot3 o1) :z)))
                 (slot-dlet (((o1 'slot2) :start))
                   (let ((value (slot2 o1)))
                     (unless (eq value :start)
                       (setf fail value)))
                   (loop repeat 1024 do
                     (setf (slot2 o1) (elt values (random (length values))))
                     (let ((value (slot2 o1)))
                       (unless (member value values)
                         (setf fail value))))))))
        (let ((threads (loop for i from 0 below 64
                             for v = (list (make-symbol (format nil "A~d" i))
                                           (make-symbol (format nil "B~d" i))
                                           (make-symbol (format nil "C~d" i)))
                             collect (bt:make-thread (make-runner v)))))
          (map nil #'bt:join-thread threads)
          (is (eq (slot1 o1) :x))
          (is (eq (slot2 o1) :y))
          (is (eq (slot3 o1) :c))       ; <- tls variable
          (is (not (null pass)))
          (is (null fail)))))))


(in-suite :stress)

(test stress.9
  "Stress test the implementation (see :FAKE-PROGV-KLUDGE)."
  (finishes                              ; at the same time
    (#-fake-progv-kludge with-all-dynamic-variable-classes
     #+fake-progv-kludge with-surrogate-dynamic-variable-classes ()
     (let ((dvars (loop repeat 4096 collect (make-dynamic-variable))))
       ;; ensure tls variable
       (loop for v in dvars do
         (dlet ((v 1))))
       (loop for i from 0 below 4096
             for r = (random 4096)
             for v1 in dvars
             for v2 = (elt dvars r) do
               (when (zerop (mod i 512))
                 (pass))
               (dlet ((v1 42)
                      (v2 43))
                 (values)))))))

(test stress.0
  "Stress test the implementation (see :FAKE-PROGV-KLUDGE)."
  (finishes                             ; can be gc-ed
    (#-fake-progv-kludge with-all-dynamic-variable-classes
     #+fake-progv-kludge with-surrogate-dynamic-variable-classes ()
     (loop for i from 0 below 4096 do
       (when (zerop (mod i 512))
         (pass))
       (dlet (((make-dynamic-variable) 42))
         (values))))))
