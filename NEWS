* Changes in mcclim-0.9.9 relative to 0.9.8 Yule:

** [2025-02-25 Tue] Flesh out timer gesture support with a demo

Now it is possible to have a timer gesture as a keystroke value in the command
table accelerators. This functionality is features in a demo Timer Gestures.

** [2025-02-20 Thu] Introduce new demos in CLIM-EXAMPLES

"Incremental Redisplay" shows how to use UPDATING-OUTPUT
"Animation with pulse" shows how to make animations in the repaint loop
"Concurrent drawing" stress tests stream written from multiple threads
"Concurrent text" demonstrates that drawing options are thread-local
"Concurrent grid" stress tests the output history performance with many records

** [2025-02-20 Thu] Thread-safe writing to CLIM Streams

Streams that feature output recording are thread-safe with regard to drawing and
writing text to them as well as with drawing options. Medium is now thread-local
and output history manipulation is protected with a read-write lock.

Additional performance improvements are achieved thanks to batching repaint,
scroll and resize requests until finish-output is called. Thread safety allows
for concurrent drawing to a stream from multiple threads or using updating
output records to modify existing objects.

From now on the correct order of operation is to record output and then replay
from the history. Directly drawing on the sheet without output recording is
deprecated. Output recording options are managed by McCLIM control loop.

** [2025-02-11 Tue] Introduce a new interpretation of the parameter MOVE-CURSOR

In the macro SURROUNDING-OUTPUT-WITH-BORDER the macro :MOVE-CURSOR T means that
the record is added to the current text line (so its position may change), while
:MOVE-CURSOR NIL means that the record position is fixed.

** [2025-02-11 Tue] Add gutters in formatting macro cells

This ensures that graphics that do not intersect with the point [0,0] have the
correct offset in cells and enforces minimal dimensions

** [2025-01-31 Fri] Add specialization for CLIM-STREAM-PANES to COPY-AREA

** [2024-09-03 Tue] Rewrite POINTER-DOCUMENTATION to fix performance issues

This involves better caching and more responsive handler when we wait for input.

** [2024-07-14 Sun] fix a memory leak in clx backend

** [2024-06-05 Wed] WITH-OUTPUT-TO-PIXMAP creates a pixmap with transparent background

It is not necessary to clean it anymore. MEDIUM-CLEAR-AREA handles now
transparency as expected too (i.e it uses :src operation instead of :over).

** [2024-06-04 Tue] ps/pdf backends use the correct transformation for text

** [2024-05-14 Tue] implement vertical text for backends using ttf renderer

This change also makes transform-glyphs work as specified in the specificaiton.

** [2024-03-24 Sun] allow selecting children of the presentation sequence

** [2024-03-24 Sun] bugfix: repaint scroll bars consistently

** [2024-02-13 Tue] Performance improvements for X11 text rendering

The untransformed text renders around 1.5x faster and the transformed text
renders around 400x faster.

** [2024-02-08 Thu] DRAW-TEXT implements now :TOWARD-POINT parameter

On top of the current device transformation, TOWARD-POINT specifies the text
direction.

A new demo interactive demo "Draw Text test" has been implemented that allows
experimentation with the text alignment, direction and transformations.

** [2024-02-19 Mon] UPDATING-OUTPUT does not overwrite CACHE-TEST when supplied

Previously when CACHE-VALUE was not supplied, then CACHE-TEST was set to
FORCE-UPDATE-CACHE-TEST, even when the programmer explicitly specified
:CACHE-TEST (CONSTANTLY T) ; from now on the CACHE-TEST is supplanted only when
neither CACHE-VALUE nor CACHE-TEST are supplied by the programmer.

** [2024-01-24 Wed] Change the class hierarchy of extended output streams

STANDARD-OUTPUT-RECORDING-STREAM is a superclass of
STANDARD-EXTENDED-OUTPUT-STREAM (previously it was the other way around).

** [2024-01-12 Fri] WITH-ROOM-FOR-GRAPHICS allows mixing text and graphics

Previously this operator was used to estabilish the transformation and local
coordinate system. These effects are achievable by using other operators.

From now on it works only on streams that maintain the text line and is used
strictly to incorporate graphics on the text line.

A new demo has been added that illustrates this new functionality.

** [2024-01-12 Fri] Rewrite STANDARD-TEXT-DISPLAYED-OUTPUT-RECORD to use DRAW-TEXT

This includes improving the implementation of the STANDARD-TEXT-CURSOR and
fixing bugs related to line breaks where the cursor did not change its size.

** [2024-01-12 Fri] Generalize the concept of a baseline for vertical directions

Additionally all output records may have a baseline, not only the text. That
will support future improvements to formatting operators.

** [2024-01-11 Thu] Implement the missing operator MAKE-DESIGN-FROM-OUTPUT-RECORD

This operator allows for incorporating graphical output records in drawing
functions.

** [2024-01-11 Thu] DRAW-TEXT is a subject of the medium transformation

Previously we've taken an interpretation that the argument :TRANSFORM-GLYPHS
decides whether the string is transformed or not; that interpretation was wrong
because this argument has a different purpose.

** [2024-01-06 Sat] Replaying the output does not change the stream's text cursor

The function REPLAY will save and restore the text cursor after execution.

** [2024-01-06 Sat] WITH-OUTPUT-TO-OUTPUT-RECORD does not terminate the current text line

For the extent of the macro body the text line is closed, but afterwards it is
reopened and the cursor position is re-set to the old position.

** [2024-01-06 Sat] Multi-directional pages for STANDARD-EXTENDED-OUTPUT-STREAM

It is now possible to specify the line and the page directions. Elements are
added piece-wise on the baseline that may be either vertical or horizontal.

#+begin_src lisp
  ;; New initargs (applies also to CLIM-STREAM-PANE as a subclass of SEOS):
  :line-direction [ :left-to-right :right-to-left :top-to-bottom :bottom-to-top ]
  :page-direction [ :top-to-bottom :bottom-to-top :left-to-right :right-to-left ]
#+end_src

Consequences are undefined when the line direction and the page direction are on
the same axis.

* Changes in mcclim-0.9.8 relative to 0.9.7:
** Listener UX improvements
** Refactor command tables and commands parsing
** Refactor of the sheet and the mirror implementation
** Numerous new examples
** Unification of CLX backends into a single system
** bugfix: TTF font caches do not overlap between backends
** Add an alternative (not default) TTF renderer based on FFI to FreeType
** bugfix: address numerous issues in tab-layout
** Improved support for text transformations
** Bundle cl-dejavu fonts as a dependency
** Portability fixes for CL implementations
** Better separation of responsibilities between sheets and mediums
** TABLE-FORMATTING improvements
** Support for system clipboard and for internal selection
** Remove invalid assumptions about the sheet coordinates while scrolling
** Full rewrite of CLX renderer to use XRender (transparency, transformations, clipping)
** Incremental redisplay partial rewrite and bug fixes
** UX imrovements for gadget states with regard to pointer events
** Bug fixes and conformity improvements for PS and PDF backends
** Pull bezier curve and bezier area extensions as core regions
** Improved set of colors for MAKE-CONTRASTING-INKS
** Improved set of patterns for MAKE-CONTRASTING-DASH-PATTERNS
** A new protocol for handling patterns, transformations and arbitrary designs
** Rewrite mcclim-render extension
** Enable rendering directly to the pattern or a raster file
** Flesh out the protocol to implement indrect inks
** Remove obsolete backend "beagle"
** Rewrite of the demo DRAWING-TESTS
** Improved support for arcs, angles and ellipses
** Geometry module partial rewrite - conformance and performance improvements
** Improved class hierarchy for input events
** Space requirements rewrite to allow specification of padding and margins
** Menu bar rewrite to handle keyboard accelerators and arrows
** Remove runtime overhead from protocol classes
** More conforming handling of line style unit
** Introduce the concept of a page with four margins
** Implement word wrap and extend FILLING-OUTPUT and INDENTING-OUTPUT macros
** Rewrite PRESENTATION-SUBTYPEP and PRESENTATION-TYPEP for conformance
** Much faster TTF renderer, implement kerning and tracking, thread safe rendering
** Pixel-perfect FONT-TEXT-EXTENT implementation for the TTF renderer, transformations
** Gadgets UX improvements for both space composition and event handling
** Better PRINT-OBJECT methods for numerous internal objects
** Rewrite EVENT-QUEUE for thread safety and performance, extend TIMEOUT interface
** Fix McCLIM regression to work on non-SMP systems
** Improved parsing for space requirements
** Rewrite the implementastion of mapping between text styles and fonts
** Refactor of the EXTENDED-INPUT-STREAM
** Add numerous missing ACCEPT-PRESENT-DEFAULT methods for dialog-views
** Improved repaint-sheet handling (less repaints, more performant)
** Improvements to numerous presentation methods in functions ACCEPT and PRESENT
** Presentation translator lookup rewrite
** Selection API can carry arbitrary presentations with "paste" translators
** Extensions documentation in manual, various fixups and improvements
** Specification clarifications in the bundled latex source code
** Implement double buffering in the CLX backend
** New protocols to set the frame (and sheet) pretty name and icon
** Refactor mirrored/non-mirrored event distribution and add synthetic events
** Rewrite pointer tracking, pointer grabbing and drag&drop support
** Include the new inspector "Clouseau" with bundled applications, remove old the inspector
** Put Franz-specific extensions to the system "mcclim-franz"
** A new command parameter OUTPUT-DESTINATION to redirect the command output
** Improve frame and panes definitions to allow dynamic reinitialization
** Improve PROCESS-NEXT-EVENT specification and implementations for clarity and completness
** It is possible to define a presentation to command translator with the argument
** Rework accepting values to remove a few kludges
** Refactor FORMAT-GRAPH-FROM-ROOT and the layout algorithm for correctness
** Numerous cleanups to avoid accessing internal symbols and have a clean file structure
** Introduce the presentation BLANK-AREA (similar to NULL-PRESENTATION) that allows parameters
** Provide an uniform interface for mapping over command table menu items and keystrokes
** Improve the protocol for defining new ports (backends)
** Allow for existance of ungrafted mediums
** Allow runtime replacement of the menu bar in the frame
** Better spearation of responsibilities between frames and frame managers
** Improvemed gesture and event matching that allows wildcards
** Minor integrations between CLIM-DEBUGGER and Clouseau
** Removal of numerous internal obsolete interfaces
** Thread-safe implementation of the function EXECUTE-FRAME-COMMAND
** Better support for repeated grafting and degrafting sheets (migrating frames between backends)
** Implement a new backend that creates SVG documents
** Introduce a new macro for drawing backends CLIME:WITH-OUTPUT-TO-DRAWING-STREAM
** Include a new extension "mcclim-tooltips"
** Include a new extension "mcclim-dot"
** Write new implementation of text-field and text-editor gadgets (with pointer selection etc)
** Allow for preloaded fonts in the application image
** Flesh out important protocols useful for writing new backends
** Add a new gesture types :TIMER, :INDIRECT, :POINTER-MOTION and :POINTER-SCROLL
** TTF renderer consults the graft DPI to decide on the font scaling

* Changes in mcclim-0.9.7 relative to 0.9.6:
** Bug fix: Many! (and some regressions).
** Bug fix: tab-layout fixes.
** Bug fix: formatting-table fixes.
** Bug fix: scrolling and viewport fixes and refactor.
** Feature: raster image draw backend extension.
** Feature: bezier curves extension.
** Feature: new tests and demos in clim-examples.
** Feature: truetype rendering is now default on clx.
** Feature: additions to region, clipping rectangles and drawing.
** Feature: clim-debugger and clim-listener improvmenets.
** Feature: mop is now done with CLOSER-MOP.
** Feature: threading is now done with BORDEAUX-THREADS.
** Feature: clx-fb backend (poc of framebuffer-based backend).
** Feature: assumption that all panes must be mirrored has been removed.
** Cleanup: many files cleaned up from style warnings and such.
** Cleanup: removal of PIXIE.
** Cleanup: removal of CLIM-FFI package.
** Cleanup: changes to directory structure and asd definitions.
** Cleanup: numerous manual additions and corrections.
** Cleanup: broken backends has been removed.
** Cleanup: goatee has been removed in favour of Drei.
** Cleanup: all methods have now corresponding generic function declarations.

* Changes in mcclim-0.9.6 relative to 0.9.5:
** Bug fix: ESA's help commands are better at finding bindings and
   describing them
** Bug fix: Some missing methods and functions have been implemented
   for the Null backend, allowing headless operation for many
   applications.
** Specification compliance: READ-BITMAP-FILE and
   MAKE-PATTERN-FROM-BITMAP-FILE from CLIM 2.2. Includes new example
   program, IMAGE-VIEWER.
** Drei improvements
*** New redisplay engine that is faster and has more features.
*** Support for "views" concept.
*** Support for modes a la Emacs "mini-modes".
*** Bug fix: Input prompting now works for directly recursive calls to
    ACCEPT.
*** Improvement: Goal-columns for line movement.
*** Improvement: More Emacs-like expression movement for Lisp syntax.
** Bug fix: label panes no longer have a restrictive maximum width.
** Bug fix: ellipses with a zero radius no longer cause errors.
** Bug fix: bezier drawing in CLIM-FIG less likely to cause errors.
** Bug fix: restored somewhat working undo in CLIM-FIG.
** Specification compliance: The :inherit-menu keyword argument to
   DEFINE-COMMAND-TABLE and MAKE-COMMAND-TABLE is now implemented with
   CLIM 2.2 semantics. The :keystrokes value is not handled yet.
** Specification compliance: :PRINTER functions for MENU-CHOOSE are
   now called with the menu item, not the display object.
** Improvement: Faster drawing and AA text rendering. AA text requires
   a fix to the Xrender support of CLX, available in Christophe Rhodes's
   current CLX distribution from darcs.
** Improvement: Look up arbitrary truetype fonts by name via fontconfig.
** New extension: mcclim-truetype: provides a 100% lisp path for
   AA fonts with CLX using cl-vectors and zpb-ttf, as an alternative
   to mcclim-freetype.
** Bug fix: correct computation of bounding rectangle after
   clear-output-record and recompute-extent-for-new-child.

* Changes in mcclim-0.9.5 relative to 0.9.4:
** Installation: the systems clim-listener, clim-examples,
   and clouseau can now be loaded without loading the system mcclim
   first. Users with existing McCLIM installations should use the
   provided script:
                ./symlink-asd-files.sh /path/to/asdf-central-registry/
** New extension: tab-layout. This extension allows keeping a stack of panes
   whose foreground pane is controlled by a tab bar. This layout can be
   customized in backends and frame managers. For examples, see the
   gtkairo backend and the pixie frame manager.
** New extension function: SHEET-RGB-IMAGE: makes a screenshot of a sheet
   in the CLX backend. (Supported on truecolor visuals only for now.)
** New experimental extension: tree-with-cross-edges are an extension to
   the graph formatter.
** New experimental backend: clim-graphic-forms: native widgets on Windows.
   This backend is still very experimental (it doesn't run demos yet).
** New inspector feature: The inspector now displays more useful information
   about hash tables and generic functions.
** Specification compliance: Various layout panes no longer quite as
   aggressive at eating the space requirements of their children.
** Specification compliance: There is now a rudimentary implementation of
   NOTIFY-USER
** Usability: Text editors and text input panes now use click-to-focus.
** Improvement: the ACCEPTING-VALUES command table was renamed to
   ACCEPT-VALUES (as this is the name that the other clim-2 implementation
   uses)
** Improvement: the CLX backend should no longer cause focus stealing
   when an application has text-editor panes.  This change comes with
   a rudimentary click-to-focus-keyboard widget policy.
** Improvement: define-application-frame now allows a :default-initargs
   option. (This is not exactly a "specification compliance" fix, as
   d-a-frame is not defined to accept this option.).
** Improvement: menu-choose menus now look a little prettier.
** Improvement: added more styles for bordered-output: :rounded, :ellipse
** Improvement: Toggle button values now default to NIL.
** Improvement: Frame layouts are now inherited from the frame's
   superclass.
** Improvement: The Lisp Syntax is much improved: now recognizes
   delimiter characters, and more types of Lambda lists.
** Bug fix: Bezier designs should now draw in the right place in all
   backends.
** Bug fix: Text in Drei no longer "walks" to the left.
** Bug fix: Drei now has better support for delimiter gestures.
** Bug fix: Partial commands now work better when invoked from the menu.


* Changes in mcclim-0.9.4 relative to 0.9.3:
** cleanup: removed the obsolete system.lisp file.
** backend improvements: Gtkairo
*** Double buffering is now supported (fixes disappearing widgets on Windows).
*** X errors no longer terminate the lisp process.
*** Some bugfixes, including CMUCL support and better key event handling.
*** Native implementation of context menus, list panes, label panes, and
    option panes.
*** Draw text using Pango.  (Bug fix: Fixed-width font supported on Windows
    now.  Multiple lines of output in TEXT-SIZE supported now.
    TEXT-STYLE-FIXED-WIDTH-P works correctly now.)
** Improvement: Added new editor substrate ("Drei").
** Improvement: Improved the pathname presentation methods considerably.
** specification compliance: DELETE-GESTURE-NAME function now implemented.
** specification compliance: PRESENTATION-TYPE-SPECIFIER-P presentaion
   function now implemented.
** specification compliance: DISPLAY-COMMAND-TABLE-MENU function now
   implemented.
** specification compliance: DISPLAY-COMMAND-MENU function now
   implemented.
** specification compliance: POINTER-PLACE-RUBBER-BAND-LINE* function
   now implemented.
** specification compliance: POINTER-INPUT-RECTANGLE* function now
   implemented.
** specification compliance: POINTER-INPUT-RECTANGLE function now
   implemented.
** Improvement: Added font listing support, see section "Fonts and Extended
   Text Styles" in the manual.
** Improvement: Added support for bezier splines (Robert Strandh).
   To be documented.
** better PRESENTATION-SUBTYPEP (more likely to give the right answer
   on some-of and all-of presentation types)
** Improvement: M-n/M-p gestures for navigating presentation histories.

* Changes in mcclim-0.9.3 "All Souls' Day" relative to 0.9.2:
** backend improvement: The Null backend now registers itself in the
   server search path
** improvement: with-output-as-gadget now sets the correct cursor
   position when incremental redisplay is active.
** specification compliance: INVOKE-WITH-NEW-OUTPUT-RECORD's argument
   list now is the same as the one in the Franz CLIM user guide.
** improvement: The text field cursor is now a solid block again.
** backend improvement: the PostScript backend now outputs correct EPS
** improvement: Graph nodes can now be dragged
** improvement: Possibilities when reading from
   COMPLETE-FROM-GENERATOR are now sorted alphabetically.
** new experimental backend: gtkairo (loads on SBCL, CMUCL and SCL):
   Uses GTK+ for gadgets and cairo for rendering graphics.
** Bug fix: incremental-redisplay does no longer leak memory
** improvement: incremental-redisplay is now a little faster
** Bug fix: Invisible text cursors no longer leave a dangling space
   behind the text output record
** improvement: commands whose names are shadowed in child command
   tables are now suggested in preference to their parents.
** Bug fix: (setf stream-cursor-position) and output record replay on
   encapsulating streams work now.
** Bug fix: Invoking command menu items in frames with no interactor
   works now.
** Bug fix: DESTROY-PORT removes the port even if an error occurs
   while closing the port
** Bug fix: make-process now sets the process name on SBCL
** specification compliance: MENU-CHOOSE now supports almost all
   features demanded in the CLIM 2.0 specification.
** improvement: new and improved ACCEPT presentation method for
   expressions on interactive streams.
** specification compliance: LOOKUP-KEYSTROKE-ITEM no longer accepts
   the :errorp argument.
** Bug fix: incremental redisplay no longer breaks on output records
   that had no children.
** Bug fix: arrow head sizes are now transformed along with the line thickness.
** improvement: resizing a viewport's child will now move the viewport's focus.
** improvement: loading mcclim.asd no longer shows a code deletion note on SBCL.
** new demo: logic-cube
** compatibility: Add support for post-1.0 openmcl, and for Allegro
   Common Lisp 8.0 (ansi mode).
** new example application showing use of CLIM views.
