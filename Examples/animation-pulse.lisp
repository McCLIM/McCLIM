;;; ---------------------------------------------------------------------------
;;;   License: BSD-2-Clause.
;;; ---------------------------------------------------------------------------
;;;
;;;  (c) Copyright 2025 Daniel Kochmański <daniel@turtleware.eu>
;;;
;;; ---------------------------------------------------------------------------
;;;
;;; In this demo I'm demonstrating how to implement animations using the pulse
;;; event. The code illustrates both repaint (low-level) and display (CLIM) way
;;; of doing that.
;;;
;;; This code is much faster than concurrent drawing from different threads,
;;; because there are no competition to access the output record history.

(defpackage "CLIM-DEMO.ANIMATION-PULSE"
  (:use "CLIM-LISP" "CLIM")
  (:export "ANIMATION-PULSE"))
(in-package "CLIM-DEMO.ANIMATION-PULSE")


;;; Drawing functions and utilities

(defun random-ink ()
  (make-contrasting-inks 8 (random 8)))

(defun timed-ink (n)
  (make-contrasting-inks 8 (mod (+ (get-universal-time) n) 8)))

(defun paint-1 (frame sheet)
  (declare (ignore frame))
  (let* ((text (format nil "Hello world!~%You know~%This is our home!"))
         (tau (get-internal-real-time))
         (rot (mod (/ tau internal-time-units-per-second) 360))
         (ink1 (compose-in +blue+ (make-opacity .3)))
         (ink2 (compose-in +red+  (make-opacity .3))))
    (draw-text* sheet text 100 100 :align-x :center :align-y :center)
    (with-rotation (sheet rot (make-point 100 100))
      (draw-rectangle* sheet 50 50 150 150 :ink ink1)
      (draw-circle* sheet 50 50 15 :ink +deep-pink+))
    (with-rotation (sheet (- rot) (make-point 100 100))
      (draw-rectangle* sheet 50 50 150 150 :ink ink2)
      (draw-circle* sheet 150 150 15 :ink +deep-sky-blue+))
    (dotimes (v 10)
      (draw-point* sheet (random 200) (random 200)
                   :line-thickness 30
                   :ink (compose-in +dark-green+
                                    (make-opacity .8))))))

(defun paint-2 (frame sheet)
  (declare (ignore frame))
  (loop for x from 0 below 200 by 10 do
    (loop for y from 0 below 200 by 10 do
      (draw-rectangle* sheet x y (+ x 10) (+ y 10) :ink (random-ink)))))

(defun paint-3 (frame sheet)
  (declare (ignore frame))
  (draw-rectangle* sheet  25  25 100 100 :ink (timed-ink 0))
  (draw-rectangle* sheet 100  25 175 100 :ink (timed-ink 1))
  (draw-rectangle* sheet  25 100 100 175 :ink (timed-ink 2))
  (draw-rectangle* sheet 100 100 175 175 :ink (timed-ink 3)))


;;; Sheets and events.

(defclass common-mixin ()
  ((pulse-running-p :initform nil :accessor pulse-running-p))
  (:default-initargs :region (make-rectangle* 0 0 200 200)))

(defclass my-pane (common-mixin basic-pane)
  ((repaint-function :initarg :repaint-function :reader repaint-function))
  (:default-initargs :repaint-function 'paint-1))

(defclass my-rapp (common-mixin clim-stream-pane)
  ()
  (:default-initargs :display-function 'paint-1))

(defun dispatch-redisplay (pane)
  (with-output-recording-options (pane :draw nil :record t)
    (clear-output-record (stream-output-history pane))
    (funcall (climi::pane-display-function pane)
             (pane-frame pane) pane))
  (dispatch-repaint pane +everywhere+))

(defun toggle-pulse (pane)
  (setf (pulse-running-p pane)
        (not (pulse-running-p pane)))
  (when (pulse-running-p pane)
    (clime:schedule-pulse-event pane :boom 0.001)))

(defun handle-pulse (pane)
  (etypecase pane
    (my-pane (dispatch-repaint pane +everywhere+))
    (my-rapp (dispatch-redisplay pane))))

(defmethod handle-event
    ((self common-mixin) (event climi::pulse-event))
  (if (and (pulse-running-p self)
           (sheet-grafted-p self))
      (handle-pulse self)
      (clime:delete-pulse-event event)))

(defmethod handle-event
    ((self common-mixin) (event pointer-button-press-event))
  (toggle-pulse self))

(defmethod compose-space ((self common-mixin) &key width height)
  (declare (ignore width height))
  (make-space-requirement :width 200 :height 200))

(defmethod handle-repaint ((object my-pane) region)
  (declare (ignore region))
  (with-application-frame (frame)
    (funcall (repaint-function object) frame object)))


;;; Let there be light
(defun explanation-display (frame stream)
  (declare (ignore frame))
  (format stream "Click panes to enable and disable animations.
Panes on the left are updated on each repaint.
Panes on the right are update after redisplay.
Animation is synchronous with the event queue.")
  (finish-output stream))

(define-application-frame animation-pulse ()
  ()
  (:pane (vertically ()
           (horizontally ()
             (labelling (:label "repainted")
               (vertically ()
                 (make-pane 'my-pane :repaint-function 'paint-1)
                 (make-pane 'my-pane :repaint-function 'paint-2)
                 (make-pane 'my-pane :repaint-function 'paint-3)))
             (labelling (:label "displayed")
               (vertically ()
                 (make-pane 'my-rapp :display-function 'paint-1)
                 (make-pane 'my-rapp :display-function 'paint-2)
                 (make-pane 'my-rapp :display-function 'paint-3))))
           
             (make-pane :application
                        :display-time t
                        :display-function 'explanation-display
                        :background +beige+))))

;(find-application-frame 'animation-pulse)
