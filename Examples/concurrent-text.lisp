;;; ---------------------------------------------------------------------------
;;;   License: BSD-2-Clause.
;;; ---------------------------------------------------------------------------
;;;
;;;  (c) Copyright 2025 Daniel Kochmański <daniel@turtleware.eu>
;;;
;;; ---------------------------------------------------------------------------
;;;
;;; In this demo I'm demonstrating concurrent drawing and writing to a stream.
;;; This demo is different from concurrent-draw.lisp because it does not use
;;; UPDATING-OUTPUT (hence it is less buggy). It is possible to scroll output
;;; while new lines are added. The main purpose is to verify that drawwing
;;; options are not leaking into another thread.
;;;
;;; We display a rotating gadget at the beginning so that it is possible to
;;; verify that while the text is added incrementally the animation progresses.

(defpackage "CLIM-DEMO.CONCURRENT-TEXT"
  (:use "CLIM-LISP" "CLIM")
  (:export "CONCURRENT-TEXT"))
(in-package "CLIM-DEMO.CONCURRENT-TEXT")

;;; Prints slips every second line that end with an index. This is the first
;;; synchronous pass in the demo so tha we ensure the stream size eagerly -
;;; thanks to that we don't need to call FINISH-OUTPUT in other drawers to
;;; resize the sheet after each draw.
(defun run-slip (stream iterations)  ; synchronous
  (dotimes (i iterations)
    (let* ((y1 (* i 50))
           (y2 (+ y1 24)))
      (draw-rectangle* stream 0 y1 400 y2 :ink +light-grey+)
      (draw-text* stream (format nil "~4d" i) 350 (+ y1 12) :align-y :center)))
  (finish-output stream))

;;; Prints a circle every second line. They are meant to be aligned with other
;;; drawers on each line, so we allow specifying the line offset.
(defun run-draw (stream iterations line-offset ink)
  (dotimes (i iterations)
    (with-drawing-options (stream :ink ink)
      (let* ((radius 12)
             (height 50)                ; 2*line height (guessed)
             (center-x (+ line-offset radius))
             (center-y (+ (* i height) radius)))
       (draw-circle* stream center-x center-y radius)))))

;;; Prints text lines with a specified ink. It is important that inks are not
;;; mixed between lines and that they are not corrupted (i.e double newline).
(defun run-text (stream iterations prefix ink)
  (let* ((frame (pane-frame stream))
         (incer (cond ((string= prefix "XXX")
                       (lambda (i) (setf (xxx frame) i)))
                      ((string= prefix "YYY")
                       (lambda (i) (setf (yyy frame) i))))))
    (dotimes (i iterations)
      (funcall incer i)
      ;; It would be more efficient to set drawing options over the loop but
      ;; setting it inside puts more stress on the stream.
      (with-drawing-options (stream :ink ink)
        (format stream "~a: ~5d~%" prefix i)))))

(define-application-frame concurrent-text ()
  ((xxx :initform 0 :accessor xxx)
   (yyy :initform 0 :accessor yyy))
  (:pane :application :display-time nil
         :text-style (make-text-style :fix nil nil)
         :end-of-page-action :allow
         :width 600 :height 800 :scroll-bars :vertical))

(define-concurrent-text-command (com-start :menu t) ()
  (let* ((frame *application-frame*)
         (stream *standard-output*)
         (iterations 10000)
         (done-p nil)
         (spin-rot 0)
         (spin-ink +deep-pink+))
    (setf (xxx frame) 0 (yyy frame) 0)
    (window-clear stream)
    (bt:make-thread
     (lambda ()
       (flet ((progress-spinner ()
                (updating-output (stream :unique-id :spinner)
                  (with-rotation (stream spin-rot (make-point 500 100))
                    (draw-rectangle* stream 450 50 550 150 :filled nil
                                                           :line-thickness 4
                                                           :ink spin-ink))
                  (draw-text* stream
                              (format nil "XXX ~a/~a~%YYY ~a/~a"
                                      (1+ (xxx frame)) iterations
                                      (1+ (yyy frame)) iterations)
                              500 100
                              :align-x :center
                              :align-y :center
                              :text-size :tiny))))
         (loop until done-p do
           (progress-spinner)
           (incf spin-rot (/ pi 60))
           (sleep 1/30))
         (progress-spinner))))
    ;; We spin a thread so that the main thread is not blocked.
    (bt:make-thread
     (lambda ()
       (time
        (flet ((spin-draw (offset ink)
                 (bt:make-thread
                  (lambda () (run-draw stream iterations offset ink))))
               (spin-text (prefix ink)
                 (bt:make-thread
                  (lambda () (run-text stream iterations prefix ink)))))
          (run-slip stream iterations)
          (setf spin-ink +deep-sky-blue+)
          (map nil #'bt:join-thread (list (spin-text "XXX" +dark-red+)
                                          (spin-text "YYY" +dark-blue+)
                                          (spin-draw 150 +deep-pink+)
                                          (spin-draw 200 +dark-green+)
                                          (spin-draw 250 +purple+)
                                          (spin-draw 300 +dark-grey+)))
          (finish-output stream)
          (setf spin-ink +dark-grey+)
          (setf done-p t)))))))

;; (find-application-frame 'concurrent-text)
