;;; ---------------------------------------------------------------------------
;;;   License: BSD-2-Clause.
;;; ---------------------------------------------------------------------------
;;;
;;;  (c) Copyright 2025 Daniel Kochmański <daniel@turtleware.eu>
;;;
;;; ---------------------------------------------------------------------------
;;;
;;; In this demo I'm demonstrating concurrent drawing to a stream. Note that
;;; only CLIM-STREAM-PANE instances are thread-safe for drawing. It is possible
;;; to implement animations using this method, but they are not very performant
;;; because the output record history is a shared resource. That said it is a
;;; good way to show output of independent programs and for rapid prototyping.
;;; 
;;; Note that thread-safety applies to the history access and drawing options.
;;; There are still possible race conditions on the display level and other
;;; frame operations. Generally this demo is experimental.

(defpackage "CLIM-DEMO.CONCURRENT-DRAW"
  (:use "CLIM-LISP" "CLIM")
  (:export "CONCURRENT-DRAW"))
(in-package "CLIM-DEMO.CONCURRENT-DRAW")

(defgeneric display (frame stream))

(defun random-ink ()
  (compose-in (make-contrasting-inks 8 (random 8))
              (make-opacity .7)))

(define-border-type :matrix (stream left top right bottom)
  (draw-polygon* stream
                 (list (+ left 15) top
                       (- left  5) top
                       (- left  5) bottom
                       (+ left 15) bottom)
                 :filled nil :closed nil :ink +red+
                 :line-thickness 2)
  (draw-polygon* stream
                 (list (- right 15) top
                       (+ right  5) top
                       (+ right  5) bottom
                       (- right 15) bottom)
                 :filled nil :closed nil :ink +red+
                 :line-thickness 2))


(defclass thread-wrapper ()
  ((systhread :initform nil :accessor systhread)
   (displayer :initarg :displayer :accessor displayer)
   (thread-id :initarg :thread-id :reader thread-id)
   (record :initform nil :accessor thread-record)))

(defmethod print-object ((self thread-wrapper) stream)
  (let* ((systhread (systhread self))
         (displayer (displayer self))
         (thread-id (thread-id self))
         (fun-name (and displayer (function-name displayer))))
    (with-drawing-options (stream :ink (if (and systhread
                                                (bt:thread-alive-p systhread))
                                           +dark-blue+
                                           +dark-grey+))
      (format stream "#<~a ~a>"
              (or fun-name "(inactive)")
              thread-id))))

(defun make-thread (frame stream displayer)
  (let* ((thread-id (gensym "THREAD-"))
         (thread (make-instance 'thread-wrapper :displayer displayer
                                                :thread-id thread-id)))
    (flet ((thread-fun ()
             (funcall (displayer thread) frame stream thread)))
      (clim:execute-frame-command frame `(com-add-thread ,thread))
      (setf (systhread thread)
            (bt:make-thread #'thread-fun)))
    thread))

(defun stop-thread (frame thread)
  (declare (ignore frame))
  (let ((systhread (systhread thread)))
    (when (bt:thread-alive-p systhread)
      (setf (displayer thread) nil)
      (bt:join-thread systhread))))

(defclass draw-action (c2mop:funcallable-standard-object)
  ((name :initarg :name :reader function-name))
  (:metaclass c2mop:funcallable-standard-class))

(define-presentation-type draw-action ())

(defmethod print-object ((fun draw-action) stream)
  (format stream "#<~a>" (function-name fun)))

(defmacro define-action (name (frame stream thread) slots &body body)
  (let ((fun (gensym))
        (lam (gensym))
        (record (gensym)))
    `(let ((,fun (make-instance 'draw-action :name ',name))
           (,lam (lambda (,frame ,stream ,thread)
                   (declare (ignorable ,frame ,stream ,thread))
                   (let ,slots
                     (let (,record)
                       (setf (thread-record ,thread)
                             (updating-output (,stream :unique-id (thread-id ,thread))
                               (setf ,record (updating-output (,stream) ,@body))))
                       (loop while (displayer ,thread) do
                         (redisplay-output-record ,record ,stream)
                         ;; Don't be a pig!
                         (sleep 1/60)))))))
       (setf (fdefinition ',name) ,fun)
       (c2mop:set-funcallable-instance-function ,fun ,lam)
       ,fun)))

(defun draw-random-matrix (stream)
  (surrounding-output-with-border (stream :shape :matrix)
    (formatting-table (stream)
      (loop for row from 0 below 3 do
        (formatting-row (stream)
          (loop for col from 0 below 3 do
            (formatting-cell (stream)
              (princ (format nil "~,2f" (random 100.0)) stream))))))))

(define-action draw-matrix (frame stream thread) ()
  (draw-random-matrix stream))

(define-action draw-spinner (frame stream thread)
               ((ink (random-ink))
                (spin 0)
                (center (make-point 300 300)))
  (incf spin (/ pi 100))
  (with-rotation (stream spin center)
    (draw-rectangle* stream 200 200 400 400 :ink ink)))

(define-action draw-wheeler (frame stream thread)
               ((ink (random-ink))
                (spin 0)
                (center (make-point 300 300)))
  (incf spin (/ pi 100))
  (with-rotation (stream spin center)
    (draw-line* stream 300 300 600 300 :thickness 3)
    (draw-rectangle* stream 500 200 700 400 :ink ink)))


(define-application-frame concurrent-draw ()
  ((actions :initform (list) :accessor actions)
   (threads :initform (list) :accessor threads)
   (records :initform (list) :accessor records))
  (:reinitialize-frames t)
  (:pretty-name "Concurrent Drawing (EXPERIMENTAL)")
  (:panes (options :application :display-function 'display
                                :text-margins '(:left 20 :top 10)
                                :width 800 :height 600
                                :end-of-line-action :allow
                                :end-of-page-action :allow)
          (outputs :application :width 800 :height 600
                                :display-time nil
                                :text-style (make-text-style :fix nil nil)
                                :end-of-line-action :allow
                                :end-of-page-action :allow)))

(defmethod display ((frame concurrent-draw) stream)
  (formatting-table (stream :x-spacing 20)
    (formatting-column (stream)
      (formatting-cell (stream)
        (surrounding-output-with-border (stream :shape :underline)
          (format stream "Catalogue")))
      (dolist (v (actions frame))
        (formatting-cell (stream)
          (present v 'draw-action :stream stream))))
    (formatting-column (stream)
      (formatting-cell (stream)
        (surrounding-output-with-border (stream :shape :underline)
          (format stream "Active threads")))
      (dolist (v (threads frame))
        (formatting-cell (stream)
          (present v 'thread-wrapper :stream stream))))))

(macrolet ((def-pair (add-name del-name collection ptype)
             `(progn
                ;; Create
                (define-concurrent-draw-command ,add-name
                    ((object ,ptype))
                  (with-application-frame (frame)
                    (push object (,collection frame))))
                ;; Delete
                (define-concurrent-draw-command ,del-name
                    ((object ,ptype))
                  (with-application-frame (frame)
                    (setf (,collection frame)
                          (remove object (,collection frame))))))))
  (def-pair com-add-action com-del-action actions t)
  (def-pair com-add-thread com-del-thread threads t)
  (def-pair com-add-record com-del-record records t))

(define-concurrent-draw-command run-action
    ((self 'draw-action :gesture :select))
  (with-application-frame (frame)
    (let ((stream (get-frame-pane frame 'outputs)))
      (make-thread frame stream self))))

(define-concurrent-draw-command halt-thread
    ((self 'thread-wrapper :gesture :select))
  (with-application-frame (frame)
    (let ((stream (get-frame-pane frame 'outputs)))
      (stop-thread frame self)
      (com-del-thread self)
      (erase-output-record (thread-record self) stream nil)
      (finish-output stream))))

(define-concurrent-draw-command wipe-thread
    ((self 'thread-wrapper))
  (with-application-frame (frame)
    (stop-thread frame self)))

(define-concurrent-draw-command (com-clear-output :menu t) ()
  (with-application-frame (frame)
    (let ((sheet (get-frame-pane frame 'outputs)))
     (window-clear sheet))))

(define-concurrent-draw-command (com-reset-cursor :menu t) ()
  (with-application-frame (frame)
    (let ((stream (get-frame-pane frame 'outputs)))
      (climi::reset-stream-cursor stream
                                  (stream-text-cursor stream)))))

(defmethod initialize-instance :after ((frame concurrent-draw) &key)
  (setf (actions frame)
        (list #'draw-matrix
              #'draw-spinner
              #'draw-wheeler)))

;; (find-application-frame 'concurrent-draw)

