;;; ---------------------------------------------------------------------------
;;;   License: BSD-2-Clause.
;;; ---------------------------------------------------------------------------
;;;
;;;  (c) Copyright 2025 Daniel Kochmański <daniel@turtleware.eu>
;;;
;;; ---------------------------------------------------------------------------
;;;
;;; In this demo I'm demonstrating an example use of the timer as an accelerator
;;; gesture. Features also pulse and varying qualifiers.

(defpackage "CLIM-DEMO.TIMER-GESTURES"
  (:use "CLIM-LISP" "CLIM")
  (:export "LE-DOT"))
(in-package "CLIM-DEMO.TIMER-GESTURES")

(defgeneric display (frame stream))

(define-gesture-name :update-radar :timer :update)
(define-gesture-name :change-radar :timer :change)

(define-application-frame le-dot ()
  ((counter :accessor counter :initform 0)
   (update-color :accessor update-color :initform +black+)
   (update-pulse :accessor update-pulse :initform nil)
   (change-pulse :accessor change-pulse :initform nil))
  (:reinitialize-frames t)
  (:panes (app :application
               :text-margins '(:left 10 :top 5)
               :scroll-bars nil
               :width 480
               :height 320
               :display-time nil
               :display-function #'display)))

(defmethod display ((frame le-dot) stream)
  (format stream "Redisplay count: ~d~%" (incf (counter frame)))
  (with-drawing-options (stream :ink (update-color frame)
                                :line-thickness 4)
    
    (draw-circle* stream 240 160
                  (mod (truncate (get-internal-real-time) 20000)
                       120))))

(define-le-dot-command (com-update :menu nil :keystroke :update-radar)
    ()
  (with-application-frame (frame)
    (redisplay-frame-panes frame :force-p t)))

(define-le-dot-command (com-change :menu nil :keystroke :change-radar)
    ()
  (with-application-frame (frame)
    (setf (update-color frame)
          (make-contrasting-inks 8 (random 8)))))

(define-le-dot-command (com-single-update-timer :menu t) ()
  (clime:schedule-timer-event *standard-output* :update 1))

(define-le-dot-command (com-toggle-update-pulse :menu t) ()
  (with-application-frame (frame)
    (let ((sheet (frame-standard-output frame)))
      (with-slots (update-pulse) frame
        (setf update-pulse
              (if update-pulse
                  (prog1 nil
                    (clime:delete-pulse-event update-pulse))
                  (clime:schedule-pulse-event sheet :update 1/60)))))))

(define-le-dot-command (com-toggle-change-pulse :menu t) ()
  (with-application-frame (frame)
    (let ((sheet (frame-standard-output frame)))
      (with-slots (change-pulse) frame
        (setf change-pulse
              (if change-pulse
                  (prog1 nil
                    (clime:delete-pulse-event change-pulse))
                  (clime:schedule-pulse-event sheet :change .5)))))))

;;; (find-application-frame 'le-dot)
