;;; ------------------------------------
;;; coordinate-swizzling.lisp

(defpackage #:clim-demo.coord-swizzling
  (:use #:clim-lisp #:clim)
  (:export #:run #:coordinate-swizzling))
(in-package #:clim-demo.coord-swizzling)

(clim:define-application-frame coordinate-swizzling ()
  ()
  (:menu-bar nil)
  (:panes (app :application
               :scroll-bars t
               :display-time t)
          (int :interactor
               :scroll-bars t))
  (:geometry :left 200 :top 200 :width 480 :height 640)
  (:layouts
   (:default (clim:vertically ()
               (4/5 app)
               (1/5 int)))))

(defun run ()
  (clim:run-frame-top-level
   (clim:make-application-frame 'coordinate-swizzling)))

(define-coordinate-swizzling-command (com-clear :name t) ()
  (window-clear (clim:find-pane-named clim:*application-frame* 'app))
  (window-clear (clim:find-pane-named clim:*application-frame* 'int)))

(define-coordinate-swizzling-command (com-fill :name t) ()
  (let ((pane (clim:find-pane-named clim:*application-frame* 'app))
        (time (get-internal-real-time)))
    (loop for i from 0 to 4400
          do (format pane "~4,'0d~%" i))
    (setf time (- (get-internal-real-time) time))
    (let ((output (make-broadcast-stream *standard-input* *debug-io*)))
      (format output "Fill took ~,2fs.~%" (/ time internal-time-units-per-second)))))

(define-coordinate-swizzling-command (com-quit :name t) ()
  (clim:frame-exit clim:*application-frame*))
