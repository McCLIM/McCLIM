;;; ---------------------------------------------------------------------------
;;;   License: BSD-2-Clause.
;;; ---------------------------------------------------------------------------
;;;
;;;  (c) Copyright 2025 Daniel Kochmański <daniel@turtleware.eu>
;;;
;;; ---------------------------------------------------------------------------
;;;
;;; In this demo I'm demonstrating incremental redisplay and how caching work.
;;; The frame contains a few "pieces", each displayed in a new line. Both the
;;; and each piece have a display counter incremented on each redisplay of the
;;; element.
;;;
;;; The frame displays 8 pieces - four with unique ids and four anonymous.
;;; Interactions with pieces:
;;;
;;; - SELECT   (left click)      toggle piece state between "clean" and "dirty"
;;; - DESCRIBE (middle click)    toggle piece details view (second row of text)
;;; - SCROLL-DOWN                show piece details
;;; - SCROLL-UP                  hide piece details
;;;
;;; Each interaction is translated to a command that is followed by a redisplay.
;;; Anonymous pieces will be redisplayed each time, while named pieces will be
;;; redisplayed depending on the state of the state.
;;;
;;; Moreover there are two commands Redisplay and Shuffle available in the menu
;;; that trigger redisplay without interacting with a piece.

(defpackage "CLIM-DEMO.UNIQUE-ID-TEST"
  (:use "CLIM-LISP" "CLIM")
  (:export "UNIQUE-ID-TEST"))
(in-package "CLIM-DEMO.UNIQUE-ID-TEST")

;;; UNIQUE-ID of output records is specified by PIECE-ID. When it is NIL, then
;;; the record is anonymous and it will be always recreated when we redisplay
;;; the frame, because there is no way to compare two anonymous records.
(defclass piece ()
  ((id :initarg :id :accessor piece-id :initform nil)
   (valid :initform t :accessor piece-valid)
   (count :initform 0 :accessor piece-count)
   (extra :initform nil :accessor piece-extra)))

;;; When the cache-test returns true, then the output record is not redisplayed
;;; - we used the cached subtree. Our cache-test ensures that we redisplay when
;;; the piece is "dirty" or when the new value is different from the old one.
(defun piece-cache-test (new-value old-value)
  (and new-value
       (eql new-value old-value)))

(defun piece-cache-state-label (self)
  (if (piece-valid self) "[CLEAN]" "[DIRTY]"))

;;; The present method shows the cache state, the piece name and number of
;;; redisplays. Optionally it prints some details (internal real time).
;;;
;;; Note that we specialize to the gadget view. The default TEXTUAL-VIEW is
;;; reserved for menus and the interactor and should provide a succint text.
(define-presentation-method present
    ((self piece) (type piece) stream (view gadget-view) &key)
  (format stream "~a " (piece-cache-state-label self))
  (let ((id (piece-id self)))
    (if (null id)
        (with-drawing-options (stream :text-face :italic)
          (format stream "~12a" "anonymous"))
        (format stream "~12a" id))
    (format stream " (redisplayed ~2d times)" (incf (piece-count self)))
    (when (piece-extra self)
      (terpri stream)
      (with-drawing-options (stream :text-size :small)
        (format stream "Some details ~a" (get-internal-real-time))))))

;;; We display eight pieces, where four are anonymous and will be redisplayed
;;; along with the frame regardless of the cache state.
(defun make-pieces ()
  (list (make-instance 'piece :id :piece-0)
        (make-instance 'piece :id :piece-1)
        (make-instance 'piece :id :piece-2)
        (make-instance 'piece :id :piece-3)
        (make-instance 'piece)
        (make-instance 'piece)
        (make-instance 'piece)
        (make-instance 'piece)))

(define-application-frame unique-id-test ()
  ((counter :initform 0 :accessor counter)
   (pieces :initform (make-pieces) :accessor pieces))
  (:pane :application :display-function 'display
   :incremental-redisplay t
   :default-view +gadget-view+
   :text-style (make-text-style :fix :roman :large)
   :width 800 :height 600))

(define-unique-id-test-command (com-redisplay :menu t) ())

(define-unique-id-test-command (com-shuffle :menu t) ()
  (with-application-frame (frame)
   (setf (pieces frame) (alexandria:shuffle (pieces frame)))))

(define-unique-id-test-command (com-switch-cache)
    ((self 'piece :gesture :select))
  (setf (piece-valid self)
        (not (piece-valid self))))

(define-unique-id-test-command (com-switch-extra)
    ((self 'piece :gesture :describe))
  (setf (piece-extra self)
        (not (piece-extra self))))

(define-unique-id-test-command (com-show-extra)
    ((self 'piece :gesture :scroll-down))
  (setf (piece-extra self) t))

(define-unique-id-test-command (com-hide-extra)
    ((self 'piece :gesture :scroll-up))
  (setf (piece-extra self) nil))

;;; It is possible to redisplay individual anonymous records, but the cache test
;;; will be ignored.
(define-presentation-action act-redisplay-record
    (piece nil unique-id-test :gesture nil)
    (object presentation window)
  (labels ((find-updating-record (r)
             (if (or (null r) (updating-output-record-p r))
                 r
                 (find-updating-record (output-record-parent r)))))
    (redisplay-output-record (find-updating-record presentation)
                             window)))

(defun display (frame stream)
  (format stream "Frame displayed ~a times.~%"
          (incf (counter frame)))
  (terpri stream)
  (dolist (piece (pieces frame))
    (updating-output (stream :unique-id (piece-id piece)
                             :cache-value (piece-valid piece)
                             :cache-test 'piece-cache-test)
      (present piece 'piece :stream stream :single-box t))
    (terpri stream)))

;;; (find-application-frame 'unique-id-test)

