;;; ---------------------------------------------------------------------------
;;;   License: BSD-2-Clause.
;;; ---------------------------------------------------------------------------
;;;
;;;  (c) Copyright 2019 Daniel Kochmański <daniel@turtleware.eu>
;;;
;;; ---------------------------------------------------------------------------
;;;

(in-package #:clim-demo)

(define-application-frame tracking-pointer-test ()
  ()
  (:geometry :width 800 :height 600)
  (:panes (pane :basic-pane)
          (app :application :display-function #'display :scroll-bars nil
                            :text-margins '(:left 20 :top 10)))
  (:reinitialize-frames t)
  (:pointer-documentation t)
  (:layouts (default (vertically ()
                       (1/2 (labelling (:label "Application Pane") app))
                       (1/2 (labelling (:label "Basic Pane") pane))))))

(defmethod display ((frame tracking-pointer-test) stream)
  (flet ((present (object stream)
           (present object (presentation-type-of object) :stream stream)))
    (format-textual-list '(symbol "string" 31337 "") #'present
                         :stream stream :separator ", "
                         :suppress-separator-before-conjunction t
                         :conjunction ".")))

(defun make-draw-cursor-function ()
  (lambda (window x y id)
    (when (output-recording-stream-p window)
      (let ((ink (if (eq id :presentation) +green+ +red+)))
        (updating-output (window :unique-id :cursor)
          (with-drawing-options (window :clipping-region (sheet-region window))
            (draw-point* window x y :line-thickness 32)
            (draw-point* window x y :line-thickness 24
                                    :ink ink)))))
    (let ((stream *pointer-documentation-output*))
      (window-clear stream)
      (format stream "Press SPACE to exit.~%")
      (format stream "Window: ~4a, Clause ID: ~s.~%" (pane-name window) id)
      (finish-output stream))))

(define-tracking-pointer-test-command (track-pointer :name t :menu t) ()
  (let* ((tracked-sheet *standard-output*)
         (draw-cursor (make-draw-cursor-function)))
    (tracking-pointer (tracked-sheet :multiple-window t
                                     :context-type '(or string integer))
      (:pointer-motion
       (&key window x y)
       (funcall draw-cursor window x y :pointer-motion))
      (:pointer-button-press
       (&key event x y)
       (funcall draw-cursor (event-sheet event) x y :pointer-button-press))
      (:pointer-button-release
       (&key event x y)
       (funcall draw-cursor (event-sheet event) x y :pointer-button-release))
      (:presentation
       (&key presentation window x y)
       (declare (ignore presentation))
       (funcall draw-cursor window x y :presentation))
      (:presentation-button-press
       (&key presentation event x y)
       (declare (ignore presentation event x y))
       (window-clear *pointer-documentation-output*))
      (:presentation-button-release
       (&key presentation event x y)
       (declare (ignore presentation event x y))
       (return-from track-pointer))
      (:keyboard
       (&key gesture)
       ;; when gesture is a space then it should be already character.
       (when (eventp gesture)
         (setq gesture (keyboard-event-character gesture)))
       (when (eql gesture #\space)
         (window-clear *pointer-documentation-output*)
         (return-from track-pointer))))))
