;;; ---------------------------------------------------------------------------
;;;   License: BSD-2-Clause.
;;; ---------------------------------------------------------------------------
;;;
;;;  (c) Copyright 2025 Daniel Kochmański <daniel@turtleware.eu>
;;;
;;; ---------------------------------------------------------------------------
;;;
;;; This demo is used to stress-test concurrent drawing and the number of output
;;; records. It gets dog-slow very fast (above 100K records). If we minimize the
;;; window then records are added much faster.
;;;
;;; FIXME Mitigating that is a matter of caching the latest full replay and
;;; copying the pixmap on replay, and regenerating on damage. Yes, this requires
;;; us to put a firm distinction between repaint and repaint in the core.

(defpackage "CLIM-DEMO.CONCURRENT-GRID"
  (:use "CLIM-LISP" "CLIM")
  (:export "CONCURRENT-GRID"))
(in-package "CLIM-DEMO.CONCURRENT-GRID")

(defgeneric display (frame stream))

(define-application-frame concurrent-grid ()
  ((running :accessor running))
  (:pane :application
   :width 600 :height 800
   :text-style (make-text-style :fix nil nil)
   :scroll-bars nil
   :borders nil
   :display-time t
   :display-function #'display))

(defmethod display ((frame concurrent-grid) stream)
  (let ((ink1 +dark-grey+)
        (ink2 +light-grey+)
        (tile-size 200))
    (loop for y from 200 by tile-size below 800 do
      (loop for x from 0 by tile-size below 600 do
        (rotatef ink1 ink2)
        (draw-rectangle* stream x y (+ x tile-size) (+ y tile-size)
                         :ink ink1)))))

(defun stress-tile-1 (stream x0 y0 ink)
  (let ((tile-size 200)
        (cell-size 20))
    (loop for y from y0 by cell-size below (+ y0 tile-size) do
      (loop for x from x0 by cell-size below (+ x0 tile-size) do
        (let ((x1 (+ x 1))
              (x2 (+ x cell-size -1))
              (y1 (+ y 1))
              (y2 (+ y cell-size -1)))
          (draw-rectangle* stream x1 y1 x2 y2 :ink ink))
        (finish-output stream)))))

(defun stress-tile-2 (stream x0 y0 ink1 ink2)
  (let ((frame (pane-frame stream)))
    (loop while (running frame) do
      (stress-tile-1 stream x0 y0 ink1)
      (stress-tile-1 stream x0 y0 ink2))))

(define-concurrent-grid-command (com-start :menu t) ()
  (with-application-frame (frame)
    (let ((stream (frame-standard-output frame))
          (tile-size 200)
          (ink-no 0))
      (setf (running frame) t)
      (bt:make-thread
       (lambda ()
         (let ((history (stream-output-history stream))
               (counter 0)
               (opt-vel nil))
           (loop while (running frame) do
             (let ((old-count (output-record-count history))
                   (new-count (progn
                                (incf counter)
                                (sleep 1)
                                (output-record-count history))))
               (when (= counter 10)
                 (setf opt-vel (truncate (/ new-count counter))))
               (updating-output (stream :unique-id :summary :fixed-position t)
                 (format stream "Record count: ~d~%~
                                 Velocity:      ~4d/s~%~
                                 Avg. velocity: ~4d/s~%~
                                 10th velocity: ~4d/s~%"
                         new-count
                         (- new-count old-count)
                         (truncate (/ new-count counter))
                         (or opt-vel "WAIT"))))
             (sleep 1/10)))))
      (loop for y from 200 by tile-size below 800 do
        (loop for x from 0 by tile-size below 600 do
          (let ((ink1 (make-contrasting-inks 8 (mod (incf ink-no) 8)))
                (ink2 (make-contrasting-inks 8 (mod (incf ink-no) 8)))
                (x0 x)
                (y0 y))
            (bt:make-thread
             (lambda ()
               (stress-tile-2 stream x0 y0 ink1 ink2)))))))))

(define-concurrent-grid-command (com-stop :menu t) ()
  (with-application-frame (frame)
    (setf (running frame) nil)))

(define-concurrent-grid-command (com-clear :menu t) ()
  (with-application-frame (frame)
    (let ((stream (frame-standard-output frame)))
      (window-clear stream)
      (display frame stream)
      (finish-output stream))))

;; (find-application-frame 'concurrent-grid)

